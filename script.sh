#!/bin/bash
#mkdir -p media
#chmod 777 media
#mkdir -p static
echo "¡Bienvenido al Gestor de Proyectos IS2!"
echo "Seleccione uno de los siguientes entornos de despliegue:"
PS3='Por favor, ingrese una opción:' 
options=("Desarrollo" "Producción" "Generar Documentación" "Pruebas Unitarias" "Salir")
select opt in "${options[@]}"
do
    case $opt in
        "Desarrollo")
            echo "Eligió desplegar desarrollo..."
            echo
            #echo "Elija uno de los siguientes tags:"
            #git tag
            #echo "Ingrese en nombre del tag:"
            #read tg
            #git checkout $tg
            #while [ "$?" -ne 0 ]; do
            #     echo
            #     echo "No existe tag con el nombre proveído. Vuelva a intentar..."
            #     echo
            #     echo "Elija uno de los siguientes tags:"
            #     git tag
            #     read -p "Ingrese en nombre del tag: " tg
            #     git checkout $tg
            #done
            echo
            pip install virtualenv
            sudo rm -rf venv
            virtualenv venv --python=python3
            source venv/bin/activate
            pip3 install -r requirements.txt
            echo
            chmod +x devbdconf.sh
            sudo -u postgres ./devbdconf.sh
            break
            ;;
        "Producción")
            echo "Eligió desplegar producción..."
            echo
            pip install virtualenv
            sudo rm -rf venv
            virtualenv venv
            source venv/bin/activate
            pip3 install -r requirements.txt

            host_name="gestorproyectosis2.com" ## mysite SE REFIERE AL NOMBRE DEL PROYECTO DJANGO AL CREAR CON el comando django-admin startproject xxxx
            project_name="mysite"
            # find existing instances in the host file and save the line numbers
            matches_in_hosts="$(grep -n $host_name /etc/hosts | cut -f1 -d:)"


            #pip freeze > requirements.txt #para guardar los requerimientos automaticamente a un .txt
            #chmod +x produccion.sh #concede permisos para ejecucion del script
            #path=$(pwd) ##recupera el directorio de  trabajo del usuario por ejemplo /home/usuario
            root="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )" #path de directorio root
            #https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself
            cd $root
            #python-home=$path  #se usa si existe algun entorno virtual en la configuracion del Virtual host



            echo "Configuracion de Servidor Apache para Producción"

            if [ ! -z "$matches_in_hosts" ]
            then
                echo "El host $host_name ya se encuentra agregado en /etc/hosts"
            else
                echo "127.0.0.1 $host_name" | sudo tee -a /etc/hosts > /dev/null #agrega la direccion ip con el nombre del dominio al final de archivo /etc/hosts de apache
            fi        

            #https://stackoverflow.com/questions/19339248/append-line-to-etc-hosts-file-with-shell-script
            echo "<VirtualHost *:80> 
                    ServerName $host_name
                    ServerAdmin email@deladministrador.com
                    DocumentRoot $root

                    WSGIDaemonProcess $project_name python-home=$root/venv python-path=$root
                    WSGIProcessGroup $project_name
                    WSGIScriptAlias / $root/$project_name/wsgi.py

                    Alias /static $root/static
                    <Directory $root/static >
                        Require all granted
                    </Directory>

                    <Directory $root/$project_name >
                        <Files wsgi.py>
                            Require all granted
                        </Files>
                    </Directory>
                </VirtualHost>

                # vim: syntax=apache ts=4 sw=4 sts=4 sr noet" | sudo tee /etc/apache2/sites-available/"$project_name".conf > /dev/null
            sudo a2ensite "$project_name".conf #creamos un archivo de configuracion myproject.conf para el Apache
            #find . -type d -exec chmod 755 {} \;
            #find . -type f -exec chmod 644 {} \;
            #SE CONCEDEN PERMISOS AL USUARIO:GRUPO DE APACHE www-data
            #sudo chown www-data:www-data $root/
            #sudo chown www-data:www-data $root/venv 
            #sudo chown www-data:www-data $root/db.sqlite3 #esta linea se usa si se esta desarrolando con db sqlite3 o da error de permisos
            #https://unix.stackexchange.com/questions/1416/redirecting-stdout-to-a-file-you-dont-have-write-permission-on
            sudo systemctl restart apache2.service #Reinciamos Apache Server

            chmod +x prodbdconf.sh
            sudo -u postgres ./prodbdconf.sh
            break
            ;;
        "Generar Documentación")
            echo "Generando documentación..."
	        pycco mysite -s -i -p
            break
            ;;
        "Pruebas Unitarias")
            python3 manage.py test
            break
            ;;
        "Salir")
            break
            ;;
        *) echo "Opción inválida";;
    esac
done

