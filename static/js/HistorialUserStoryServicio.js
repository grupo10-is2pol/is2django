var HistorialUserStoryServicio = (function(){
/**documentacion */
    var ruta = '/api/historial-userstory/';

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }

    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }

    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }

    function HistorialUserStoryPorUs(id_us) {
        return axios.get(ruta +"?us="+id_us)
        .then( ( resultado ) => resultado.data )
    }

    function HistorialUserStoryPorUs_Sprint(id_us, id_sprint) {
        return axios.get(ruta +"?us=" + id_us + "&sprint=" + id_sprint)
        .then( ( resultado ) => resultado.data )
    }

    function HistorialSprintBacklog(id_sprint) { //trae (los userstories) como quedo el sprintbacklog antes de finalizar el sprint
        return axios.get(ruta + "?sprint=" + id_sprint)
        .then( ( resultado ) => resultado.data )
    }

    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        HistorialUserStoryPorUs,
        HistorialUserStoryPorUs_Sprint,
        HistorialSprintBacklog
    }

})();