var UserStoryServicio = (function(){

    var ruta = '/api/userstory/';    

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }

    function ListarPorProyecto( id_proyecto ) {
        return axios.get(ruta + "?proyecto=" + id_proyecto)
            .then( ( resultado ) => resultado.data )
    }

    function ListarUsSprintBackLog( id_proyecto,id_sprint ) { //ESTA FUNCION TAMBIEN ES ÚTIL PARA RECUPERAR TODOS LOS US EN LA VISTA DE TABLERO
        return axios.get(ruta + "?proyecto=" + id_proyecto +"&sprint="+id_sprint+"&estado=Asignado")
        .then( ( resultado1 ) => {
            return axios.get(ruta + "?proyecto=" + id_proyecto +"&sprint="+id_sprint+"&estado=No Finalizado").then( (resultado2) => {
                const userstories1= resultado1.data;
                const userstories2= resultado2.data;
                const result = userstories1.concat(userstories2);
                return result; //userStories con estado asignado y estado no finalizado, listado por proyecto y sprint
            })
        })
    }

    function ListarUsSprint( id_proyecto,id_sprint ) { //ESTA FUNCION TAMBIEN ES ÚTIL PARA RECUPERAR TODOS LOS US EN LA VISTA DE TABLERO
        return axios.get(ruta + "?proyecto=" + id_proyecto +"&sprint="+id_sprint)
        .then( ( resultado1 ) => {
            return resultado1.data
        })
    }
    

    
    function ListarUsSprintPlaning( id_proyecto ,id_sprint) {
        return axios.get(ruta + "?proyecto=" + id_proyecto +"&estado=No Finalizado")
            .then( ( resultado1 ) => {
                return axios.get(ruta + "?proyecto=" + id_proyecto  +"&estado=Pendiente").then( (resultado2) => {
                    return axios.get(ruta + "?proyecto=" + id_proyecto  + "&sprint=" + id_sprint + "&estado=Asignado").then( (resultado3) => {
                        const userstories1= resultado1.data;
                        const userstories2= resultado2.data;
                        const userstories3= resultado3.data;
                        const aux = userstories1.concat(userstories2);
                        const result = aux.concat(userstories3);
                        return result; //userStories con estado pendiente, estado asignado (para editar antes de inciar sprint ) y estado no finalizado, listado por proyecto
                    })
                })
  
            })
    }


    

    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }

    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }

    function ListarUsSprintBackLogPorEstadoEnTablero( id_proyecto,id_sprint,estado_tablero ) { //ESTA FUNCION TAMBIEN ES ÚTIL PARA RECUPERAR TODOS LOS US EN LA VISTA DE TABLERO con estado que le pasemos en estado_tablero
        return axios.get(ruta + "?proyecto=" + id_proyecto+"&sprint="+id_sprint+"&estado=Asignado"+"&estado_tablero="+estado_tablero)
            .then( ( resultado ) => resultado.data )
    }


    function ListarUsPorProyectoQA( id_proyecto,estado_tablero ) { //ESTA FUNCION TAMBIEN ES ÚTIL PARA RECUPERAR TODOS LOS US del proyecto que tengan estado asignado y que esten en qa
        return axios.get(ruta + "?proyecto=" + id_proyecto+ "&estado=Asignado" + "&estado_tablero="+estado_tablero)
            .then( ( resultado ) => resultado.data )
    }

    function ListarUsPorProyectoRealese( id_proyecto ) { //ESTA FUNCION TAMBIEN ES ÚTIL PARA RECUPERAR TODOS LOS US del proyecto que tengan estado asignado y que esten en qa
        return axios.get(ruta + "?proyecto=" + id_proyecto+ "&estado=Finalizado")
            .then( ( resultado ) => resultado.data )
    }
    

    

    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        ListarPorProyecto,
        ListarUsSprintPlaning,
        ListarUsSprintBackLog,
        ListarUsSprintBackLogPorEstadoEnTablero,
        ListarUsPorProyectoQA,
        ListarUsPorProyectoRealese,
        ListarUsSprint

    }

})();