var ProyectoServicio = (function(){

    var ruta = '/api/proyecto/';
    var ruta2 = '/api/miembro/'

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }

    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }

    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }

    function ListarProyectosDelUsuario( id_usuario ) { //Proyectos donde el usuario es miembro de algun proyecto
        return axios.get(ruta2 + "?usuario=" + id_usuario)
            .then( ( resultado ) => {
                const proyectosRecuperados = resultado.data;
                var proyectosActuales=[]
                proyectosRecuperados.forEach(item =>{
                    var merge = item.rol_proyecto
                    proyectosActuales.push(merge);
                })
                console.log("Proyectos Recuperados",proyectosActuales)
                return proyectosActuales;
                
            }) 
    }

    


    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        ListarProyectosDelUsuario,
        
    }

})();