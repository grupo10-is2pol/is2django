var MiembroServicio = (function(){

    var ruta = '/api/miembro/';
    var ruta2 = '/api/rol-proyecto/';    

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }

    function ObtenerMiembrosProyecto( id_proyecto ) {
        return axios.get(ruta + "?proyecto=" + id_proyecto)
            .then( ( resultado ) => resultado.data )
    }

    function ObtenerMiembrosProyectoDisponibles( id_proyecto ) {
        return axios.get(ruta + "?proyecto=" + id_proyecto +"&disponible=true")
            .then( ( resultado ) => resultado.data )
    }

    function ObtenerMiembro( id_usuario, id_proyecto ) {
        return axios.get(ruta + "?proyecto=" + id_proyecto + "&usuario=" + id_usuario)
            .then( ( resultado ) => {
                return resultado.data[0]
            }) 
    }


    function ObtenerIdMiembro( id_usuario, id_proyecto ) {
        return axios.get(ruta + "?proyecto=" + id_proyecto + "&usuario=" + id_usuario)
            .then( ( resultado ) => {
                var idMiembro = resultado.data[0].id;
                console.log("El idMiembro recuperado es: ", idMiembro);
                return idMiembro;
            }) 
    }


    

    function ObtenerMiembroScrumMaster( id_proyecto ) {
        return axios.get(ruta2 + "?nombre=ScrumMaster" +"&proyecto="+id_proyecto)
        .then( ( resultado ) => {
            RolProyectoIdScrumMaster=resultado.data[0].id
            console.log(RolProyectoIdScrumMaster)
            return axios.get(ruta + "?proyecto=" + id_proyecto + "&rol_proyecto="+RolProyectoIdScrumMaster)
            .then( ( resultado2 ) => {
                console.log(resultado2.data)
                ScrumMasterActualDelProyecto = resultado2.data[0]
                return ScrumMasterActualDelProyecto
            }) 
        } )
    }

    function esMiembro(id_usuario,id_proyecto){
        return axios.get(ruta + "?proyecto="+id_proyecto +"&usuario="+id_usuario)
        .then((resultado) =>{
            if(resultado.data[0] == undefined){
                return false
            }else if(resultado.data[0].usuario.id == id_usuario){
                return true
            }
        })
    }

    





    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }


   


    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }
    //asdf

    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        ObtenerMiembrosProyecto,
        ObtenerIdMiembro,
        ObtenerMiembro,
        ObtenerMiembroScrumMaster,
        ObtenerMiembrosProyectoDisponibles,
        esMiembro
    }

})();