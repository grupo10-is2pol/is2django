var RolProyectoServicio = (function(){

    var ruta = '/api/rol-proyecto/';

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }
 

    function rolesUnicos(id_proyecto) {
        return axios.get(ruta + "?es_unico=true" +"&proyecto="+ id_proyecto)
            .then( ( resultado ) => resultado.data )

    }

    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }

    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }

    function ListarRolesPorProyecto(id_proyecto) {
        return axios.get(ruta  + "?proyecto=" + id_proyecto)
        .then( ( resultado ) => resultado.data )
    }

    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        rolesUnicos,
        ListarRolesPorProyecto

    }

})();