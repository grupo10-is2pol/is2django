var UsuarioSistemaServicio = (function(){

    var ruta = '/api/usuario/';
    var ruta2 = '/api/miembro/';

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }

    function ListarUsuariosSelectEditarProyecto(id_proyecto) {
        return axios.get(ruta2 +"?proyecto"+id_proyecto )
        .then( ( resultado ) => {
            return axios.get(ruta).then((resultado2)=>{
                var aux1=resultado.data
                var aux2=resultado2.data
                var aux3=[]

                aux2.forEach(usuario => {
                    aux1.forEach(miembro =>{
                        if(usuario.id != miembro.usuario.id){
                            if(aux3.indexOf(usuario) == -1)
                                aux3.push(usuario);
                        }
                    })
                    
                });
                return aux3; //usuarios que no son miembros del proyecto
            })
        })
    }

    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }

    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }

    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        ListarUsuariosSelectEditarProyecto
    }

})();