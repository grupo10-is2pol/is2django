var SprintServicio = (function(){

    var ruta = '/api/sprint/';
    var ruta2 = '/api/miembros-sprint/'

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }

    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }

    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }

    function CapacidadSprint( id_sprint ) { 
        return axios.get(ruta2 + '?sprint=' + id_sprint)
            .then( ( resultado ) => {
                const miembrosSprint = resultado.data;                
                
                var capacidad = 0;
                var capacidadSprint = 0;

                //console.log(miembrosSprint)

                miembrosSprint.forEach(miembroActual =>{
                    capacidad = capacidad + miembroActual.horas_laborales
                });

                //console.log(capacidad)

                return axios.get(ruta + id_sprint + '/').then( (resultado2) =>{

                    const SprintActual = resultado2.data

                    //console.log(SprintActual)

                    capacidadSprint = capacidad * SprintActual.dias_laborales
                    //console.log(capacidadSprint);

                    console.log("LA CAPACIDAD DEL SPRINT CON ID: " + id_sprint + " es: " + capacidadSprint);

                    return capacidadSprint; 
                })
                     
            }) 
    }

    function ListarSprintPorProyecto(id_proyecto) {
        return axios.get(ruta + '?proyecto=' + id_proyecto )
        .then( ( resultado ) => resultado.data )
    }


    function ListarSprintPorProyectoEnProcesoPendiente(id_proyecto) { //para la pantalla de ejecucion de proyecto
        return axios.get(ruta + "?proyecto=" + id_proyecto +"&estado=En Proceso") //solo puede haber uno a la vez
        .then( ( resultado1 ) => {
            if(resultado1.data[0] == undefined){ //si no hay sprints en proceso, traer todos los sprints pendientes
                return axios.get(ruta + "?proyecto=" + id_proyecto +"&estado=Pendiente")
                .then((resultado2) => resultado2.data)
            }else{ //existen sprint en proceso
                return resultado1.data
            }
                
        })

    }


    function ListarSprintDelUsuarioPorProyecto( id_proyecto,id_usuario ) { //Sprints donde el usuario es miembro de alguno
        return axios.get(ruta2 + "?usuario=" + id_usuario)
            .then( ( resultado ) => {
                const miembrosSprintRecuperados = resultado.data;
                var sprintActuales=[]
                miembrosSprintRecuperados.forEach(item =>{
                    if(item.sprint.proyecto.id == id_proyecto)
                        sprintActuales.push(item.sprint);
                })
                console.log("Los sprints de este Proyecto en donde trabaja el usuario son: ",sprintActuales);
                return sprintActuales;
                
            }) 
    }
    


    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        CapacidadSprint,
        ListarSprintPorProyecto,
        ListarSprintDelUsuarioPorProyecto,
        ListarSprintPorProyectoEnProcesoPendiente
    }

})();