var MiembroSprintServicio = (function(){

    var ruta = '/api/miembros-sprint/';
    var ruta2 = '/api/rol-proyecto/';    

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }

    function ListarPorSprint( id_sprint ) {
        return axios.get(ruta + "?sprint=" + id_sprint)
            .then( ( resultado ) => resultado.data )
    }

    function ObtenerMiembro( id_team_member, id_sprint ) {
        return axios.get(ruta + "?sprint=" + id_sprint + "&team_member=" + id_team_member)
            .then( ( resultado ) => {
                return resultado.data[0]
            }) 
    }


    function ObtenerIDMiembroSprint( id_team_member, id_sprint ) {
        return axios.get(ruta + "?sprint=" + id_sprint + "&team_member=" + id_team_member)
            .then( ( resultado ) => {
                var idMiembro = resultado.data[0].id;
                console.log("El idMiembro del Sprint recuperado es: ", idMiembro);
                return idMiembro;
            }) 
    }



    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }

    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }

    function ListarMiembrosSprintPorMiembro( id_team_member) {
        return axios.get(ruta + "?team_member=" + id_team_member)
            .then( ( resultado ) => {
                var miembrosprint = resultado.data;
                console.log("los miembros sprint  recuperados son: ", miembrosprint);
                return miembrosprint;
            }) 
    }
    

    

    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        ListarPorSprint,
        ObtenerIDMiembroSprint,
        ObtenerMiembro,
        ListarMiembrosSprintPorMiembro
    }

})();