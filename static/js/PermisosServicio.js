var PermisosServicio = (function(){

    var ruta = '/api/permiso/';
    var ruta2 = '/api/miembro/';
    var ruta3 = '/api/usuario/';

    function ObtenerPermisosSistema() {
        return axios.get(ruta + "?tipo_permiso=Administracion")
            .then( ( resultado ) => resultado.data )

    }

    function ObtenerPermisosProyecto() {
        return axios.get(ruta + "?tipo_permiso=Estandar")
            .then( ( resultado ) => resultado.data )

    }

    function ObtenerPermisosProyectoDesactivados() {
        return axios.get(ruta + "?tipo_permiso=Estandar")
            .then( ( resultado ) => resultado.data )

    }

    function PermisosDelUsuarioPorProyecto( id_usuario, id_proyecto ) {
        return axios.get(ruta2 + "?proyecto=" + id_proyecto + "&usuario=" + id_usuario)
            .then( ( resultado ) => {
                const permisosRecuperados = resultado.data[0].rol_proyecto.permisos;
                var permisosActuales=[]
                permisosRecuperados.forEach(permiso =>{
                    permisosActuales.push(permiso.nombre);
                })
                console.log(permisosActuales);
                return permisosActuales;
            }) 
    }

    function PermisosDelUsuarioPorSistema( id_usuario) {
        return axios.get(ruta3 + id_usuario + "/")
            .then( ( resultado ) => {
                //console.log(resultado.data)
                const permisosRecuperados = resultado.data.rol_sistema.permisos;
                var permisosActuales=[]
                permisosRecuperados.forEach(permiso =>{
                    permisosActuales.push(permiso.nombre);
                })
                console.log(permisosActuales);
                return permisosActuales;
            }) 
    }

    return {
        ObtenerPermisosSistema,
        ObtenerPermisosProyecto,
        PermisosDelUsuarioPorProyecto,
        PermisosDelUsuarioPorSistema,
        ObtenerPermisosProyectoDesactivados
    }
})();