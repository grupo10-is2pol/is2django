var HistorialEstadosUserStoryServicio = (function(){
/**documentacion */
    var ruta = '/api/historial-estados-userstory/';

    function Listar() {
        return axios.get(ruta )
        .then( ( resultado ) => resultado.data )
    }

    function Guardar( obj ) {
        return axios.post(ruta, obj)
                .then( ( resultado ) => resultado.data )
    }

    function Obtener( id ) {
        return axios.get(ruta + id +"/")
                    .then( ( resultado ) => resultado.data )
    }

    function Editar( obj ) {
        return axios.put(ruta + obj.id +"/", obj)
                    .then( ( resultado ) => resultado.data )
    }

    function Borrar( id ) {
        return axios.delete( ruta + id +"/" )
                    .then( ( resultado ) => resultado.data )
    }

    function HistorialEstadosUserStoryPorUs(id_us) {
        return axios.get(ruta +"?us="+id_us)
        .then( ( resultado ) => resultado.data )
    }

    function HistorialEstadosUserStoryPorUs_Sprint(id_us, id_sprint) {
        return axios.get(ruta +"?us=" + id_us + "&sprint=" + id_sprint)
        .then( ( resultado ) => resultado.data )
    }

    return {
        Listar,
        Borrar,
        Guardar,
        Editar,
        Obtener,
        HistorialEstadosUserStoryPorUs,
        HistorialEstadosUserStoryPorUs_Sprint
    }

})();