--
-- PostgreSQL database dump
--

-- Dumped from database version 12.8 (Ubuntu 12.8-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.4 (Ubuntu 13.4-4.pgdg20.04+1)

-- Started on 2021-11-19 04:23:23 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3535 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 99352)
-- Name: Actividad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Actividad" (
    id bigint NOT NULL,
    nombre character varying(255) NOT NULL,
    duracion integer NOT NULL,
    descripcion text,
    fecha date NOT NULL,
    miembro_id bigint NOT NULL,
    sprint_id bigint NOT NULL,
    us_id bigint NOT NULL,
    estado_tablero character varying(30) NOT NULL,
    CONSTRAINT "Actividad_duracion_check" CHECK ((duracion >= 0)),
    CONSTRAINT "DuracionDebeEstarEntre:1-1000" CHECK (((duracion >= 0) AND (duracion <= 1000)))
);


ALTER TABLE public."Actividad" OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 99360)
-- Name: Actividad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Actividad_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Actividad_id_seq" OWNER TO postgres;

--
-- TOC entry 3536 (class 0 OID 0)
-- Dependencies: 203
-- Name: Actividad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Actividad_id_seq" OWNED BY public."Actividad".id;


--
-- TOC entry 204 (class 1259 OID 99362)
-- Name: HistorialEstadosUserStory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."HistorialEstadosUserStory" (
    id bigint NOT NULL,
    estado_tablero character varying(30),
    fecha_cambio_estado date,
    miembro_sprint_id bigint NOT NULL,
    sprint_id bigint NOT NULL,
    us_id bigint NOT NULL,
    hora_cambio_estado character varying(30)
);


ALTER TABLE public."HistorialEstadosUserStory" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 99365)
-- Name: HistorialEstadosUserStory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."HistorialEstadosUserStory_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."HistorialEstadosUserStory_id_seq" OWNER TO postgres;

--
-- TOC entry 3537 (class 0 OID 0)
-- Dependencies: 205
-- Name: HistorialEstadosUserStory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."HistorialEstadosUserStory_id_seq" OWNED BY public."HistorialEstadosUserStory".id;


--
-- TOC entry 206 (class 1259 OID 99367)
-- Name: HistorialUserStory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."HistorialUserStory" (
    id bigint NOT NULL,
    duracion_estimada_sprint integer NOT NULL,
    fecha_estimada date,
    estado_tablero character varying(30),
    horas_trabajadas_sprint integer,
    miembro_sprint_id bigint NOT NULL,
    sprint_id bigint NOT NULL,
    us_id bigint NOT NULL,
    priorizacion numeric(10,2),
    motivo_comentario character varying(1000),
    estado character varying(30) NOT NULL,
    CONSTRAINT "HistorialUserStory_duracion_estimada_sprint_check" CHECK ((duracion_estimada_sprint >= 0)),
    CONSTRAINT "HistorialUserStory_horas_trabajadas_sprint_check" CHECK ((horas_trabajadas_sprint >= 0))
);


ALTER TABLE public."HistorialUserStory" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 99375)
-- Name: HistorialUserStory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."HistorialUserStory_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."HistorialUserStory_id_seq" OWNER TO postgres;

--
-- TOC entry 3538 (class 0 OID 0)
-- Dependencies: 207
-- Name: HistorialUserStory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."HistorialUserStory_id_seq" OWNED BY public."HistorialUserStory".id;


--
-- TOC entry 208 (class 1259 OID 99377)
-- Name: MiembrosSprint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."MiembrosSprint" (
    id bigint NOT NULL,
    horas_laborales integer NOT NULL,
    sprint_id bigint NOT NULL,
    team_member_id bigint NOT NULL,
    usuario_id integer,
    CONSTRAINT "HorasLaboralesDelMiembroDebeEstarEntre:1-24" CHECK (((horas_laborales >= 1) AND (horas_laborales <= 24)))
);


ALTER TABLE public."MiembrosSprint" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 99381)
-- Name: MiembrosSprint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."MiembrosSprint_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."MiembrosSprint_id_seq" OWNER TO postgres;

--
-- TOC entry 3539 (class 0 OID 0)
-- Dependencies: 209
-- Name: MiembrosSprint_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."MiembrosSprint_id_seq" OWNED BY public."MiembrosSprint".id;


--
-- TOC entry 210 (class 1259 OID 99383)
-- Name: Nota; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Nota" (
    id bigint NOT NULL,
    nota text NOT NULL,
    fecha timestamp with time zone NOT NULL,
    us_id bigint NOT NULL
);


ALTER TABLE public."Nota" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 99389)
-- Name: Nota_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Nota_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Nota_id_seq" OWNER TO postgres;

--
-- TOC entry 3540 (class 0 OID 0)
-- Dependencies: 211
-- Name: Nota_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Nota_id_seq" OWNED BY public."Nota".id;


--
-- TOC entry 212 (class 1259 OID 99391)
-- Name: Permiso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Permiso" (
    id bigint NOT NULL,
    nombre character varying(75) NOT NULL,
    tipo_permiso character varying(25) NOT NULL,
    desactivado boolean NOT NULL
);


ALTER TABLE public."Permiso" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 99394)
-- Name: Permiso_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Permiso_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Permiso_id_seq" OWNER TO postgres;

--
-- TOC entry 3541 (class 0 OID 0)
-- Dependencies: 213
-- Name: Permiso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Permiso_id_seq" OWNED BY public."Permiso".id;


--
-- TOC entry 214 (class 1259 OID 99396)
-- Name: Profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Profile" (
    id bigint NOT NULL,
    descripcion text,
    rol_sistema_id bigint,
    user_id integer NOT NULL
);


ALTER TABLE public."Profile" OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 99402)
-- Name: Profile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Profile_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Profile_id_seq" OWNER TO postgres;

--
-- TOC entry 3542 (class 0 OID 0)
-- Dependencies: 215
-- Name: Profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Profile_id_seq" OWNED BY public."Profile".id;


--
-- TOC entry 216 (class 1259 OID 99404)
-- Name: Proyecto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto" (
    id bigint NOT NULL,
    nombre character varying(300) NOT NULL,
    fecha_inicio date,
    fecha_fin date,
    estado character varying(25) NOT NULL,
    descripcion text
);


ALTER TABLE public."Proyecto" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 99410)
-- Name: Proyecto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_id_seq" OWNER TO postgres;

--
-- TOC entry 3543 (class 0 OID 0)
-- Dependencies: 217
-- Name: Proyecto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_id_seq" OWNED BY public."Proyecto".id;


--
-- TOC entry 218 (class 1259 OID 99412)
-- Name: RolProyecto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."RolProyecto" (
    id bigint NOT NULL,
    nombre character varying(55) NOT NULL,
    descripcion text,
    es_unico boolean NOT NULL,
    es_modificable boolean NOT NULL,
    proyecto_id bigint
);


ALTER TABLE public."RolProyecto" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 99418)
-- Name: RolProyecto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."RolProyecto_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RolProyecto_id_seq" OWNER TO postgres;

--
-- TOC entry 3544 (class 0 OID 0)
-- Dependencies: 219
-- Name: RolProyecto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."RolProyecto_id_seq" OWNED BY public."RolProyecto".id;


--
-- TOC entry 220 (class 1259 OID 99420)
-- Name: RolProyecto_permisos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."RolProyecto_permisos" (
    id bigint NOT NULL,
    rolproyecto_id bigint NOT NULL,
    permiso_id bigint NOT NULL
);


ALTER TABLE public."RolProyecto_permisos" OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 99423)
-- Name: RolProyecto_permisos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."RolProyecto_permisos_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RolProyecto_permisos_id_seq" OWNER TO postgres;

--
-- TOC entry 3545 (class 0 OID 0)
-- Dependencies: 221
-- Name: RolProyecto_permisos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."RolProyecto_permisos_id_seq" OWNED BY public."RolProyecto_permisos".id;


--
-- TOC entry 222 (class 1259 OID 99425)
-- Name: RolSistema; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."RolSistema" (
    id bigint NOT NULL,
    nombre character varying(55) NOT NULL,
    descripcion text,
    es_modificable boolean NOT NULL
);


ALTER TABLE public."RolSistema" OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 99431)
-- Name: RolSistema_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."RolSistema_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RolSistema_id_seq" OWNER TO postgres;

--
-- TOC entry 3546 (class 0 OID 0)
-- Dependencies: 223
-- Name: RolSistema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."RolSistema_id_seq" OWNED BY public."RolSistema".id;


--
-- TOC entry 224 (class 1259 OID 99433)
-- Name: RolSistema_permisos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."RolSistema_permisos" (
    id bigint NOT NULL,
    rolsistema_id bigint NOT NULL,
    permiso_id bigint NOT NULL
);


ALTER TABLE public."RolSistema_permisos" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 99436)
-- Name: RolSistema_permisos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."RolSistema_permisos_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RolSistema_permisos_id_seq" OWNER TO postgres;

--
-- TOC entry 3547 (class 0 OID 0)
-- Dependencies: 225
-- Name: RolSistema_permisos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."RolSistema_permisos_id_seq" OWNED BY public."RolSistema_permisos".id;


--
-- TOC entry 226 (class 1259 OID 99438)
-- Name: Sprint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Sprint" (
    id bigint NOT NULL,
    nombre character varying(20) NOT NULL,
    dias_laborales integer NOT NULL,
    dias_habiles integer[],
    estado character varying(25) NOT NULL,
    fecha_inicio date,
    fecha_fin date,
    proyecto_id bigint NOT NULL,
    CONSTRAINT "DiasLaboralesDebeEstarEntre:1-365" CHECK (((dias_laborales >= 1) AND (dias_laborales <= 365))),
    CONSTRAINT mysite_sprint_dias_laborales_check CHECK ((dias_laborales >= 0))
);


ALTER TABLE public."Sprint" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 99446)
-- Name: Tablero; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Tablero" (
    id bigint NOT NULL,
    sprint_id bigint NOT NULL,
    us_id bigint
);


ALTER TABLE public."Tablero" OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 99449)
-- Name: Tablero_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Tablero_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Tablero_id_seq" OWNER TO postgres;

--
-- TOC entry 3548 (class 0 OID 0)
-- Dependencies: 228
-- Name: Tablero_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Tablero_id_seq" OWNED BY public."Tablero".id;


--
-- TOC entry 229 (class 1259 OID 99451)
-- Name: TeamMember; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."TeamMember" (
    id bigint NOT NULL,
    proyecto_id bigint NOT NULL,
    rol_proyecto_id bigint,
    usuario_id integer,
    disponible boolean NOT NULL
);


ALTER TABLE public."TeamMember" OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 99454)
-- Name: TeamMember_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."TeamMember_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."TeamMember_id_seq" OWNER TO postgres;

--
-- TOC entry 3549 (class 0 OID 0)
-- Dependencies: 230
-- Name: TeamMember_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."TeamMember_id_seq" OWNED BY public."TeamMember".id;


--
-- TOC entry 231 (class 1259 OID 99456)
-- Name: UserStory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."UserStory" (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    valor_negocio integer NOT NULL,
    prioridad integer NOT NULL,
    valor_tecnico integer NOT NULL,
    descripcion text,
    estado character varying(30) NOT NULL,
    estado_tablero character varying(30) NOT NULL,
    priorizacion numeric(10,2),
    duracion_estimada integer,
    duracion_restante integer,
    miembro_id bigint,
    miembro_sprint_id bigint,
    proyecto_id bigint NOT NULL,
    sprint_id bigint,
    usuario_id integer,
    horas_trabajadas_us integer,
    porcentaje_realizado integer,
    horas_extras_trabajadas_us integer,
    motivo_comentario character varying(1000),
    estado_qa character varying(30) NOT NULL,
    CONSTRAINT "DuracionEstimadaUSDebeEstarEntre:1-1000Horas" CHECK (((duracion_estimada >= 0) AND (duracion_estimada <= 1000))),
    CONSTRAINT "PrioridadDebeEstarEntre:0-10" CHECK (((prioridad >= 0) AND (prioridad <= 10))),
    CONSTRAINT "UserStory_prioridad_check" CHECK ((prioridad >= 0)),
    CONSTRAINT "UserStory_valor_negocio_check" CHECK ((valor_negocio >= 0)),
    CONSTRAINT "UserStory_valor_tecnico_check" CHECK ((valor_tecnico >= 0)),
    CONSTRAINT "ValorDeNegocioDebeEstarEntre:0-10" CHECK (((valor_negocio >= 0) AND (valor_negocio <= 10))),
    CONSTRAINT "ValorTecnicoDebeEstarEntre:0-10" CHECK (((valor_tecnico >= 0) AND (valor_tecnico <= 10)))
);


ALTER TABLE public."UserStory" OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 99469)
-- Name: UserStory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."UserStory_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."UserStory_id_seq" OWNER TO postgres;

--
-- TOC entry 3550 (class 0 OID 0)
-- Dependencies: 232
-- Name: UserStory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."UserStory_id_seq" OWNED BY public."UserStory".id;


--
-- TOC entry 233 (class 1259 OID 99471)
-- Name: account_emailaddress; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account_emailaddress (
    id bigint NOT NULL,
    email character varying(254) NOT NULL,
    verified boolean NOT NULL,
    "primary" boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.account_emailaddress OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 99474)
-- Name: account_emailaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_emailaddress_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailaddress_id_seq OWNER TO postgres;

--
-- TOC entry 3551 (class 0 OID 0)
-- Dependencies: 234
-- Name: account_emailaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.account_emailaddress_id_seq OWNED BY public.account_emailaddress.id;


--
-- TOC entry 235 (class 1259 OID 99476)
-- Name: account_emailconfirmation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account_emailconfirmation (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    sent timestamp with time zone,
    key character varying(64) NOT NULL,
    email_address_id bigint NOT NULL
);


ALTER TABLE public.account_emailconfirmation OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 99479)
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_emailconfirmation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailconfirmation_id_seq OWNER TO postgres;

--
-- TOC entry 3552 (class 0 OID 0)
-- Dependencies: 236
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.account_emailconfirmation_id_seq OWNED BY public.account_emailconfirmation.id;


--
-- TOC entry 237 (class 1259 OID 99481)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 99484)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- TOC entry 3553 (class 0 OID 0)
-- Dependencies: 238
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- TOC entry 239 (class 1259 OID 99486)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 99489)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 3554 (class 0 OID 0)
-- Dependencies: 240
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- TOC entry 241 (class 1259 OID 99491)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 99494)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- TOC entry 3555 (class 0 OID 0)
-- Dependencies: 242
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- TOC entry 243 (class 1259 OID 99496)
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 99502)
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_groups (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 99505)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- TOC entry 3556 (class 0 OID 0)
-- Dependencies: 245
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- TOC entry 246 (class 1259 OID 99507)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- TOC entry 3557 (class 0 OID 0)
-- Dependencies: 246
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- TOC entry 247 (class 1259 OID 99509)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_user_permissions (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 99512)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 3558 (class 0 OID 0)
-- Dependencies: 248
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- TOC entry 249 (class 1259 OID 99514)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 99521)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- TOC entry 3559 (class 0 OID 0)
-- Dependencies: 250
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- TOC entry 251 (class 1259 OID 99523)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 99526)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- TOC entry 3560 (class 0 OID 0)
-- Dependencies: 252
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- TOC entry 253 (class 1259 OID 99528)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 99534)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- TOC entry 3561 (class 0 OID 0)
-- Dependencies: 254
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- TOC entry 255 (class 1259 OID 99536)
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 99542)
-- Name: django_site; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 99545)
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO postgres;

--
-- TOC entry 3562 (class 0 OID 0)
-- Dependencies: 257
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- TOC entry 258 (class 1259 OID 99547)
-- Name: mysite_sprint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.mysite_sprint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mysite_sprint_id_seq OWNER TO postgres;

--
-- TOC entry 3563 (class 0 OID 0)
-- Dependencies: 258
-- Name: mysite_sprint_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.mysite_sprint_id_seq OWNED BY public."Sprint".id;


--
-- TOC entry 259 (class 1259 OID 99549)
-- Name: socialaccount_socialaccount; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socialaccount_socialaccount (
    id bigint NOT NULL,
    provider character varying(30) NOT NULL,
    uid character varying(191) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialaccount OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 99555)
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.socialaccount_socialaccount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialaccount_id_seq OWNER TO postgres;

--
-- TOC entry 3564 (class 0 OID 0)
-- Dependencies: 260
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.socialaccount_socialaccount_id_seq OWNED BY public.socialaccount_socialaccount.id;


--
-- TOC entry 261 (class 1259 OID 99557)
-- Name: socialaccount_socialapp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socialaccount_socialapp (
    id bigint NOT NULL,
    provider character varying(30) NOT NULL,
    name character varying(40) NOT NULL,
    client_id character varying(191) NOT NULL,
    secret character varying(191) NOT NULL,
    key character varying(191) NOT NULL
);


ALTER TABLE public.socialaccount_socialapp OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 99563)
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.socialaccount_socialapp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_id_seq OWNER TO postgres;

--
-- TOC entry 3565 (class 0 OID 0)
-- Dependencies: 262
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.socialaccount_socialapp_id_seq OWNED BY public.socialaccount_socialapp.id;


--
-- TOC entry 263 (class 1259 OID 99565)
-- Name: socialaccount_socialapp_sites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socialaccount_socialapp_sites (
    id bigint NOT NULL,
    socialapp_id bigint NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialapp_sites OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 99568)
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.socialaccount_socialapp_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_sites_id_seq OWNER TO postgres;

--
-- TOC entry 3566 (class 0 OID 0)
-- Dependencies: 264
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.socialaccount_socialapp_sites_id_seq OWNED BY public.socialaccount_socialapp_sites.id;


--
-- TOC entry 265 (class 1259 OID 99570)
-- Name: socialaccount_socialtoken; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socialaccount_socialtoken (
    id bigint NOT NULL,
    token text NOT NULL,
    token_secret text NOT NULL,
    expires_at timestamp with time zone,
    account_id bigint NOT NULL,
    app_id bigint NOT NULL
);


ALTER TABLE public.socialaccount_socialtoken OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 99576)
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.socialaccount_socialtoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialtoken_id_seq OWNER TO postgres;

--
-- TOC entry 3567 (class 0 OID 0)
-- Dependencies: 266
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.socialaccount_socialtoken_id_seq OWNED BY public.socialaccount_socialtoken.id;


--
-- TOC entry 3078 (class 2604 OID 99578)
-- Name: Actividad id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Actividad" ALTER COLUMN id SET DEFAULT nextval('public."Actividad_id_seq"'::regclass);


--
-- TOC entry 3081 (class 2604 OID 99579)
-- Name: HistorialEstadosUserStory id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialEstadosUserStory" ALTER COLUMN id SET DEFAULT nextval('public."HistorialEstadosUserStory_id_seq"'::regclass);


--
-- TOC entry 3082 (class 2604 OID 99580)
-- Name: HistorialUserStory id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialUserStory" ALTER COLUMN id SET DEFAULT nextval('public."HistorialUserStory_id_seq"'::regclass);


--
-- TOC entry 3085 (class 2604 OID 99581)
-- Name: MiembrosSprint id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."MiembrosSprint" ALTER COLUMN id SET DEFAULT nextval('public."MiembrosSprint_id_seq"'::regclass);


--
-- TOC entry 3087 (class 2604 OID 99582)
-- Name: Nota id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Nota" ALTER COLUMN id SET DEFAULT nextval('public."Nota_id_seq"'::regclass);


--
-- TOC entry 3088 (class 2604 OID 99583)
-- Name: Permiso id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Permiso" ALTER COLUMN id SET DEFAULT nextval('public."Permiso_id_seq"'::regclass);


--
-- TOC entry 3089 (class 2604 OID 99584)
-- Name: Profile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Profile" ALTER COLUMN id SET DEFAULT nextval('public."Profile_id_seq"'::regclass);


--
-- TOC entry 3090 (class 2604 OID 99585)
-- Name: Proyecto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_id_seq"'::regclass);


--
-- TOC entry 3091 (class 2604 OID 99586)
-- Name: RolProyecto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto" ALTER COLUMN id SET DEFAULT nextval('public."RolProyecto_id_seq"'::regclass);


--
-- TOC entry 3092 (class 2604 OID 99587)
-- Name: RolProyecto_permisos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto_permisos" ALTER COLUMN id SET DEFAULT nextval('public."RolProyecto_permisos_id_seq"'::regclass);


--
-- TOC entry 3093 (class 2604 OID 99588)
-- Name: RolSistema id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolSistema" ALTER COLUMN id SET DEFAULT nextval('public."RolSistema_id_seq"'::regclass);


--
-- TOC entry 3094 (class 2604 OID 99589)
-- Name: RolSistema_permisos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolSistema_permisos" ALTER COLUMN id SET DEFAULT nextval('public."RolSistema_permisos_id_seq"'::regclass);


--
-- TOC entry 3095 (class 2604 OID 99590)
-- Name: Sprint id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sprint" ALTER COLUMN id SET DEFAULT nextval('public.mysite_sprint_id_seq'::regclass);


--
-- TOC entry 3098 (class 2604 OID 99591)
-- Name: Tablero id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tablero" ALTER COLUMN id SET DEFAULT nextval('public."Tablero_id_seq"'::regclass);


--
-- TOC entry 3099 (class 2604 OID 99592)
-- Name: TeamMember id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TeamMember" ALTER COLUMN id SET DEFAULT nextval('public."TeamMember_id_seq"'::regclass);


--
-- TOC entry 3100 (class 2604 OID 99593)
-- Name: UserStory id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserStory" ALTER COLUMN id SET DEFAULT nextval('public."UserStory_id_seq"'::regclass);


--
-- TOC entry 3108 (class 2604 OID 99594)
-- Name: account_emailaddress id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailaddress ALTER COLUMN id SET DEFAULT nextval('public.account_emailaddress_id_seq'::regclass);


--
-- TOC entry 3109 (class 2604 OID 99595)
-- Name: account_emailconfirmation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailconfirmation ALTER COLUMN id SET DEFAULT nextval('public.account_emailconfirmation_id_seq'::regclass);


--
-- TOC entry 3110 (class 2604 OID 99596)
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- TOC entry 3111 (class 2604 OID 99597)
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 3112 (class 2604 OID 99598)
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- TOC entry 3113 (class 2604 OID 99599)
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- TOC entry 3114 (class 2604 OID 99600)
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- TOC entry 3115 (class 2604 OID 99601)
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 3116 (class 2604 OID 99602)
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- TOC entry 3118 (class 2604 OID 99603)
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- TOC entry 3119 (class 2604 OID 99604)
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- TOC entry 3120 (class 2604 OID 99605)
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- TOC entry 3121 (class 2604 OID 99606)
-- Name: socialaccount_socialaccount id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialaccount ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialaccount_id_seq'::regclass);


--
-- TOC entry 3122 (class 2604 OID 99607)
-- Name: socialaccount_socialapp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_id_seq'::regclass);


--
-- TOC entry 3123 (class 2604 OID 99608)
-- Name: socialaccount_socialapp_sites id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_sites_id_seq'::regclass);


--
-- TOC entry 3124 (class 2604 OID 99609)
-- Name: socialaccount_socialtoken id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialtoken_id_seq'::regclass);


--
-- TOC entry 3465 (class 0 OID 99352)
-- Dependencies: 202
-- Data for Name: Actividad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Actividad" (id, nombre, duracion, descripcion, fecha, miembro_id, sprint_id, us_id, estado_tablero) FROM stdin;
126	Actividad1	8	trabajo realizado	2021-11-19	151	141	160	Doing
127	Actividad2	8	descripcion	2021-11-20	151	141	160	Doing
128	Actividad3	8	descr	2021-11-21	151	141	160	Doing
129	Actividad4	8		2021-11-22	151	141	160	Doing
130	Actividad5	8		2021-11-23	151	141	160	Doing
131	Actividad6	6		2021-11-24	151	141	160	Doing
132	Actividad Final	10	trabajo terminado	2021-11-25	151	141	160	Doing
133	Actividad1	7		2021-11-19	152	141	162	Doing
134	Actividad2	7		2021-11-20	152	141	162	Doing
135	Actividad3	7		2021-11-21	152	141	162	Doing
136	Actividad4	8		2021-11-22	152	141	162	Doing
137	Actividad5	6		2021-11-23	152	141	162	Doing
138	Actividad6	9		2021-11-24	152	141	162	Doing
139	Actividad7	9	userstory listo para pasar a Done	2021-11-25	152	141	162	Doing
140	Actividad1	8		2021-11-19	153	141	161	Doing
141	Actividad2	6		2021-11-20	153	141	161	Doing
142	Actividad3	7		2021-11-21	153	141	161	Doing
143	Actividad4	6		2021-11-22	153	141	161	Doing
144	Actividad5	8		2021-11-23	153	141	161	Doing
145	Actividad6	8		2021-11-24	153	141	161	Doing
146	Actividad Final	6		2021-11-25	153	141	161	Doing
147	Aprobación de User Story UserStory1	0	excelente trabajo	2021-11-19	151	141	160	Control de Calidad
148	Aprobación de User Story UserStory3	0	buen trabajo	2021-11-19	152	141	162	Control de Calidad
149	Rechazo de User Story UserStory2	0	porfavor rehacer el userstory	2021-11-19	153	141	161	Control de Calidad
150	Actividad1	6		2021-11-19	154	142	163	Doing
151	Actividad2	8		2021-11-20	154	142	163	Doing
152	Actividad3	10		2021-11-21	154	142	163	Doing
153	Actividad4	6		2021-11-22	154	142	163	Doing
154	Actividad1	3		2021-11-19	156	142	161	Doing
155	Actividad2	3		2021-11-20	156	142	161	Doing
156	Actividad Final	4	se volvio a trabajar en el userstory	2021-11-21	156	142	161	Doing
157	Aprobación de User Story UserStory2	0	buen trabajo, excelente!!	2021-11-19	156	142	161	Control de Calidad
158	Aprobación de User Story UserStory4	0	buen trabajo!!	2021-11-19	154	142	163	Control de Calidad
159	Actividad1	8		2021-11-19	158	143	164	Doing
160	Actividad2	10	asd	2021-11-20	158	143	164	Doing
161	Actividad3	10		2021-11-21	158	143	164	Doing
162	Actividad1	9		2021-11-19	159	143	165	Doing
163	Actividad2	6		2021-11-20	159	143	165	Doing
164	Actividad3	6		2021-11-21	159	143	165	Doing
165	Actividad4	10		2021-11-22	159	143	165	Doing
166	Aprobación de User Story UserStory5	0	buen trabajo!!!	2021-11-19	158	143	164	Control de Calidad
167	Aprobación de User Story UserStory6	0	buen trabajo!!!	2021-11-19	159	143	165	Control de Calidad
\.


--
-- TOC entry 3467 (class 0 OID 99362)
-- Dependencies: 204
-- Data for Name: HistorialEstadosUserStory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."HistorialEstadosUserStory" (id, estado_tablero, fecha_cambio_estado, miembro_sprint_id, sprint_id, us_id, hora_cambio_estado) FROM stdin;
282	To Do	2021-11-19	152	141	162	02:10:02
283	To Do	2021-11-19	151	141	160	02:10:02
284	To Do	2021-11-19	153	141	161	02:10:02
285	Doing	2021-11-19	151	141	160	02:11:17
286	Done	2021-11-19	151	141	160	02:20:59
287	Doing	2021-11-19	152	141	162	02:22:59
288	Done	2021-11-19	152	141	162	02:26:59
289	Doing	2021-11-19	153	141	161	02:29:08
290	Done	2021-11-19	153	141	161	02:32:28
291	To Do	2021-11-19	156	142	161	02:45:49
292	To Do	2021-11-19	154	142	163	02:45:49
293	Doing	2021-11-19	154	142	163	02:48:00
294	Done	2021-11-19	154	142	163	02:49:12
295	Doing	2021-11-19	156	142	161	02:50:44
296	Done	2021-11-19	156	142	161	02:53:29
297	To Do	2021-11-19	158	143	164	03:35:32
298	To Do	2021-11-19	159	143	165	03:35:32
299	Doing	2021-11-19	158	143	164	03:37:47
300	Done	2021-11-19	158	143	164	03:40:24
301	Doing	2021-11-19	159	143	165	03:41:53
302	Done	2021-11-19	159	143	165	03:43:14
\.


--
-- TOC entry 3469 (class 0 OID 99367)
-- Dependencies: 206
-- Data for Name: HistorialUserStory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."HistorialUserStory" (id, duracion_estimada_sprint, fecha_estimada, estado_tablero, horas_trabajadas_sprint, miembro_sprint_id, sprint_id, us_id, priorizacion, motivo_comentario, estado) FROM stdin;
57	56	2021-11-19	Control de Calidad	56	151	141	160	6.75	excelente trabajo	Finalizado
58	56	2021-11-19	Control de Calidad	53	152	141	162	5.00	buen trabajo	Finalizado
59	56	2021-11-19	Control de Calidad	49	153	141	161	3.50	porfavor rehacer el userstory	No Finalizado
61	10	2021-11-19	Control de Calidad	10	156	142	161	12.50	buen trabajo, excelente!!	Finalizado
60	30	2021-11-19	Control de Calidad	30	154	142	163	4.75	buen trabajo!!	Finalizado
62	27	2021-11-19	Control de Calidad	35	158	143	164	4.75	buen trabajo!!!	Finalizado
63	27	2021-11-19	Control de Calidad	31	159	143	165	4.50	buen trabajo!!!	Finalizado
\.


--
-- TOC entry 3471 (class 0 OID 99377)
-- Dependencies: 208
-- Data for Name: MiembrosSprint; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."MiembrosSprint" (id, horas_laborales, sprint_id, team_member_id, usuario_id) FROM stdin;
151	8	141	97	4
152	8	141	98	2
153	8	141	99	5
154	8	142	97	4
155	1	142	98	2
156	1	142	99	5
157	6	143	97	4
158	6	143	98	2
159	6	143	99	5
\.


--
-- TOC entry 3473 (class 0 OID 99383)
-- Dependencies: 210
-- Data for Name: Nota; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Nota" (id, nota, fecha, us_id) FROM stdin;
40	nota1	2021-11-19 02:11:24.428924-03	160
41	nota2	2021-11-19 02:11:29.96749-03	160
42	Empezamos a trabajar en el us	2021-11-19 02:29:41.261741-03	161
43	trabajo terminado	2021-11-19 02:49:09.118843-03	163
44	empezamos a trabajar	2021-11-19 02:50:56.751236-03	161
\.


--
-- TOC entry 3475 (class 0 OID 99391)
-- Dependencies: 212
-- Data for Name: Permiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Permiso" (id, nombre, tipo_permiso, desactivado) FROM stdin;
3	Listar Proyecto	Administracion	f
6	Listar Rol Sistema	Administracion	f
8	Listar Usuario	Administracion	f
12	Ver Menu del Proyecto	Estandar	f
15	Listar Rol Proyecto	Estandar	f
20	Listar Sprint	Estandar	f
23	Listar UserStory	Estandar	f
1	Crear Proyecto	Administracion	t
2	Editar Proyecto	Administracion	t
4	Crear Rol Sistema	Administracion	t
5	Editar Rol Sistema	Administracion	t
7	Editar Usuario	Administracion	t
11	Activar Proyecto	Estandar	t
14	Crear Rol Proyecto	Estandar	t
16	Editar Rol Proyecto	Estandar	t
17	Cancelar Proyecto	Estandar	t
18	Suspender Proyecto	Estandar	t
19	Finalizar Proyecto	Estandar	t
21	Crear Sprint	Estandar	t
22	Editar Sprint	Estandar	t
24	Crear UserStory	Estandar	t
25	Editar UserStory	Estandar	t
26	Agregar Miembro al Sprint	Estandar	t
27	Reemplazar Miembro	Estandar	t
28	Asignar UserStory	Estandar	t
13	Agregar Miembro al Proyecto	Estandar	t
29	Ver SprintBacklog	Estandar	t
30	Ver ProductBacklog	Estandar	t
32	Iniciar Sprint	Estandar	t
33	Finalizar Sprint	Estandar	t
31	Ver BurndownChart	Estandar	t
34	Ver Kanban	Estandar	t
35	Ver Control de Calidad	Estandar	t
36	Ver Actividades	Estandar	t
37	Aprobar UserStory	Estandar	t
38	Rechazar UserStory	Estandar	t
39	Generar Reportes	Estandar	t
40	Eliminar UserStory	Estandar	t
10	Ver Ejecucion de Proyecto	Administracion	t
41	Eliminar Proyecto	Administracion	t
42	Eliminar Rol Proyecto	Estandar	t
43	Eliminar Rol Sistema	Administracion	t
\.


--
-- TOC entry 3477 (class 0 OID 99396)
-- Dependencies: 214
-- Data for Name: Profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Profile" (id, descripcion, rol_sistema_id, user_id) FROM stdin;
3		2	3
2		2	2
5	Usuario que solo puede ver ejecucion del Proyecto, (Puede ser un developer o un scrumMaster por ejemplo)	2	5
6	Super Usuario, encargado de la administración de proyectos, asignar roles de sistema, en otras labores	1	6
4	Usuario que solo puede ver ejecucion del Proyecto, (Puede ser un developer o un scrumMaster por ejemplo)	2	4
1		1	1
\.


--
-- TOC entry 3479 (class 0 OID 99404)
-- Dependencies: 216
-- Data for Name: Proyecto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto" (id, nombre, fecha_inicio, fecha_fin, estado, descripcion) FROM stdin;
175	ProyectoIteracion6	2021-11-19	2021-11-19	Terminado	proyecto para el examen final de IS2
176	ProyectoCancelado	2021-11-19	2021-11-19	Cancelado	ejemplo de proyecto cancelado
\.


--
-- TOC entry 3481 (class 0 OID 99412)
-- Dependencies: 218
-- Data for Name: RolProyecto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."RolProyecto" (id, nombre, descripcion, es_unico, es_modificable, proyecto_id) FROM stdin;
111	ScrumMaster	ScrumMaster	t	f	175
112	Developer	Desarrollador del proyecto	f	f	175
113	ProductOwner	Es el responsable de asegurar que el equipo aporte valor al negocio. Representa las partes interesadas internas y externas, por lo que debe comprender y apoyar las necesidades de todos los usuarios en el negocio, así como también las necesidades y el funcionamiento del Equipo Scrum.	t	f	175
114	ScrumMaster	ScrumMaster	t	f	176
115	Developer	Desarrollador del proyecto	f	f	176
116	ProductOwner	Es el responsable de asegurar que el equipo aporte valor al negocio. Representa las partes interesadas internas y externas, por lo que debe comprender y apoyar las necesidades de todos los usuarios en el negocio, así como también las necesidades y el funcionamiento del Equipo Scrum.	t	f	176
\.


--
-- TOC entry 3483 (class 0 OID 99420)
-- Dependencies: 220
-- Data for Name: RolProyecto_permisos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."RolProyecto_permisos" (id, rolproyecto_id, permiso_id) FROM stdin;
776	111	11
777	111	12
778	111	13
779	111	14
780	111	15
781	111	16
782	111	17
783	111	18
784	111	19
785	111	20
786	111	21
787	111	22
788	111	23
789	111	24
790	111	25
791	111	26
792	111	27
793	111	28
794	111	29
795	111	30
796	111	31
797	111	32
798	111	33
799	111	34
800	111	35
801	111	36
802	111	37
803	111	38
804	111	39
805	111	40
806	111	42
807	112	34
808	112	12
809	113	34
810	113	39
811	113	12
812	113	29
813	113	30
814	113	31
815	114	11
816	114	12
817	114	13
818	114	14
819	114	15
820	114	16
821	114	17
822	114	18
823	114	19
824	114	20
825	114	21
826	114	22
827	114	23
828	114	24
829	114	25
830	114	26
831	114	27
832	114	28
833	114	29
834	114	30
835	114	31
836	114	32
837	114	33
838	114	34
839	114	35
840	114	36
841	114	37
842	114	38
843	114	39
844	114	40
845	114	42
846	115	34
847	115	12
848	116	34
849	116	39
850	116	12
851	116	29
852	116	30
853	116	31
\.


--
-- TOC entry 3485 (class 0 OID 99425)
-- Dependencies: 222
-- Data for Name: RolSistema; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."RolSistema" (id, nombre, descripcion, es_modificable) FROM stdin;
2	Usuario Normal	Usuario que puede acceder a los proyectos (al cual pertenece)	f
1	Administrador	Es el encargado de crear los proyectos, asignar un Scrum Master y de asignar roles de Sistema a los usuarios, entre otras labores.	f
3	rolsitemaDinamico		t
\.


--
-- TOC entry 3487 (class 0 OID 99433)
-- Dependencies: 224
-- Data for Name: RolSistema_permisos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."RolSistema_permisos" (id, rolsistema_id, permiso_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	1	4
5	1	5
6	1	6
7	1	7
8	1	8
10	1	10
11	2	10
13	1	41
14	1	43
15	3	2
16	3	3
17	3	6
\.


--
-- TOC entry 3489 (class 0 OID 99438)
-- Dependencies: 226
-- Data for Name: Sprint; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Sprint" (id, nombre, dias_laborales, dias_habiles, estado, fecha_inicio, fecha_fin, proyecto_id) FROM stdin;
141	Iteracion1	7	\N	Terminado	2021-11-19	2021-11-26	175
142	Iteracion	4	\N	Terminado	2021-11-19	2021-11-23	175
143	Iteracion3	3	\N	Terminado	2021-11-19	2021-11-22	175
\.


--
-- TOC entry 3490 (class 0 OID 99446)
-- Dependencies: 227
-- Data for Name: Tablero; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Tablero" (id, sprint_id, us_id) FROM stdin;
\.


--
-- TOC entry 3492 (class 0 OID 99451)
-- Dependencies: 229
-- Data for Name: TeamMember; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."TeamMember" (id, proyecto_id, rol_proyecto_id, usuario_id, disponible) FROM stdin;
97	175	111	4	t
98	175	112	2	t
99	175	112	5	t
100	176	114	4	t
101	176	115	2	t
102	176	115	5	t
\.


--
-- TOC entry 3494 (class 0 OID 99456)
-- Dependencies: 231
-- Data for Name: UserStory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."UserStory" (id, nombre, valor_negocio, prioridad, valor_tecnico, descripcion, estado, estado_tablero, priorizacion, duracion_estimada, duracion_restante, miembro_id, miembro_sprint_id, proyecto_id, sprint_id, usuario_id, horas_trabajadas_us, porcentaje_realizado, horas_extras_trabajadas_us, motivo_comentario, estado_qa) FROM stdin;
160	UserStory1	4	5	7	userstory1	Finalizado	Control de Calidad	6.75	56	0	\N	151	175	141	\N	56	100	0	excelente trabajo	Aprobado
162	UserStory3	3	8	3	userstory3	Finalizado	Control de Calidad	5.00	56	3	\N	152	175	141	\N	53	94	0	buen trabajo	Aprobado
164	UserStory5	5	3	3		Finalizado	Control de Calidad	4.75	27	-8	\N	158	175	143	\N	35	129	8	buen trabajo!!!	Aprobado
165	UserStory6	3	2	5		Finalizado	Control de Calidad	4.50	27	-4	\N	159	175	143	\N	31	114	4	buen trabajo!!!	Aprobado
166	userstoryCancelado	3	3	3	ejemplo	Pendiente	To Do	3.75	\N	\N	\N	\N	176	\N	\N	0	0	0	\N	Ninguno
161	UserStory2	10	10	10	userstory2	Finalizado	Control de Calidad	12.50	10	0	\N	156	175	142	\N	59	100	\N	buen trabajo, excelente!!	Aprobado
167	userstoryCancelado2	1	1	1		Pendiente	To Do	1.25	\N	\N	\N	\N	176	\N	\N	0	0	0	\N	Ninguno
163	UserStory4	5	5	2		Finalizado	Control de Calidad	4.75	30	0	\N	154	175	142	\N	30	100	0	buen trabajo!!	Aprobado
\.


--
-- TOC entry 3496 (class 0 OID 99471)
-- Dependencies: 233
-- Data for Name: account_emailaddress; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.account_emailaddress (id, email, verified, "primary", user_id) FROM stdin;
1	mateoggv94@fpuna.edu.py	t	t	2
2	mateoggv94@gmail.com	t	t	3
3	is2equipo10@gmail.com	t	t	4
4	equipo10is22021@gmail.com	t	t	5
5	equipo10superuser@gmail.com	t	t	6
\.


--
-- TOC entry 3498 (class 0 OID 99476)
-- Dependencies: 235
-- Data for Name: account_emailconfirmation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.account_emailconfirmation (id, created, sent, key, email_address_id) FROM stdin;
\.


--
-- TOC entry 3500 (class 0 OID 99481)
-- Dependencies: 237
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- TOC entry 3502 (class 0 OID 99486)
-- Dependencies: 239
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- TOC entry 3504 (class 0 OID 99491)
-- Dependencies: 241
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add site	1	add_site
2	Can change site	1	change_site
3	Can delete site	1	delete_site
4	Can view site	1	view_site
5	Can add log entry	2	add_logentry
6	Can change log entry	2	change_logentry
7	Can delete log entry	2	delete_logentry
8	Can view log entry	2	view_logentry
9	Can add permission	3	add_permission
10	Can change permission	3	change_permission
11	Can delete permission	3	delete_permission
12	Can view permission	3	view_permission
13	Can add group	4	add_group
14	Can change group	4	change_group
15	Can delete group	4	delete_group
16	Can view group	4	view_group
17	Can add user	5	add_user
18	Can change user	5	change_user
19	Can delete user	5	delete_user
20	Can view user	5	view_user
21	Can add content type	6	add_contenttype
22	Can change content type	6	change_contenttype
23	Can delete content type	6	delete_contenttype
24	Can view content type	6	view_contenttype
25	Can add session	7	add_session
26	Can change session	7	change_session
27	Can delete session	7	delete_session
28	Can view session	7	view_session
29	Can add miembros sprint	8	add_miembrossprint
30	Can change miembros sprint	8	change_miembrossprint
31	Can delete miembros sprint	8	delete_miembrossprint
32	Can view miembros sprint	8	view_miembrossprint
33	Can add permiso	9	add_permiso
34	Can change permiso	9	change_permiso
35	Can delete permiso	9	delete_permiso
36	Can view permiso	9	view_permiso
37	Can add proyecto	10	add_proyecto
38	Can change proyecto	10	change_proyecto
39	Can delete proyecto	10	delete_proyecto
40	Can view proyecto	10	view_proyecto
41	Can add rol proyecto	11	add_rolproyecto
42	Can change rol proyecto	11	change_rolproyecto
43	Can delete rol proyecto	11	delete_rolproyecto
44	Can view rol proyecto	11	view_rolproyecto
45	Can add sprint	12	add_sprint
46	Can change sprint	12	change_sprint
47	Can delete sprint	12	delete_sprint
48	Can view sprint	12	view_sprint
49	Can add team member	13	add_teammember
50	Can change team member	13	change_teammember
51	Can delete team member	13	delete_teammember
52	Can view team member	13	view_teammember
53	Can add user story	14	add_userstory
54	Can change user story	14	change_userstory
55	Can delete user story	14	delete_userstory
56	Can view user story	14	view_userstory
57	Can add tablero	15	add_tablero
58	Can change tablero	15	change_tablero
59	Can delete tablero	15	delete_tablero
60	Can view tablero	15	view_tablero
61	Can add rol sistema	16	add_rolsistema
62	Can change rol sistema	16	change_rolsistema
63	Can delete rol sistema	16	delete_rolsistema
64	Can view rol sistema	16	view_rolsistema
65	Can add profile	17	add_profile
66	Can change profile	17	change_profile
67	Can delete profile	17	delete_profile
68	Can view profile	17	view_profile
69	Can add nota	18	add_nota
70	Can change nota	18	change_nota
71	Can delete nota	18	delete_nota
72	Can view nota	18	view_nota
73	Can add actividad	19	add_actividad
74	Can change actividad	19	change_actividad
75	Can delete actividad	19	delete_actividad
76	Can view actividad	19	view_actividad
77	Can add email address	20	add_emailaddress
78	Can change email address	20	change_emailaddress
79	Can delete email address	20	delete_emailaddress
80	Can view email address	20	view_emailaddress
81	Can add email confirmation	21	add_emailconfirmation
82	Can change email confirmation	21	change_emailconfirmation
83	Can delete email confirmation	21	delete_emailconfirmation
84	Can view email confirmation	21	view_emailconfirmation
85	Can add social account	22	add_socialaccount
86	Can change social account	22	change_socialaccount
87	Can delete social account	22	delete_socialaccount
88	Can view social account	22	view_socialaccount
89	Can add social application	23	add_socialapp
90	Can change social application	23	change_socialapp
91	Can delete social application	23	delete_socialapp
92	Can view social application	23	view_socialapp
93	Can add social application token	24	add_socialtoken
94	Can change social application token	24	change_socialtoken
95	Can delete social application token	24	delete_socialtoken
96	Can view social application token	24	view_socialtoken
97	Can add historial user story	25	add_historialuserstory
98	Can change historial user story	25	change_historialuserstory
99	Can delete historial user story	25	delete_historialuserstory
100	Can view historial user story	25	view_historialuserstory
101	Can add historial estados user story	26	add_historialestadosuserstory
102	Can change historial estados user story	26	change_historialestadosuserstory
103	Can delete historial estados user story	26	delete_historialestadosuserstory
104	Can view historial estados user story	26	view_historialestadosuserstory
\.


--
-- TOC entry 3506 (class 0 OID 99496)
-- Dependencies: 243
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
3	!kpOoWbA8hwJggxiLucnIzmnxOzokQHOoP2xvEQs5	2021-11-18 01:29:43.217541-03	f	mateo	Mateo	Guillen	mateoggv94@gmail.com	f	t	2021-10-01 14:59:59.350478-04
2	!GaX2YWgxiutjCZEaPYuN4YQrs5qUzaY2gayjtD3L	2021-11-19 03:37:22.924123-03	f	mateo_giovanni	Mateo Giovanni	Guillen Vera	mateoggv94@fpuna.edu.py	f	t	2021-10-01 14:59:38.022734-04
5	!cRDLuIqF9PNa8AI1tPHGCM7eIWFUXFXb3zRoJZx0	2021-11-19 03:41:23.224865-03	f	maria	maria	is2	equipo10is22021@gmail.com	f	t	2021-10-01 15:17:10.206431-04
6	!a3UG5fW1rhO77PKrMdfUaQhxOsH30pB98sAymxdo	2021-11-19 03:56:33.548185-03	f	admin_equipo10	admin equipo10	is2	equipo10superuser@gmail.com	f	t	2021-11-18 15:39:54.191457-03
4	!M2thy6Tcv4VYqk2oYCCjF50cTZRSE4SRafvadHZn	2021-11-19 04:11:01.106979-03	f	equipo10	equipo10	is2	is2equipo10@gmail.com	f	t	2021-10-01 15:13:52.80271-04
1	pbkdf2_sha256$260000$xrjWYilfI70NN5Hiq3MA9l$p+qlwcNBLK/gTTXmGznnzBHoD+x5vtQ3HtErCOyzO/Q=	2021-11-19 04:21:22.688905-03	t	admin			is2equipo10@gmail.com	t	t	2021-10-01 14:51:12-04
\.


--
-- TOC entry 3507 (class 0 OID 99502)
-- Dependencies: 244
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- TOC entry 3510 (class 0 OID 99509)
-- Dependencies: 247
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- TOC entry 3512 (class 0 OID 99514)
-- Dependencies: 249
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-10-01 14:58:13.750058-04	1	admin	2	[{"changed": {"fields": ["Rol de Sistema"]}}]	17	1
2	2021-10-01 15:00:22.821362-04	2	mateo_giovanni	2	[{"changed": {"fields": ["Rol de Sistema"]}}]	17	1
3	2021-10-01 15:00:34.176394-04	3	mateo	2	[{"changed": {"fields": ["Rol de Sistema"]}}]	17	1
4	2021-10-01 16:34:53.716942-04	12	r1	1	[{"added": {}}]	11	1
5	2021-10-01 16:35:24.093228-04	3	p1	2	[{"added": {"name": "team member", "object": "TeamMember object (5)"}}]	10	1
6	2021-10-01 16:35:45.705853-04	12	r1	3		11	1
7	2021-10-01 16:36:08.029903-04	3	p1	3		10	1
8	2021-10-02 00:15:39.562247-04	20	Listar Sprint	1	[{"added": {}}]	9	1
9	2021-10-02 00:15:52.192261-04	21	Crear Sprint	1	[{"added": {}}]	9	1
10	2021-10-02 00:16:05.812194-04	22	Editar Sprint	1	[{"added": {}}]	9	1
11	2021-10-02 00:18:36.217763-04	23	Listar UserStory	1	[{"added": {}}]	9	1
12	2021-10-02 00:19:26.544166-04	24	Crear UserStory	1	[{"added": {}}]	9	1
13	2021-10-02 00:19:41.933738-04	25	Editar UserStory	1	[{"added": {}}]	9	1
14	2021-10-02 03:31:49.137225-04	10	sprtdaad8uuuiuy	3		12	1
15	2021-10-02 03:31:49.248882-04	9	TESTeditsprint	3		12	1
16	2021-10-02 03:31:49.281964-04	8	fgfaaf	3		12	1
17	2021-10-02 03:31:49.307035-04	7	tttttttdada	3		12	1
18	2021-10-02 03:31:49.332031-04	6	yui	3		12	1
19	2021-10-02 03:31:49.340184-04	5	yui	3		12	1
20	2021-10-02 03:31:49.348442-04	4	SprintTestFinal	3		12	1
21	2021-10-02 03:31:49.356733-04	3	hr	3		12	1
22	2021-10-02 03:31:49.381895-04	2	sp2	3		12	1
23	2021-10-02 03:31:49.406927-04	1	sprint1	3		12	1
24	2021-10-02 03:34:33.648056-04	6	p3	3		10	1
25	2021-10-02 03:34:33.666099-04	5	p2	3		10	1
26	2021-10-02 03:34:33.674706-04	4	p1	3		10	1
27	2021-10-02 03:39:48.445238-04	11	Sprint1	2	[{"changed": {"fields": ["Estado"]}}]	12	1
28	2021-10-02 21:42:31.388811-04	26	Agregar Miembro al Sprint	1	[{"added": {}}]	9	1
29	2021-10-02 21:43:18.36185-04	27	Reemplazar Miembro	1	[{"added": {}}]	9	1
30	2021-10-02 23:14:41.814182-04	26	porfin	3		12	1
31	2021-10-02 23:14:41.831904-04	25	dd	3		12	1
32	2021-10-02 23:14:41.840169-04	24	uuuuu	3		12	1
33	2021-10-02 23:14:41.848477-04	23	iii	3		12	1
34	2021-10-02 23:14:41.856992-04	22	hoaadsd	3		12	1
35	2021-10-02 23:14:41.865359-04	21	hola	3		12	1
36	2021-10-02 23:14:41.873767-04	20	ppppiuu	3		12	1
37	2021-10-02 23:14:41.882405-04	19	ppppiuu	3		12	1
38	2021-10-02 23:14:41.890455-04	18	ppppiuu	3		12	1
39	2021-10-02 23:14:41.898715-04	17	sprintaasdadad	3		12	1
40	2021-10-02 23:14:41.907155-04	16	sprintequipo10sm	3		12	1
41	2021-10-02 23:14:41.915629-04	15	sprintequipo10sm	3		12	1
42	2021-10-02 23:14:41.923789-04	14	sprintscrumequipo1	3		12	1
43	2021-10-02 23:14:41.932116-04	13	sprintscrumequipo1'	3		12	1
44	2021-10-02 23:14:41.940513-04	12	Sprint2edit	3		12	1
45	2021-10-02 23:14:41.948773-04	11	Sprint1	3		12	1
46	2021-10-02 23:45:00.380304-04	27	PorFin	3		12	1
47	2021-10-02 23:45:18.755997-04	15	rara	3		10	1
48	2021-10-02 23:45:18.768884-04	14	protestfinalaafat	3		10	1
49	2021-10-02 23:45:18.778131-04	13	protestfinalaafa	3		10	1
50	2021-10-02 23:45:18.785643-04	12	protestfinal	3		10	1
51	2021-10-02 23:45:18.794033-04	11	pp	3		10	1
52	2021-10-02 23:45:18.802451-04	10	p3	3		10	1
53	2021-10-02 23:45:18.810614-04	9	yyyeey	3		10	1
54	2021-10-02 23:45:18.819099-04	8	ooooooo	3		10	1
55	2021-10-02 23:45:18.827367-04	7	p1	3		10	1
56	2021-10-03 03:59:42.455687-03	28	sprint1	2	[{"deleted": {"name": "miembros sprint", "object": "mateo_giovanni"}}, {"deleted": {"name": "miembros sprint", "object": "maria"}}, {"deleted": {"name": "miembros sprint", "object": "mateo"}}]	12	1
57	2021-10-03 16:45:12.609614-03	29	sprint2	3		12	1
58	2021-10-03 17:16:23.249894-03	28	sprint1	2	[{"deleted": {"name": "miembros sprint", "object": "mateo"}}]	12	1
59	2021-10-03 19:08:00.067561-03	28	sprint1	2	[{"deleted": {"name": "miembros sprint", "object": "mateo_giovanni"}}]	12	1
60	2021-10-03 19:09:24.431806-03	28	sprint1	2	[{"added": {"name": "miembros sprint", "object": "maria"}}]	12	1
61	2021-10-03 19:10:10.067696-03	28	sprint1	2	[{"changed": {"name": "miembros sprint", "object": "maria", "fields": ["Team member"]}}]	12	1
62	2021-10-03 19:10:38.496246-03	28	sprint1	2	[{"deleted": {"name": "miembros sprint", "object": "maria"}}]	12	1
63	2021-10-03 19:11:01.144732-03	28	sprint1	2	[{"added": {"name": "miembros sprint", "object": "maria"}}]	12	1
64	2021-10-03 22:28:39.083751-03	1	UserStory object (1)	1	[{"added": {}}]	14	1
65	2021-10-04 01:05:30.092332-03	13	UserStory object (13)	3		14	1
66	2021-10-04 01:05:30.111607-03	12	UserStory object (12)	3		14	1
67	2021-10-04 01:05:30.119614-03	11	UserStory object (11)	3		14	1
68	2021-10-04 01:05:30.127887-03	10	UserStory object (10)	3		14	1
69	2021-10-04 01:05:30.136484-03	9	UserStory object (9)	3		14	1
70	2021-10-04 01:05:30.144645-03	8	UserStory object (8)	3		14	1
71	2021-10-04 01:05:30.15301-03	7	UserStory object (7)	3		14	1
72	2021-10-04 02:13:20.530337-03	21	UserStory object (21)	3		14	1
73	2021-10-04 02:13:20.549386-03	20	UserStory object (20)	3		14	1
74	2021-10-04 02:13:20.557982-03	19	UserStory object (19)	3		14	1
75	2021-10-04 13:47:55.284796-03	28	Asignar UserStory	1	[{"added": {}}]	9	1
76	2021-10-04 14:38:53.67653-03	17	Proyecto2	2	[{"deleted": {"name": "team member", "object": "mateo_giovanni"}}, {"deleted": {"name": "team member", "object": "mateo"}}, {"deleted": {"name": "team member", "object": "maria"}}]	10	1
77	2021-10-04 16:44:30.121656-03	32	sprint1	2	[{"deleted": {"name": "miembros sprint", "object": "admin"}}, {"deleted": {"name": "miembros sprint", "object": "mateo"}}]	12	1
78	2021-10-04 16:44:36.450452-03	32	sprint1	2	[{"deleted": {"name": "miembros sprint", "object": "maria"}}]	12	1
79	2021-10-04 16:48:44.145337-03	32	sprint1	2	[{"deleted": {"name": "miembros sprint", "object": "mateo"}}, {"deleted": {"name": "miembros sprint", "object": "maria"}}, {"deleted": {"name": "miembros sprint", "object": "mateo_giovanni"}}]	12	1
80	2021-10-05 01:35:56.224046-03	1	Crear Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
81	2021-10-05 01:36:04.194133-03	2	Editar Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
82	2021-10-05 01:36:10.586059-03	4	Crear Rol Sistema	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
83	2021-10-05 01:36:18.640869-03	5	Editar Rol Sistema	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
84	2021-10-05 01:36:30.875584-03	7	Editar Usuario	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
85	2021-10-05 01:36:55.378208-03	11	Activar Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
443	2021-11-18 15:52:00.234887-03	216	adasd	3		26	1
86	2021-10-05 01:37:17.771901-03	14	Crear Rol Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
87	2021-10-05 01:37:29.234034-03	16	Editar Rol Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
88	2021-10-05 01:37:43.997303-03	17	Cancelar Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
89	2021-10-05 01:37:50.244645-03	18	Suspender Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
90	2021-10-05 01:38:01.441668-03	19	Finalizar Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
91	2021-10-05 01:38:11.629557-03	21	Crear Sprint	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
92	2021-10-05 01:38:18.163701-03	22	Editar Sprint	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
93	2021-10-05 01:38:42.277387-03	24	Crear UserStory	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
94	2021-10-05 01:38:48.240165-03	25	Editar UserStory	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
95	2021-10-05 01:38:53.333492-03	26	Agregar Miembro al Sprint	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
96	2021-10-05 01:38:58.282039-03	27	Reemplazar Miembro	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
97	2021-10-05 01:39:03.30289-03	28	Asignar UserStory	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
98	2021-10-05 01:56:21.299763-03	13	Agregar Miembro al Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
99	2021-10-05 23:01:20.739232-03	18	UserStory object (18)	3		14	1
100	2021-10-05 23:01:20.864968-03	17	UserStory object (17)	3		14	1
101	2021-10-05 23:01:32.074204-03	25	UserStory object (25)	2	[{"changed": {"fields": ["Estado"]}}]	14	1
102	2021-10-05 23:10:22.042919-03	28	UserStory object (28)	2	[{"changed": {"fields": ["Estado"]}}]	14	1
103	2021-10-05 23:10:32.444328-03	27	UserStory object (27)	2	[{"changed": {"fields": ["Estado"]}}]	14	1
104	2021-10-05 23:10:41.430967-03	26	UserStory object (26)	2	[{"changed": {"fields": ["Estado"]}}]	14	1
105	2021-10-06 05:18:54.124431-03	30	UserStory object (30)	3		14	1
106	2021-10-06 05:19:02.024457-03	29	UserStory object (29)	3		14	1
107	2021-10-06 05:21:10.842475-03	26	USS1	2	[{"changed": {"fields": ["Proyecto"]}}]	14	1
108	2021-10-07 03:57:04.789315-03	37	sprint66666	2	[{"changed": {"fields": ["Fecha inicio", "Fecha fin"]}}]	12	1
109	2021-10-07 03:58:17.323365-03	37	sprint66666	2	[{"changed": {"fields": ["Dias habiles"]}}]	12	1
110	2021-10-07 04:08:29.012461-03	1	actividad1	1	[{"added": {}}]	19	1
111	2021-10-07 04:08:50.01789-03	2	actividad2	1	[{"added": {}}]	19	1
112	2021-10-07 04:09:21.142863-03	3	actividad3	1	[{"added": {}}]	19	1
113	2021-10-07 04:15:47.830656-03	4	actividad5	1	[{"added": {}}]	19	1
114	2021-10-07 21:05:31.499805-03	30	sp1	3		12	1
115	2021-10-07 21:05:53.656364-03	22	proyecto6	3		10	1
116	2021-10-07 21:06:19.367207-03	33	Sprint5	3		12	1
117	2021-10-07 21:06:40.314381-03	24	us3	3		14	1
118	2021-10-07 21:06:49.274882-03	25	us4	3		14	1
119	2021-10-07 21:06:56.599169-03	22	us1	3		14	1
120	2021-10-07 21:06:59.89682-03	23	us2	3		14	1
121	2021-10-07 21:07:04.607461-03	21	Proyecto5	3		10	1
122	2021-10-07 21:07:17.010022-03	32	sprint1	3		12	1
123	2021-10-07 21:07:27.959486-03	20	Proyecto4	3		10	1
124	2021-10-07 21:07:44.095393-03	31	Sprint1	3		12	1
125	2021-10-07 21:07:58.585332-03	26	USS1	3		14	1
126	2021-10-07 21:08:13.099821-03	19	Proyecto1	3		10	1
127	2021-10-07 21:08:20.590365-03	18	p3	3		10	1
128	2021-10-07 21:54:29.272308-03	42	usHOLAMUNDO	3		14	1
129	2021-10-07 21:54:29.287031-03	41	usfinalFINAL	3		14	1
130	2021-10-07 21:54:29.294953-03	40	us541414	3		14	1
131	2021-10-07 21:54:29.303526-03	39	us5aasdad	3		14	1
132	2021-10-07 21:54:29.311824-03	38	us3asdd	3		14	1
133	2021-10-07 21:54:29.320074-03	37	us2asdad	3		14	1
134	2021-10-07 21:54:29.328462-03	36	us1asd	3		14	1
135	2021-10-07 21:54:29.336765-03	34	dqdqdqdq	3		14	1
136	2021-10-07 21:54:29.345348-03	33	usqweqedwqqdqwdqd	3		14	1
137	2021-10-07 21:54:29.353503-03	32	daadassdadsada	3		14	1
138	2021-10-07 21:54:29.361694-03	31	us233441	3		14	1
139	2021-10-07 21:54:29.370177-03	28	US44	3		14	1
140	2021-10-07 21:54:29.3786-03	27	US2	3		14	1
141	2021-10-07 21:54:39.930977-03	39	holaasdf	3		12	1
142	2021-10-07 21:54:39.96994-03	38	SPRINTFINALFINLA	3		12	1
143	2021-10-07 21:54:39.978432-03	37	sprint66666	3		12	1
144	2021-10-07 21:54:39.986454-03	36	sprinTESTASD	3		12	1
145	2021-10-07 21:54:39.995042-03	35	sprintestUsuarioid	3		12	1
146	2021-10-07 21:54:40.003518-03	34	Sprint12345	3		12	1
147	2021-10-07 21:54:40.011535-03	28	sprint1	3		12	1
148	2021-10-07 21:54:50.231346-03	24	ProyectoParaDesarrollar	3		10	1
149	2021-10-07 21:54:50.269668-03	23	Proyecto7	3		10	1
150	2021-10-07 21:54:50.277973-03	17	Proyecto2	3		10	1
151	2021-10-08 03:16:37.936118-03	40	Sprint1	2	[{"changed": {"fields": ["Estado", "Fecha inicio", "Fecha fin"]}}]	12	1
152	2021-10-08 03:27:46.455322-03	29	Ver SprintBacklog	1	[{"added": {}}]	9	1
153	2021-10-08 03:28:03.949127-03	30	Ver ProductBacklog	1	[{"added": {}}]	9	1
154	2021-10-08 03:28:13.083162-03	31	Ver BurndownChart	1	[{"added": {}}]	9	1
155	2021-10-08 03:28:27.461699-03	32	Iniciar Sprint	1	[{"added": {}}]	9	1
156	2021-10-08 03:28:41.784881-03	33	Finalizar Sprint	1	[{"added": {}}]	9	1
157	2021-10-08 03:41:11.511838-03	78	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
158	2021-10-08 04:18:06.059565-03	31	Ver BurndownChar	2	[{"changed": {"fields": ["Nombre"]}}]	9	1
159	2021-10-08 04:18:20.487251-03	31	Ver BurndownChart	2	[{"changed": {"fields": ["Nombre"]}}]	9	1
160	2021-10-08 04:41:11.730287-03	34	Ver Kanban	1	[{"added": {}}]	9	1
161	2021-10-08 04:41:21.636928-03	78	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
162	2021-10-09 21:41:38.836633-03	1	holatestadasdada	3		18	1
163	2021-10-10 21:47:13.224608-03	55	userstory3	2	[{"changed": {"fields": ["Priorizacion"]}}]	14	1
164	2021-10-11 13:25:32.787983-03	138	proyecto_pruebas_unitarias2021-10-11 13:17:11.416948	3		10	1
165	2021-10-11 13:25:32.829349-03	136	proyecto_pruebas_unitarias2021-10-11 13:16:02.460696	3		10	1
166	2021-10-11 13:25:32.837885-03	126	proyecto_pruebas_unitaria00:00:00	3		10	1
167	2021-10-11 13:25:32.846029-03	125	proyecto_pruebas_unitarias00:00:00	3		10	1
168	2021-10-11 13:25:32.854293-03	120	proyecto_pruebas_unitarias<class 'datetime.time'>	3		10	1
169	2021-10-11 13:25:32.862841-03	117	proyecto_pruebas_unitarias0.8559276185701093	3		10	1
170	2021-10-11 13:25:32.871034-03	53	hola	3		10	1
171	2021-10-11 13:25:32.879264-03	52	  	3		10	1
172	2021-10-11 13:25:32.887695-03	50	asd	3		10	1
173	2021-10-11 13:25:32.896018-03	48		3		10	1
174	2021-10-11 13:25:32.904487-03	29	proyecto_pruebas_unitaria11	3		10	1
175	2021-10-11 13:25:32.91257-03	28	proyecto_pruebas_unitarias20	3		10	1
176	2021-10-15 15:45:26.860957-03	110	Sprint1	2	[{"changed": {"fields": ["Estado"]}}]	12	1
177	2021-10-16 21:32:16.214481-03	35	Ver Control de Calidad	1	[{"added": {}}]	9	1
178	2021-10-16 21:32:38.000059-03	84	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
179	2021-10-16 21:32:53.18439-03	81	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
180	2021-10-16 21:33:10.867663-03	87	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
181	2021-10-16 21:33:40.739759-03	78	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
182	2021-10-17 00:05:52.890299-03	97	usfinaizado1	2	[{"changed": {"fields": ["Estado"]}}]	14	1
183	2021-10-17 00:06:00.5847-03	98	usfinaizado2	2	[{"changed": {"fields": ["Estado"]}}]	14	1
184	2021-10-17 00:06:08.967723-03	99	usfinaizado3	2	[{"changed": {"fields": ["Estado"]}}]	14	1
185	2021-10-17 00:06:20.703829-03	100	usfinaizado1	2	[{"changed": {"fields": ["Estado"]}}]	14	1
186	2021-10-17 00:06:31.20238-03	101	usfinaizado2	2	[{"changed": {"fields": ["Estado"]}}]	14	1
187	2021-10-17 00:06:41.414592-03	102	usfinaizado3	2	[{"changed": {"fields": ["Estado"]}}]	14	1
188	2021-10-17 00:06:51.565888-03	103	usfinaizado1	2	[{"changed": {"fields": ["Estado"]}}]	14	1
189	2021-10-17 00:07:01.23783-03	104	usfinaizado2	2	[{"changed": {"fields": ["Estado"]}}]	14	1
190	2021-10-17 00:07:08.640921-03	105	usfinaizado1dada	2	[{"changed": {"fields": ["Estado"]}}]	14	1
191	2021-10-17 00:07:17.150699-03	106	usfinaizado13	2	[{"changed": {"fields": ["Estado"]}}]	14	1
192	2021-10-17 00:07:27.814995-03	107	usfinaizado1adad	2	[{"changed": {"fields": ["Estado"]}}]	14	1
193	2021-10-22 03:05:37.511533-03	114	Sprint 3	2	[{"changed": {"fields": ["Estado"]}}]	12	1
194	2021-10-22 03:18:58.084807-03	118	ussprint31	2	[{"changed": {"fields": ["Horas trabajadas us", "Porcentaje realizado"]}}]	14	1
195	2021-10-22 03:19:05.750638-03	119	ussprint32	2	[{"changed": {"fields": ["Horas trabajadas us", "Porcentaje realizado"]}}]	14	1
196	2021-10-22 03:19:13.098631-03	120	ussprint33	2	[{"changed": {"fields": ["Horas trabajadas us", "Porcentaje realizado"]}}]	14	1
197	2021-10-22 03:47:36.427897-03	118	ussprint31	2	[{"changed": {"fields": ["Horas extras trabajadas us"]}}]	14	1
198	2021-10-22 03:47:44.557481-03	119	ussprint32	2	[{"changed": {"fields": ["Horas extras trabajadas us"]}}]	14	1
199	2021-10-22 03:47:52.611295-03	120	ussprint33	2	[{"changed": {"fields": ["Horas extras trabajadas us"]}}]	14	1
200	2021-10-22 04:04:29.598489-03	120	ussprint33	2	[{"changed": {"fields": ["Duracion restante", "Horas trabajadas us", "Porcentaje realizado"]}}]	14	1
201	2021-10-22 04:04:42.209809-03	119	ussprint32	2	[{"changed": {"fields": ["Duracion restante"]}}]	14	1
202	2021-10-22 04:04:55.584176-03	118	ussprint31	2	[{"changed": {"fields": ["Duracion restante", "Horas trabajadas us", "Porcentaje realizado"]}}]	14	1
203	2021-10-22 04:05:25.152854-03	46	act4	3		19	1
204	2021-10-22 04:05:25.171307-03	45	act3	3		19	1
205	2021-10-22 04:05:25.179575-03	44	act2	3		19	1
206	2021-10-22 04:05:25.187853-03	43	act1	3		19	1
207	2021-10-22 04:05:25.196193-03	42	act 2	3		19	1
208	2021-10-22 04:05:25.204813-03	41	act1	3		19	1
209	2021-10-22 04:08:19.436336-03	118	ussprint31	2	[{"changed": {"fields": ["Duracion restante"]}}]	14	1
210	2021-10-22 04:08:27.914046-03	119	ussprint32	2	[{"changed": {"fields": ["Duracion restante"]}}]	14	1
211	2021-10-22 04:08:34.72622-03	120	ussprint33	2	[{"changed": {"fields": ["Duracion restante"]}}]	14	1
212	2021-10-22 04:10:50.479124-03	118	ussprint31	2	[{"changed": {"fields": ["Duracion restante"]}}]	14	1
213	2021-10-22 04:10:56.212415-03	118	ussprint31	2	[]	14	1
214	2021-10-22 04:11:02.571741-03	119	ussprint32	2	[{"changed": {"fields": ["Duracion restante"]}}]	14	1
215	2021-10-22 04:11:09.736156-03	120	ussprint33	2	[{"changed": {"fields": ["Duracion restante"]}}]	14	1
216	2021-10-23 21:46:47.97726-03	114	Sprint 3	2	[{"changed": {"fields": ["Estado"]}}]	12	1
217	2021-10-23 23:47:27.850951-03	114	Sprint 3	2	[{"changed": {"fields": ["Estado"]}}]	12	1
218	2021-10-24 21:57:14.548683-03	123	us3	2	[{"changed": {"fields": ["Estado", "Estado tablero"]}}]	14	1
219	2021-10-24 22:19:51.327911-03	123	us3	2	[{"changed": {"fields": ["Estado", "Estado tablero"]}}]	14	1
220	2021-10-25 00:07:20.545875-03	127	us3	2	[{"changed": {"fields": ["Estado", "Estado tablero"]}}]	14	1
221	2021-10-25 00:10:59.621324-03	127	us3	2	[{"changed": {"fields": ["Estado", "Estado tablero"]}}]	14	1
222	2021-10-25 00:30:26.800037-03	127	us3	2	[{"changed": {"fields": ["Estado", "Estado tablero"]}}]	14	1
223	2021-10-25 00:41:55.68391-03	127	us3	2	[{"changed": {"fields": ["Estado"]}}]	14	1
224	2021-10-25 00:46:25.801536-03	127	us3	2	[{"changed": {"fields": ["Estado tablero"]}}]	14	1
225	2021-10-26 02:42:23.039284-03	130	us5	2	[{"changed": {"fields": ["Estado"]}}]	14	1
226	2021-10-26 02:42:54.062813-03	123	sprintn	2	[{"changed": {"fields": ["Estado"]}}]	12	1
227	2021-10-28 00:20:58.031877-03	131	us6	2	[{"changed": {"fields": ["Estado", "Motivo comentario", "Estado qa"]}}]	14	1
228	2021-10-28 00:23:06.055585-03	131	us6	2	[{"changed": {"fields": ["Estado", "Motivo comentario", "Estado qa"]}}]	14	1
229	2021-10-28 00:25:19.699325-03	131	us6	2	[{"changed": {"fields": ["Estado", "Motivo comentario", "Estado qa"]}}]	14	1
230	2021-10-28 00:36:42.183972-03	1	admin	2	[{"changed": {"fields": ["Email address"]}}]	5	1
231	2021-10-28 00:37:15.821392-03	131	us6	2	[{"changed": {"fields": ["Estado", "Motivo comentario", "Estado qa"]}}]	14	1
232	2021-10-28 00:50:42.617801-03	131	us6	2	[{"changed": {"fields": ["Estado", "Motivo comentario", "Estado qa"]}}]	14	1
233	2021-10-28 00:57:45.472709-03	131	us6	2	[{"changed": {"fields": ["Estado", "Motivo comentario", "Estado qa"]}}]	14	1
234	2021-10-28 03:00:24.240778-03	36	Ver Actividades	1	[{"added": {}}]	9	1
235	2021-10-28 03:10:51.494943-03	93	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
236	2021-10-28 05:27:52.015363-03	158	ProyectoIteracion5	2	[{"changed": {"fields": ["Estado"]}}]	10	1
237	2021-10-28 05:31:42.670206-03	158	ProyectoIteracion5	2	[{"changed": {"fields": ["Estado"]}}]	10	1
238	2021-10-28 19:30:42.096738-03	134	usSprintN2-2	2	[{"changed": {"fields": ["Estado tablero"]}}]	14	1
239	2021-10-28 22:04:45.639243-03	158	ProyectoIteracion5	2	[{"changed": {"fields": ["Estado"]}}]	10	1
240	2021-10-28 22:26:44.011174-03	158	ProyectoIteracion5	2	[{"changed": {"fields": ["Estado"]}}]	10	1
241	2021-10-28 22:27:42.369523-03	158	ProyectoIteracion5	2	[{"changed": {"fields": ["Estado"]}}]	10	1
242	2021-10-28 22:36:03.428875-03	158	ProyectoIteracion5	2	[{"changed": {"fields": ["Estado"]}}]	10	1
243	2021-10-29 00:37:47.084819-03	161	2021-10-28 19:20:30.186201	3		10	1
444	2021-11-18 15:52:00.268497-03	215	dadad	3		26	1
244	2021-10-29 00:37:47.100508-03	160	proyecto_pruebas_unitarias2021-10-28 19:20:29.816440	3		10	1
245	2021-10-29 00:38:33.985123-03	121	us1	3		14	1
246	2021-10-29 00:38:40.055948-03	122	us2	3		14	1
247	2021-10-29 00:38:43.534198-03	123	us3	3		14	1
248	2021-10-29 00:38:47.598381-03	124	us4	3		14	1
249	2021-10-29 00:39:07.397253-03	115	Sprint1	3		12	1
250	2021-10-29 00:39:14.998274-03	116	Sprint 2	3		12	1
251	2021-10-29 00:39:20.424641-03	157	ProyectoTest3	3		10	1
252	2021-10-29 00:40:01.792612-03	100	usfinaizado1	3		14	1
253	2021-10-29 00:40:05.805925-03	101	usfinaizado2	3		14	1
254	2021-10-29 00:40:09.328353-03	102	usfinaizado3	3		14	1
255	2021-10-29 00:40:13.034468-03	108	us1	3		14	1
256	2021-10-29 00:40:16.824403-03	110	us2	3		14	1
257	2021-10-29 00:40:42.971334-03	111	us3	3		14	1
258	2021-10-29 00:40:46.608634-03	112	us4hist	3		14	1
259	2021-10-29 00:40:50.667531-03	113	us5hist	3		14	1
260	2021-10-29 00:40:54.3317-03	114	usTestPutHistorialESTADOtablero	3		14	1
261	2021-10-29 00:40:57.943347-03	115	usActividadhiSTO1	3		14	1
262	2021-10-29 00:41:02.56686-03	116	usActividadhiSTO2	3		14	1
263	2021-10-29 00:41:06.515977-03	117	usActividadhiSTO3	3		14	1
264	2021-10-29 00:41:10.618898-03	118	ussprint31	3		14	1
265	2021-10-29 00:41:15.112076-03	119	ussprint32	3		14	1
266	2021-10-29 00:41:18.796856-03	120	ussprint33	3		14	1
267	2021-10-29 00:41:31.952061-03	113	Sprint2	3		12	1
268	2021-10-29 00:41:35.88651-03	114	Sprint 3	3		12	1
269	2021-10-29 00:41:39.576609-03	109	Sprint1	3		12	1
270	2021-10-29 00:41:45.446484-03	148	ProyectoTest2	3		10	1
271	2021-10-29 00:42:24.676656-03	92	us4	3		14	1
272	2021-10-29 00:42:27.963348-03	93	us5	3		14	1
273	2021-10-29 00:42:31.366303-03	96	us10	3		14	1
274	2021-10-29 00:42:34.958773-03	97	usfinaizado1	3		14	1
275	2021-10-29 00:42:39.296955-03	98	usfinaizado2	3		14	1
276	2021-10-29 00:42:43.048109-03	99	usfinaizado3	3		14	1
277	2021-10-29 00:42:52.42291-03	111	Sprint2	3		12	1
278	2021-10-29 00:42:56.561772-03	112	Sprint3	3		12	1
279	2021-10-29 00:43:17.687823-03	90	us2	3		14	1
280	2021-10-29 00:43:21.212197-03	89	us1	3		14	1
281	2021-10-29 00:43:40.148474-03	94	us8	3		14	1
282	2021-10-29 00:43:43.361885-03	91	us3	3		14	1
283	2021-10-29 00:43:47.066396-03	95	us9	3		14	1
284	2021-10-29 00:43:56.564889-03	110	Sprint1	3		12	1
285	2021-10-29 00:44:00.851172-03	147	ProyectoTest1	3		10	1
286	2021-10-29 00:44:29.435489-03	105	usfinaizado1dada	3		14	1
287	2021-10-29 00:44:33.225289-03	53	userStory1	3		14	1
288	2021-10-29 00:44:36.864307-03	54	userStory2	3		14	1
289	2021-10-29 00:44:40.846089-03	55	userstory3	3		14	1
290	2021-10-29 00:44:44.07712-03	106	usfinaizado13	3		14	1
291	2021-10-29 00:44:49.047967-03	107	usfinaizado1adad	3		14	1
292	2021-10-29 00:46:19.010285-03	104	usfinaizado2	3		14	1
293	2021-10-29 00:46:19.129192-03	103	usfinaizado1	3		14	1
294	2021-10-29 00:46:19.138494-03	88	us8	3		14	1
295	2021-10-29 00:46:19.146821-03	87	us7asdf	3		14	1
296	2021-10-29 00:46:19.15524-03	86	us6	3		14	1
297	2021-10-29 00:46:19.163577-03	85	us5Iteracion4	3		14	1
298	2021-10-29 00:46:19.172341-03	77	us4	3		14	1
299	2021-10-29 00:46:19.180362-03	76	us3	3		14	1
300	2021-10-29 00:46:19.188448-03	75	us2	3		14	1
301	2021-10-29 00:46:19.196823-03	74	us1	3		14	1
302	2021-10-29 00:46:19.205256-03	48	us6	3		14	1
303	2021-10-29 00:46:19.213535-03	47	us5	3		14	1
304	2021-10-29 00:46:19.221835-03	46	us4	3		14	1
305	2021-10-29 00:46:19.230328-03	45	us4	3		14	1
306	2021-10-29 00:46:19.238829-03	44	us2	3		14	1
307	2021-10-29 00:46:19.247149-03	43	us1	3		14	1
308	2021-10-29 00:47:07.298195-03	108	Sprint1TestEditado	3		12	1
309	2021-10-29 00:47:07.368954-03	107	SprintFiNAL	3		12	1
310	2021-10-29 00:47:07.377095-03	106	sprint1	3		12	1
311	2021-10-29 00:47:07.385382-03	105	SprintIteracin1	3		12	1
312	2021-10-29 00:47:07.393634-03	101	Sprint1	3		12	1
313	2021-10-29 00:47:07.401843-03	45	SprinTestasd	3		12	1
314	2021-10-29 00:47:07.410242-03	44	Sprint5	3		12	1
315	2021-10-29 00:47:07.418561-03	43	Sprint4	3		12	1
316	2021-10-29 00:47:07.42693-03	42	Sprint3	3		12	1
317	2021-10-29 00:47:07.43551-03	41	Sprint2	3		12	1
318	2021-10-29 00:47:07.443756-03	40	Sprint1	3		12	1
319	2021-10-29 00:47:33.745666-03	146	ProyectoIteracion4	3		10	1
320	2021-10-29 00:47:33.787917-03	25	ProyectoParaDesarrollarE	3		10	1
321	2021-10-29 01:19:53.552077-03	97	Developer	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
322	2021-10-29 01:20:46.966952-03	98	ProductOwner	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
323	2021-10-29 01:32:16.830759-03	2	mateo_giovanni	2	[{"changed": {"fields": ["Rol de Sistema"]}}]	17	1
324	2021-10-29 01:32:21.931589-03	5	maria	2	[]	17	1
325	2021-10-29 01:55:26.233782-03	127	Sprint1	2	[{"changed": {"fields": ["Estado"]}}]	12	1
326	2021-10-29 01:57:54.683371-03	127	Sprint1	2	[{"changed": {"fields": ["Estado", "Fecha fin"]}}]	12	1
327	2021-11-13 14:31:15.420005-03	130	ssadas	3		12	1
328	2021-11-13 14:31:44.462314-03	131	s	3		12	1
329	2021-11-13 19:28:58.437471-03	129	Sprint1T	2	[{"changed": {"fields": ["Estado"]}}]	12	1
330	2021-11-13 19:29:16.387977-03	134	adsadad	2	[{"changed": {"fields": ["Estado"]}}]	12	1
331	2021-11-13 22:37:01.792177-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
332	2021-11-13 22:43:06.566808-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
333	2021-11-13 22:46:49.669733-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
334	2021-11-13 22:50:03.370473-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
335	2021-11-13 22:50:29.615763-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
336	2021-11-13 22:51:03.037557-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
337	2021-11-13 22:52:00.989115-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
338	2021-11-13 23:11:15.91476-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
339	2021-11-13 23:57:02.807509-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
340	2021-11-13 23:57:23.323633-03	163	Proyecto-SprintTest	2	[]	10	1
341	2021-11-13 23:57:37.439551-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
342	2021-11-14 00:05:17.672258-03	163	Proyecto-SprintTest	2	[{"changed": {"fields": ["Estado"]}}]	10	1
343	2021-11-14 00:32:23.105369-03	37	Aprobar UserStory	1	[{"added": {}}]	9	1
445	2021-11-18 15:52:00.276876-03	214	dadad	3		26	1
344	2021-11-14 00:32:47.692029-03	38	Rechazar UserStory	1	[{"added": {}}]	9	1
345	2021-11-14 00:33:44.844109-03	99	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
346	2021-11-14 00:33:57.068158-03	96	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
347	2021-11-14 00:34:09.896294-03	93	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
348	2021-11-14 00:44:13.97792-03	39	Generar Reportes	1	[{"added": {}}]	9	1
349	2021-11-14 00:44:38.012879-03	93	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
350	2021-11-14 00:44:44.688007-03	96	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
351	2021-11-14 00:44:50.904862-03	99	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
352	2021-11-14 11:54:34.161189-03	143	us2T	2	[{"changed": {"fields": ["Estado"]}}]	14	1
353	2021-11-14 11:55:02.253409-03	144	us3T	2	[{"changed": {"fields": ["Estado"]}}]	14	1
354	2021-11-14 11:59:16.396875-03	143	us2T	2	[]	14	1
355	2021-11-14 11:59:35.32305-03	143	us2T	2	[{"changed": {"fields": ["Estado"]}}]	14	1
356	2021-11-17 22:43:12.629959-03	40	Eliminar UserStory	1	[{"added": {}}]	9	1
357	2021-11-17 22:43:32.134354-03	41	Eliminar Proyecto	1	[{"added": {}}]	9	1
358	2021-11-17 22:43:40.484856-03	9	Ver Definicion de Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
359	2021-11-17 22:43:47.59642-03	10	Ver Ejecucion de Proyecto	2	[{"changed": {"fields": ["Es seleccionable"]}}]	9	1
360	2021-11-17 22:48:11.525851-03	93	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
361	2021-11-17 22:48:24.224428-03	96	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
362	2021-11-17 22:49:28.544164-03	99	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
363	2021-11-18 12:08:02.069297-03	42	Eliminar Rol Proyecto	1	[{"added": {}}]	9	1
364	2021-11-18 12:09:56.526781-03	93	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
365	2021-11-18 12:10:07.08764-03	96	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
366	2021-11-18 12:10:14.190369-03	99	ScrumMaster	2	[{"changed": {"fields": ["Permisos"]}}]	11	1
367	2021-11-18 13:00:01.88819-03	1	Administrador	2	[{"changed": {"fields": ["Permisos"]}}]	16	1
368	2021-11-18 13:00:45.711653-03	41	Eliminar Proyecto	2	[{"changed": {"fields": ["Tipo permiso"]}}]	9	1
369	2021-11-18 13:00:51.469487-03	42	Eliminar Rol Proyecto	2	[{"changed": {"fields": ["Tipo permiso"]}}]	9	1
370	2021-11-18 13:01:13.324297-03	42	Eliminar Rol Proyecto	2	[{"changed": {"fields": ["Tipo permiso"]}}]	9	1
371	2021-11-18 13:02:09.259266-03	1	Administrador	2	[{"changed": {"fields": ["Permisos"]}}]	16	1
372	2021-11-18 13:07:21.538855-03	158	us1	3		14	1
373	2021-11-18 13:08:17.416372-03	157	us1	3		14	1
374	2021-11-18 13:48:42.199961-03	43	Eliminar Rol Sistema	1	[{"added": {}}]	9	1
375	2021-11-18 13:49:21.027296-03	1	Administrador	2	[{"changed": {"fields": ["Permisos"]}}]	16	1
376	2021-11-18 14:50:00.093842-03	9	Ver Definicion de Proyecto	3		9	1
377	2021-11-18 15:51:21.33695-03	174	ProyectoPendiente	3		10	1
378	2021-11-18 15:51:41.753771-03	277	dadad	3		26	1
379	2021-11-18 15:51:59.692342-03	281	asdad	3		26	1
380	2021-11-18 15:51:59.709529-03	280	asdad	3		26	1
381	2021-11-18 15:51:59.717913-03	279	asdad	3		26	1
382	2021-11-18 15:51:59.72621-03	278	asdad	3		26	1
383	2021-11-18 15:51:59.734394-03	276	asdad	3		26	1
384	2021-11-18 15:51:59.742909-03	275	asdad	3		26	1
385	2021-11-18 15:51:59.751355-03	274	us6	3		26	1
386	2021-11-18 15:51:59.759503-03	273	us6	3		26	1
387	2021-11-18 15:51:59.767965-03	272	us6	3		26	1
388	2021-11-18 15:51:59.776461-03	271	us6	3		26	1
389	2021-11-18 15:51:59.784597-03	270	us6	3		26	1
390	2021-11-18 15:51:59.792913-03	269	usSprintN2-1	3		26	1
391	2021-11-18 15:51:59.801422-03	268	usSprintN2-1	3		26	1
392	2021-11-18 15:51:59.809701-03	267	usSprintN2-1	3		26	1
393	2021-11-18 15:51:59.818016-03	266	usSprintN2-1	3		26	1
394	2021-11-18 15:51:59.826178-03	265	usSprintN2-2	3		26	1
395	2021-11-18 15:51:59.834889-03	264	usSprintN2-1	3		26	1
396	2021-11-18 15:51:59.842871-03	263	adasd	3		26	1
397	2021-11-18 15:51:59.851854-03	262	dadad	3		26	1
398	2021-11-18 15:51:59.860797-03	261	dadad	3		26	1
399	2021-11-18 15:51:59.867928-03	260	adasd	3		26	1
400	2021-11-18 15:51:59.876216-03	259	dadad	3		26	1
401	2021-11-18 15:51:59.884998-03	258	dadad	3		26	1
402	2021-11-18 15:51:59.895003-03	257	adasd	3		26	1
403	2021-11-18 15:51:59.901237-03	256	adasd	3		26	1
404	2021-11-18 15:51:59.910009-03	255	dadad	3		26	1
405	2021-11-18 15:51:59.919412-03	254	dadad	3		26	1
406	2021-11-18 15:51:59.926604-03	253	adasd	3		26	1
407	2021-11-18 15:51:59.934557-03	252	dadad	3		26	1
408	2021-11-18 15:51:59.942939-03	251	adasd	3		26	1
409	2021-11-18 15:51:59.951316-03	250	dadad	3		26	1
410	2021-11-18 15:51:59.959628-03	249	dadad	3		26	1
411	2021-11-18 15:51:59.968311-03	248	dadad	3		26	1
412	2021-11-18 15:51:59.976335-03	247	adasd	3		26	1
413	2021-11-18 15:51:59.98465-03	246	dadad	3		26	1
414	2021-11-18 15:51:59.993036-03	245	adasd	3		26	1
415	2021-11-18 15:52:00.001364-03	244	dadad	3		26	1
416	2021-11-18 15:52:00.009711-03	243	dadad	3		26	1
417	2021-11-18 15:52:00.018219-03	242	dadad	3		26	1
418	2021-11-18 15:52:00.026387-03	241	adasd	3		26	1
419	2021-11-18 15:52:00.034866-03	240	adasd	3		26	1
420	2021-11-18 15:52:00.043086-03	239	dadad	3		26	1
421	2021-11-18 15:52:00.051417-03	238	dadad	3		26	1
422	2021-11-18 15:52:00.059821-03	237	dadad	3		26	1
423	2021-11-18 15:52:00.068239-03	236	adasd	3		26	1
424	2021-11-18 15:52:00.076544-03	235	dadad	3		26	1
425	2021-11-18 15:52:00.084719-03	234	adasd	3		26	1
426	2021-11-18 15:52:00.09305-03	233	dadad	3		26	1
427	2021-11-18 15:52:00.101589-03	232	dadad	3		26	1
428	2021-11-18 15:52:00.109778-03	231	dadad	3		26	1
429	2021-11-18 15:52:00.118112-03	230	adasd	3		26	1
430	2021-11-18 15:52:00.126437-03	229	dadad	3		26	1
431	2021-11-18 15:52:00.138809-03	228	dadad	3		26	1
432	2021-11-18 15:52:00.143064-03	227	adasd	3		26	1
433	2021-11-18 15:52:00.151877-03	226	adasd	3		26	1
434	2021-11-18 15:52:00.160021-03	225	dadad	3		26	1
435	2021-11-18 15:52:00.168426-03	224	dadad	3		26	1
436	2021-11-18 15:52:00.176577-03	223	adasd	3		26	1
437	2021-11-18 15:52:00.18521-03	222	adasd	3		26	1
438	2021-11-18 15:52:00.193182-03	221	dadad	3		26	1
439	2021-11-18 15:52:00.20161-03	220	adasd	3		26	1
440	2021-11-18 15:52:00.20994-03	219	dadad	3		26	1
441	2021-11-18 15:52:00.218478-03	218	dadad	3		26	1
442	2021-11-18 15:52:00.226848-03	217	adasd	3		26	1
446	2021-11-18 15:52:00.285326-03	213	dadad	3		26	1
447	2021-11-18 15:52:00.293755-03	212	adasd	3		26	1
448	2021-11-18 15:52:00.302081-03	211	dadad	3		26	1
449	2021-11-18 15:52:00.310125-03	210	dadad	3		26	1
450	2021-11-18 15:52:00.31858-03	209	dadad	3		26	1
451	2021-11-18 15:52:00.327017-03	208	dadad	3		26	1
452	2021-11-18 15:52:00.335411-03	207	dadad	3		26	1
453	2021-11-18 15:52:00.343487-03	206	adasd	3		26	1
454	2021-11-18 15:52:00.352183-03	205	dadad	3		26	1
455	2021-11-18 15:52:00.360383-03	204	adasd	3		26	1
456	2021-11-18 15:52:00.368889-03	203	adasd	3		26	1
457	2021-11-18 15:52:00.376918-03	202	dadad	3		26	1
458	2021-11-18 15:52:00.385182-03	201	dadad	3		26	1
459	2021-11-18 15:52:00.393739-03	200	adasd	3		26	1
460	2021-11-18 15:52:00.402086-03	199	adasd	3		26	1
461	2021-11-18 15:52:00.410488-03	198	dadad	3		26	1
462	2021-11-18 15:52:00.418573-03	197	adasd	3		26	1
463	2021-11-18 15:52:00.427449-03	196	dadad	3		26	1
464	2021-11-18 15:52:00.435431-03	195	dadad	3		26	1
465	2021-11-18 15:52:00.443758-03	194	adasd	3		26	1
466	2021-11-18 15:52:00.451943-03	193	adasd	3		26	1
467	2021-11-18 15:52:00.460735-03	192	adasd	3		26	1
468	2021-11-18 15:52:00.468921-03	191	adasd	3		26	1
469	2021-11-18 15:52:00.476945-03	190	dadad	3		26	1
470	2021-11-18 15:52:00.485658-03	189	dadad	3		26	1
471	2021-11-18 15:52:00.493627-03	188	dadad	3		26	1
472	2021-11-18 15:52:00.501992-03	187	adasd	3		26	1
473	2021-11-18 15:52:00.510403-03	186	dadad	3		26	1
474	2021-11-18 15:52:00.520151-03	185	dadad	3		26	1
475	2021-11-18 15:52:00.527151-03	184	dadad	3		26	1
476	2021-11-18 15:52:00.536318-03	183	dadad	3		26	1
477	2021-11-18 15:52:00.543742-03	182	dadad	3		26	1
478	2021-11-18 15:52:00.552325-03	181	adasd	3		26	1
479	2021-11-18 15:52:16.655791-03	180	dadad	3		26	1
480	2021-11-18 15:52:16.696547-03	179	dadad	3		26	1
481	2021-11-18 15:52:16.703709-03	178	dadad	3		26	1
482	2021-11-18 15:52:16.74546-03	177	dadad	3		26	1
483	2021-11-18 15:52:16.753951-03	176	dadad	3		26	1
484	2021-11-18 15:52:16.762574-03	175	dadad	3		26	1
485	2021-11-18 15:52:16.770746-03	174	dadad	3		26	1
486	2021-11-18 15:52:16.779066-03	173	dadad	3		26	1
487	2021-11-18 15:52:16.787187-03	172	dadad	3		26	1
488	2021-11-18 15:52:16.795518-03	171	dadad	3		26	1
489	2021-11-18 15:52:16.803976-03	170	dadad	3		26	1
490	2021-11-18 15:52:16.81266-03	169	dadad	3		26	1
491	2021-11-18 15:52:16.824678-03	168	dadad	3		26	1
492	2021-11-18 15:52:16.837264-03	167	dadad	3		26	1
493	2021-11-18 15:52:16.845643-03	166	dadad	3		26	1
494	2021-11-18 15:52:16.854077-03	165	adasd	3		26	1
495	2021-11-18 15:52:16.863829-03	164	us1T	3		26	1
496	2021-11-18 15:52:16.870794-03	163	us2T	3		26	1
497	2021-11-18 15:52:16.879129-03	162	us3T	3		26	1
498	2021-11-18 15:52:16.887549-03	161	us2T	3		26	1
499	2021-11-18 15:52:16.895732-03	160	us1T	3		26	1
500	2021-11-18 15:52:16.904025-03	159	us3T	3		26	1
501	2021-11-18 15:52:16.912608-03	158	userstorie4	3		26	1
502	2021-11-18 15:52:16.921486-03	157	userstorie4	3		26	1
503	2021-11-18 15:52:16.929029-03	156	userstorie4	3		26	1
504	2021-11-18 15:52:16.937483-03	155	userstorie4	3		26	1
505	2021-11-18 15:52:16.945631-03	154	userstorie4	3		26	1
506	2021-11-18 15:52:16.95393-03	153	userstorie4	3		26	1
507	2021-11-18 15:52:16.962318-03	152	userstorie4	3		26	1
508	2021-11-18 15:52:16.970833-03	151	userstorie4	3		26	1
509	2021-11-18 15:52:16.979314-03	150	userstorie4	3		26	1
510	2021-11-18 15:52:16.987623-03	149	us2T	3		26	1
511	2021-11-18 15:52:16.995824-03	148	us1T	3		26	1
512	2021-11-18 15:52:17.004482-03	147	us3T	3		26	1
513	2021-11-18 15:52:17.012582-03	146	us3T	3		26	1
514	2021-11-18 15:52:17.021104-03	145	us2T	3		26	1
515	2021-11-18 15:52:17.029262-03	144	us1T	3		26	1
516	2021-11-18 15:52:17.046815-03	143	us1T	3		26	1
517	2021-11-18 15:52:17.055265-03	142	us3T	3		26	1
518	2021-11-18 15:52:17.06275-03	141	us2T	3		26	1
519	2021-11-18 15:52:17.072425-03	140	us3T	3		26	1
520	2021-11-18 15:52:17.079158-03	139	us1T	3		26	1
521	2021-11-18 15:52:17.095957-03	138	us2T	3		26	1
522	2021-11-18 15:52:17.104923-03	137	us1T	3		26	1
523	2021-11-18 15:52:17.112497-03	136	us1T	3		26	1
524	2021-11-18 15:52:17.12078-03	135	us1T	3		26	1
525	2021-11-18 15:52:17.129409-03	134	us1T	3		26	1
526	2021-11-18 15:52:17.137548-03	133	userstorie4	3		26	1
527	2021-11-18 15:52:17.1459-03	132	userstorie4	3		26	1
528	2021-11-18 15:52:17.15498-03	131	userstorie2	3		26	1
529	2021-11-18 15:52:17.162838-03	130	userstorie4	3		26	1
530	2021-11-18 15:52:17.171098-03	129	userstorie2	3		26	1
531	2021-11-18 15:52:17.17942-03	128	userstorie3	3		26	1
532	2021-11-18 15:52:17.187629-03	127	userstorie1	3		26	1
533	2021-11-18 15:52:17.19596-03	126	userstorie2	3		26	1
534	2021-11-18 15:52:17.205355-03	125	userstorie3	3		26	1
535	2021-11-18 15:52:17.212925-03	124	userstorie1	3		26	1
536	2021-11-18 15:52:17.222289-03	123	userstorie2	3		26	1
537	2021-11-18 15:52:17.229442-03	122	userstorie3	3		26	1
538	2021-11-18 15:52:17.237892-03	121	userstorie2	3		26	1
539	2021-11-18 15:52:17.245947-03	120	userstorie1	3		26	1
540	2021-11-18 15:52:17.262661-03	119	usSprintN2-2	3		26	1
541	2021-11-18 15:52:17.27131-03	118	usSprintN2-2	3		26	1
542	2021-11-18 15:52:17.279382-03	117	usSprintN2-1	3		26	1
543	2021-11-18 15:52:17.287529-03	116	usSprintN2-2	3		26	1
544	2021-11-18 15:52:17.312721-03	115	usSprintN2-2	3		26	1
545	2021-11-18 15:52:17.346226-03	114	usSprintN2-1	3		26	1
546	2021-11-18 15:52:17.354156-03	113	usSprintN2-2	3		26	1
547	2021-11-18 15:52:17.362622-03	112	usSprintN2-1	3		26	1
548	2021-11-18 15:52:17.370818-03	111	usSprintN2-1	3		26	1
549	2021-11-18 15:52:17.38066-03	110	usSprintN2-2	3		26	1
550	2021-11-18 15:52:17.387597-03	109	usSprintN2-1	3		26	1
551	2021-11-18 15:52:17.396613-03	108	usSprintN2-2	3		26	1
552	2021-11-18 15:52:17.404528-03	107	usSprintN2-2	3		26	1
553	2021-11-18 15:52:17.41309-03	106	usSprintN2-2	3		26	1
554	2021-11-18 15:52:17.429397-03	105	usSprintN2-1	3		26	1
555	2021-11-18 15:52:17.437577-03	104	usSprintN2-2	3		26	1
556	2021-11-18 15:52:17.445986-03	103	usSprintN2-2	3		26	1
557	2021-11-18 15:52:17.454208-03	102	usSprintN2-1	3		26	1
558	2021-11-18 15:52:17.462646-03	101	usSprintN2-2	3		26	1
559	2021-11-18 15:52:17.471009-03	100	usSprintN2-2	3		26	1
560	2021-11-18 15:52:17.479407-03	99	usSprintN2-1	3		26	1
561	2021-11-18 15:52:17.642879-03	98	usSprintN2-2	3		26	1
562	2021-11-18 15:52:17.762635-03	97	usSprintN2-2	3		26	1
563	2021-11-18 15:52:17.921246-03	96	usSprintN2-1	3		26	1
564	2021-11-18 15:52:17.929409-03	95	usSprintN2-2	3		26	1
565	2021-11-18 15:52:17.937875-03	94	usSprintN2-2	3		26	1
566	2021-11-18 15:52:17.945997-03	93	usSprintN2-2	3		26	1
567	2021-11-18 15:52:17.954343-03	92	usSprintN2-1	3		26	1
568	2021-11-18 15:52:17.96278-03	91	usSprintN2-2	3		26	1
569	2021-11-18 15:52:17.97114-03	90	usSprintN2-2	3		26	1
570	2021-11-18 15:52:17.979428-03	89	usSprintN2-1	3		26	1
571	2021-11-18 15:52:17.996949-03	88	usSprintN2-2	3		26	1
572	2021-11-18 15:52:18.004399-03	87	usSprintN2-1	3		26	1
573	2021-11-18 15:52:18.012878-03	86	usSprintN2-2	3		26	1
574	2021-11-18 15:52:18.021077-03	85	us6	3		26	1
575	2021-11-18 15:52:18.029468-03	84	us6	3		26	1
576	2021-11-18 15:52:18.037673-03	83	us6	3		26	1
577	2021-11-18 15:52:18.046162-03	82	us6	3		26	1
578	2021-11-18 15:52:18.054469-03	81	us6	3		26	1
579	2021-11-18 15:52:27.307513-03	80	us5	3		26	1
580	2021-11-18 15:52:27.33978-03	79	us5	3		26	1
581	2021-11-18 15:52:27.347375-03	78	us5	3		26	1
582	2021-11-18 15:52:27.355434-03	77	us5	3		26	1
583	2021-11-18 15:52:27.364128-03	76	us4	3		26	1
584	2021-11-18 15:52:27.372203-03	75	us4	3		26	1
585	2021-11-18 15:52:27.380481-03	74	us4	3		26	1
586	2021-11-18 15:52:27.388977-03	73	us4	3		26	1
587	2021-11-18 15:52:27.397038-03	72	us4	3		26	1
588	2021-11-18 15:52:27.405466-03	71	us4	3		26	1
589	2021-11-18 15:52:27.413744-03	70	us4	3		26	1
590	2021-11-18 15:52:27.422047-03	69	us5	3		26	1
591	2021-11-18 15:52:27.43055-03	68	us5	3		26	1
592	2021-11-18 15:52:27.438745-03	67	us4	3		26	1
593	2021-11-18 15:52:27.447174-03	66	us4	3		26	1
594	2021-11-18 15:52:27.455488-03	65	us5	3		26	1
595	2021-11-18 15:52:27.46393-03	64	us4	3		26	1
596	2021-11-18 15:52:27.472281-03	63	us3	3		26	1
597	2021-11-18 15:52:27.480871-03	62	us3	3		26	1
598	2021-11-18 15:52:27.488831-03	61	us3	3		26	1
599	2021-11-18 15:52:27.497491-03	60	us3	3		26	1
600	2021-11-18 15:52:27.505492-03	59	us3	3		26	1
601	2021-11-18 15:52:27.513835-03	58	us3	3		26	1
602	2021-11-18 15:52:27.522198-03	57	us3	3		26	1
603	2021-11-18 15:52:27.530565-03	56	us3	3		26	1
604	2021-11-18 15:52:27.538982-03	55	us3	3		26	1
605	2021-11-18 15:52:27.547341-03	54	us3	3		26	1
606	2021-11-18 15:52:27.555552-03	53	us3	3		26	1
607	2021-11-18 15:52:27.564111-03	52	us3	3		26	1
608	2021-11-18 15:52:27.572176-03	51	us3	3		26	1
609	2021-11-18 15:52:27.581723-03	50	us3	3		26	1
610	2021-11-18 15:52:27.589014-03	49	us3	3		26	1
611	2021-11-18 15:52:27.597258-03	48	us3	3		26	1
612	2021-11-18 15:52:27.605637-03	47	us3	3		26	1
613	2021-11-18 15:52:27.614744-03	46	us3	3		26	1
614	2021-11-18 15:52:27.622349-03	45	us1	3		26	1
615	2021-11-18 15:52:27.630655-03	44	us3	3		26	1
616	2021-11-18 15:52:27.63891-03	43	us1	3		26	1
617	2021-11-18 15:52:27.647256-03	42	us3	3		26	1
618	2021-11-18 15:52:27.655609-03	41	us2	3		26	1
619	2021-11-18 15:52:27.664171-03	40	us3	3		26	1
620	2021-11-18 15:52:27.67265-03	39	us1	3		26	1
621	2021-11-18 15:52:27.680855-03	38	us3	3		26	1
622	2021-11-18 15:52:27.68891-03	37	us2	3		26	1
623	2021-11-18 15:52:27.697279-03	36	us1	3		26	1
624	2021-11-18 15:52:44.995646-03	56	dadad	3		25	1
625	2021-11-18 15:52:45.016575-03	55	asdad	3		25	1
626	2021-11-18 15:52:45.026248-03	54	us6	3		25	1
627	2021-11-18 15:52:45.045979-03	53	dadad	3		25	1
628	2021-11-18 15:52:45.057749-03	52	adasd	3		25	1
629	2021-11-18 15:52:45.066423-03	51	us1T	3		25	1
630	2021-11-18 15:52:45.074654-03	50	us2T	3		25	1
631	2021-11-18 15:52:45.082698-03	49	us3T	3		25	1
632	2021-11-18 15:52:45.091062-03	48	us3T	3		25	1
633	2021-11-18 15:52:45.099783-03	47	us2T	3		25	1
634	2021-11-18 15:52:45.10771-03	46	us1T	3		25	1
635	2021-11-18 15:52:45.11669-03	45	us1T	3		25	1
636	2021-11-18 15:52:45.127339-03	44	us2T	3		25	1
637	2021-11-18 15:52:45.134428-03	43	us3T	3		25	1
638	2021-11-18 15:52:45.141126-03	42	us3T	3		25	1
639	2021-11-18 15:52:45.149803-03	41	us1T	3		25	1
640	2021-11-18 15:52:45.158501-03	40	us2T	3		25	1
641	2021-11-18 15:52:45.166734-03	39	us1T	3		25	1
642	2021-11-18 15:52:45.174446-03	38	us1T	3		25	1
643	2021-11-18 15:52:45.183015-03	37	us1T	3		25	1
644	2021-11-18 15:52:45.191113-03	36	us1T	3		25	1
645	2021-11-18 15:52:45.199465-03	35	userstorie4	3		25	1
646	2021-11-18 15:52:45.208037-03	34	userstorie2	3		25	1
647	2021-11-18 15:52:45.216274-03	33	userstorie3	3		25	1
648	2021-11-18 15:52:45.224569-03	32	userstorie2	3		25	1
649	2021-11-18 15:52:45.23279-03	31	userstorie1	3		25	1
650	2021-11-18 15:52:45.241226-03	30	usSprintN2-2	3		25	1
651	2021-11-18 15:52:45.24949-03	29	usSprintN2-1	3		25	1
652	2021-11-18 15:52:45.333008-03	28	usSprintN2-2	3		25	1
653	2021-11-18 15:52:45.349824-03	27	us6	3		25	1
654	2021-11-18 15:52:45.358319-03	26	us5	3		25	1
655	2021-11-18 15:52:45.366096-03	25	us5	3		25	1
656	2021-11-18 15:52:45.374638-03	24	us4	3		25	1
657	2021-11-18 15:52:45.391385-03	23	us5	3		25	1
658	2021-11-18 15:52:45.39952-03	22	us4	3		25	1
659	2021-11-18 15:52:45.416543-03	21	us3	3		25	1
660	2021-11-18 15:52:45.424593-03	20	us3	3		25	1
661	2021-11-18 15:52:45.441337-03	19	us3	3		25	1
662	2021-11-18 15:52:45.449778-03	18	us3	3		25	1
663	2021-11-18 15:52:45.491368-03	17	us2	3		25	1
664	2021-11-18 15:52:45.508968-03	16	us1	3		25	1
665	2021-11-18 15:53:21.750727-03	159	da	3		14	1
666	2021-11-18 15:53:21.769856-03	156	asdad	3		14	1
667	2021-11-18 15:53:21.778333-03	153	dadad	3		14	1
668	2021-11-18 15:53:21.795839-03	152	dadad	3		14	1
669	2021-11-18 15:53:21.803049-03	147	adasd	3		14	1
670	2021-11-18 15:53:21.811464-03	144	us3T	3		14	1
671	2021-11-18 15:53:21.819919-03	143	us2T	3		14	1
672	2021-11-18 15:53:21.828421-03	142	us1T	3		14	1
673	2021-11-18 15:53:21.84582-03	141	userstorie4	3		14	1
674	2021-11-18 15:53:21.853173-03	140	userstorie3	3		14	1
675	2021-11-18 15:53:21.861449-03	139	userstorie2	3		14	1
676	2021-11-18 15:53:21.869755-03	138	userstorie1	3		14	1
677	2021-11-18 15:53:21.878571-03	134	usSprintN2-2	3		14	1
678	2021-11-18 15:53:21.895802-03	133	usSprintN2-2	3		14	1
679	2021-11-18 15:53:21.911624-03	132	usSprintN2-1	3		14	1
680	2021-11-18 15:53:21.920701-03	131	us6	3		14	1
681	2021-11-18 15:53:21.928284-03	130	us5	3		14	1
682	2021-11-18 15:53:21.936566-03	129	us5	3		14	1
683	2021-11-18 15:53:21.944863-03	128	us4	3		14	1
684	2021-11-18 15:53:21.953151-03	127	us3	3		14	1
685	2021-11-18 15:53:21.96158-03	126	us2	3		14	1
686	2021-11-18 15:53:21.970044-03	125	us1	3		14	1
687	2021-11-18 15:53:45.345782-03	140	gdgd	3		12	1
688	2021-11-18 15:53:45.390874-03	139	test	3		12	1
689	2021-11-18 15:53:45.399013-03	138	testfinalSprint	3		12	1
690	2021-11-18 15:53:45.407288-03	137	ultimosprint	3		12	1
691	2021-11-18 15:53:45.424724-03	135	sdadasd	3		12	1
692	2021-11-18 15:53:45.432185-03	134	adsadad	3		12	1
693	2021-11-18 15:53:45.440562-03	129	Sprint1T	3		12	1
694	2021-11-18 15:53:45.448792-03	128	Sprint 2	3		12	1
695	2021-11-18 15:53:45.46562-03	127	Sprint1	3		12	1
696	2021-11-18 15:53:45.473882-03	125	SprintN2	3		12	1
697	2021-11-18 15:53:45.482189-03	124	sprintN1	3		12	1
698	2021-11-18 15:53:45.490524-03	123	sprintn	3		12	1
699	2021-11-18 15:53:45.499049-03	122	Sprint 6	3		12	1
700	2021-11-18 15:53:45.507156-03	121	SPrint5	3		12	1
701	2021-11-18 15:53:45.515579-03	120	Sprint3	3		12	1
702	2021-11-18 15:53:45.523883-03	119	Sprint4	3		12	1
703	2021-11-18 15:53:45.532251-03	118	Sprint2	3		12	1
704	2021-11-18 15:53:45.540614-03	117	Sprint1	3		12	1
705	2021-11-18 15:54:06.894142-03	163	Proyecto-SprintTest	3		10	1
706	2021-11-18 15:54:06.934255-03	162	Proyecto1-Iteracion5	3		10	1
707	2021-11-18 15:54:06.94258-03	158	ProyectoIteracion5	3		10	1
708	2021-11-19 04:22:01.259232-03	168	UserStory1	3		14	1
709	2021-11-19 04:22:05.468575-03	169	UserStory2	3		14	1
710	2021-11-19 04:22:19.374932-03	144	Sprint1	3		12	1
711	2021-11-19 04:22:42.096455-03	177	ProyectoIteracionActual	3		10	1
\.


--
-- TOC entry 3514 (class 0 OID 99523)
-- Dependencies: 251
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	sites	site
2	admin	logentry
3	auth	permission
4	auth	group
5	auth	user
6	contenttypes	contenttype
7	sessions	session
8	mysite	miembrossprint
9	mysite	permiso
10	mysite	proyecto
11	mysite	rolproyecto
12	mysite	sprint
13	mysite	teammember
14	mysite	userstory
15	mysite	tablero
16	mysite	rolsistema
17	mysite	profile
18	mysite	nota
19	mysite	actividad
20	account	emailaddress
21	account	emailconfirmation
22	socialaccount	socialaccount
23	socialaccount	socialapp
24	socialaccount	socialtoken
25	mysite	historialuserstory
26	mysite	historialestadosuserstory
\.


--
-- TOC entry 3516 (class 0 OID 99528)
-- Dependencies: 253
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-10-01 14:50:17.889569-04
2	auth	0001_initial	2021-10-01 14:50:18.590232-04
3	account	0001_initial	2021-10-01 14:50:18.90646-04
4	account	0002_email_max_length	2021-10-01 14:50:18.941889-04
5	account	0003_auto_20210813_0102	2021-10-01 14:50:19.38296-04
6	admin	0001_initial	2021-10-01 14:50:19.573564-04
7	admin	0002_logentry_remove_auto_add	2021-10-01 14:50:19.603864-04
8	admin	0003_logentry_add_action_flag_choices	2021-10-01 14:50:19.641904-04
9	contenttypes	0002_remove_content_type_name	2021-10-01 14:50:19.706848-04
10	auth	0002_alter_permission_name_max_length	2021-10-01 14:50:19.739897-04
11	auth	0003_alter_user_email_max_length	2021-10-01 14:50:19.769221-04
12	auth	0004_alter_user_username_opts	2021-10-01 14:50:19.803765-04
13	auth	0005_alter_user_last_login_null	2021-10-01 14:50:19.838294-04
14	auth	0006_require_contenttypes_0002	2021-10-01 14:50:19.848261-04
15	auth	0007_alter_validators_add_error_messages	2021-10-01 14:50:19.883316-04
16	auth	0008_alter_user_username_max_length	2021-10-01 14:50:19.94594-04
17	auth	0009_alter_user_last_name_max_length	2021-10-01 14:50:19.979665-04
18	auth	0010_alter_group_name_max_length	2021-10-01 14:50:20.01813-04
19	auth	0011_update_proxy_permissions	2021-10-01 14:50:20.055056-04
20	auth	0012_alter_user_first_name_max_length	2021-10-01 14:50:20.088544-04
21	mysite	0001_initial	2021-10-01 14:50:22.743832-04
22	sessions	0001_initial	2021-10-01 14:50:22.891817-04
23	sites	0001_initial	2021-10-01 14:50:22.931418-04
24	sites	0002_alter_domain_unique	2021-10-01 14:50:23.006912-04
25	socialaccount	0001_initial	2021-10-01 14:50:23.659754-04
26	socialaccount	0002_token_max_lengths	2021-10-01 14:50:23.773447-04
27	socialaccount	0003_extra_data_default_dict	2021-10-01 14:50:23.814619-04
28	socialaccount	0004_auto_20210813_0102	2021-10-01 14:50:24.78342-04
29	mysite	0002_alter_teammember_rol_proyecto	2021-10-01 16:32:59.545787-04
30	mysite	0003_alter_sprint_dias_habiles	2021-10-02 03:21:09.495051-04
31	mysite	0004_miembrossprint_miembro del sprint debe ser unico	2021-10-02 20:57:55.444478-04
32	mysite	0005_auto_20211003_2253	2021-10-03 22:53:39.204162-03
33	mysite	0006_alter_userstory_priorizacion	2021-10-04 00:46:36.208746-03
34	mysite	0007_alter_userstory_priorizacion	2021-10-04 00:54:23.032914-03
35	mysite	0008_alter_userstory_priorizacion	2021-10-04 01:38:48.672225-03
36	mysite	0009_alter_userstory_priorizacion	2021-10-04 01:47:34.853105-03
37	mysite	0010_teammember_disponible	2021-10-04 16:39:40.995667-03
38	mysite	0011_permiso_desactivado	2021-10-05 01:34:44.362002-03
39	mysite	0012_alter_tablero_sprint	2021-10-06 04:37:49.51865-03
40	mysite	0013_miembrossprint_usuario	2021-10-06 22:52:56.019627-03
41	mysite	0014_userstory_usuario	2021-10-06 23:46:30.345751-03
42	mysite	0015_alter_sprint_table	2021-10-10 00:14:06.838381-03
43	mysite	0016_alter_proyecto_nombre	2021-10-11 13:15:47.115188-03
44	mysite	0017_alter_sprint_dias_habiles	2021-10-15 12:11:37.205736-03
45	mysite	0018_alter_actividad_fecha	2021-10-16 02:19:56.336784-03
46	mysite	0019_alter_actividad_fecha	2021-10-16 02:25:02.268709-03
47	mysite	0020_historialuserstory	2021-10-21 23:29:36.127337-03
48	mysite	0021_alter_actividad_nombre	2021-10-22 02:16:10.648523-03
49	mysite	0022_userstory_horas_trabajadas_us	2021-10-22 02:23:39.984002-03
50	mysite	0023_userstory_porcentaje_realizado	2021-10-22 02:32:19.799907-03
51	mysite	0024_userstory_horas_extras_trabajadas_us	2021-10-22 03:41:46.930454-03
52	mysite	0025_historialestadosuserstory	2021-10-23 21:28:25.318193-03
53	mysite	0026_historialestadosuserstory_hora_cambio_estado	2021-10-23 23:17:09.955712-03
54	mysite	0027_historialuserstory_priorizacion	2021-10-24 03:58:31.451347-03
55	mysite	0028_historialuserstory_motivo_comentario	2021-10-24 20:44:55.47134-03
56	mysite	0029_auto_20211024_2056	2021-10-24 20:56:10.15914-03
57	mysite	0030_auto_20211024_2127	2021-10-24 21:27:10.836299-03
58	mysite	0031_alter_userstory_duracion_estimada	2021-10-24 21:46:11.053204-03
59	mysite	0032_historialuserstory_estado	2021-10-25 01:58:25.313804-03
60	mysite	0033_auto_20211027_2134	2021-10-27 21:34:58.325157-03
61	mysite	0034_userstory_estado_qa	2021-10-27 22:31:30.122901-03
62	mysite	0035_alter_userstory_estado_qa	2021-10-27 23:57:58.86796-03
63	mysite	0036_actividad_estado_tablero	2021-11-15 16:36:06.039721-03
\.


--
-- TOC entry 3518 (class 0 OID 99536)
-- Dependencies: 255
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
t2oembcbo5xaa16haz4p6ydigl2cvohc	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mWWQd:mOI5kqT9ao-wCk2ej118mnnCJ0gLyDX40tqKOa_qr_Q	2021-10-16 01:15:15.277808-03
gzzl98xhf1qtwmua37kde1i6x9kb4rtf	.eJxVjMEOwiAQRP-Fs2lYKLL1Zn-ELAsEYkMTS0_Gf5eaHvQ0mXkz8xKO9pbdvsWnK0HcxCguv5knfsR6AFqWIx6Ied1rG76dE2_DvbtYW2FqZa3zufq7yrTl_oMcCAGAk9TaMhGYRNgV1QRaKiW9sWn0lserDWwxRdAelQGU0k8o3h_ByzwG:1mZgOq:xJQC4NKVXA_fFkHIIC_Oy0SYpShJABRA8fDeH85xIjc	2021-10-24 18:30:28.490338-03
l5v359r70atlg0k5l6hemx4kfutcawyz	.eJxVjMEOwiAQRP-Fs2lYKLL1Zn-ELAsEYkMTS0_Gf5eaHvQ0mXkz8xKO9pbdvsWnK0HcxCguv5knfsR6AFqWIx6Ied1rG76dE2_DvbtYW2FqZa3zufq7yrTl_oMcCAGAk9TaMhGYRNgV1QRaKiW9sWn0lserDWwxRdAelQGU0k8o3h_ByzwG:1mZhyr:Yfr4QxsWxU5GnMvd780mBiuZBcTpQD9_s7cimuuXy44	2021-10-24 20:11:45.207344-03
5r394b4t5yhs0s8xasd5dc0c5btcoqzg	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mg8Ou:54xAHDfuF9tBeWbZgy5uAtKxdSjJpqgU5b0r4vmSiFE	2021-11-11 13:37:12.166592-03
qwbvrvzdl59944v960lc1q1dnhoiuv5e	.eJxVjMEOwiAQRP-Fs2lYKLL1Zn-ELAsEYkMTS0_Gf5eaHvQ0mXkz8xKO9pbdvsWnK0HcxCguv5knfsR6AFqWIx6Ied1rG76dE2_DvbtYW2FqZa3zufq7yrTl_oMcCAGAk9TaMhGYRNgV1QRaKiW9sWn0lserDWwxRdAelQGU0k8o3h_ByzwG:1mdf4R:C5Mi_U5Ff3lV0ISOCs5DDXALHK2_sYlciMys8L6i33U	2021-11-04 17:53:51.328203-03
0s1cpi6dlg2byfhhf6mvzipc72bb4x51	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mgDj8:mEjsPXcklwkSLUvKT439i8Mc1YqywO7wz-RWwYIOxPY	2021-11-11 19:18:26.634866-03
v206ro7ofazk3svgn8u5pecwrfh4zk1j	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mgF7J:A2euWh2TXIYmOl6gdVIpjZ5vdcOGyiRcqZIA0sBODvY	2021-11-11 20:47:29.569175-03
1w8ft5nlvx87zgadr3kar8mvupxqjjqr	.eJxVjMEOwiAQRP-Fs2lYKLL1Zn-ELAsEYkMTS0_Gf5eaHvQ0mXkz8xKO9pbdvsWnK0HcxCguv5knfsR6AFqWIx6Ied1rG76dE2_DvbtYW2FqZa3zufq7yrTl_oMcCAGAk9TaMhGYRNgV1QRaKiW9sWn0lserDWwxRdAelQGU0k8o3h_ByzwG:1meoee:CforXa4HBUsy2G1NvUKvP7T1BKIfy79ymd-AiK7WdvU	2021-11-07 22:20:00.404328-03
hk01nwg4mqileywuqn1yfv0md9ntqbjh	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mYdMp:iV-RT8wDpEv5Ei3l3F-DU8Pj4KBDa8OUrIWjNcijCWw	2021-10-21 21:04:03.582614-03
tzezhp2k3ffh0y69pqiaxbw09mjmi4w1	.eJwVirEKgDAMBf_lzYIguLg6C24OIhJCUaFtiklFEP_dut0d90CFD_LELDnaqkbm0M0PorsNHWpUSKewUy3mZTtiKcqSyoaClG1fE50U_gFvhTDp2OQ0tFc_YHk_p3Ug5g:1me6G0:DxtsPxUxGKjdsy9TFvu8wWKtmhCEDg_Y1iIBryIiUkk	2021-11-05 22:55:36.607248-03
h52sb83rghuzhkwgvsrhf152tqfw4qsd	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mYfkr:0A-go2LoeBd8BGMuQV6sTpRo9fAJykbmWdBYdWDdmsE	2021-10-21 23:37:01.185742-03
nfesg1p82jgte92yzy64b9cxhsthz1rm	.eJxVjssOgjAQRf9l1oQwlPJwpx-hC2PI0CnSiC2hxRiN_y4QNuxm7jnz-IJ3ylBPSrnJhtoHChoO1y8Mo1PaezhA7-7GQgReuWFmMJc0ha4eaKTnIsAvgrdMxim_fM70MnCLoF6VyeuxNjw7CLusIfXQdgHU90scbx_Eq7NhHx_nTttgFAXj7Gmb2q3qyHfLAZbILLKikKIRKXKCkrjMZdJWXFR5VqLUCVYtIxFiyiSqNJO6EILzpkH4_QHC11jr:1mWPDl:BNch5rMBW3XpuuvjglPqd5tdo6RV52P2AUmEXxMOY7E	2021-10-15 17:33:29.883554-03
fvrgxxklcdfzwedpe36jvgsw8nndc5wp	.eJxVjMEOwiAQRP-Fs2lYKLL1Zn-ELAsEYkMTS0_Gf5eaHvQ0mXkz8xKO9pbdvsWnK0HcxCguv5knfsR6AFqWIx6Ied1rG76dE2_DvbtYW2FqZa3zufq7yrTl_oMcCAGAk9TaMhGYRNgV1QRaKiW9sWn0lserDWwxRdAelQGU0k8o3h_ByzwG:1meVSQ:-uXOIUx3YLPJMPePyG4-UVDm3Dwa3K6tmGQMZoGGWRM	2021-11-07 01:50:06.832046-03
4w9hq9adwjxt9tya41u1hdv1mj2420b5	.eJwVirEKgDAMBf_lzYKj4uziB4iDiIRQtVCbYiII0n-3bnfHvVBhT4GY5Y62qpE5dPOL6B5DhxoV0iXsVIsF2X0sRVlS2VCQbjvWRBed_4BcYZiabfRtsL4XLPkDpHggyg:1mekcb:4vi4w_NlRPHSDgsm_CudaLxugeuSIo1nhqgJJcSYWo8	2021-11-07 18:01:37.567689-03
zwme3qb2oiowjdrbgzwhnp6zcn6tihav	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mfFCy:5n7JI9FxWSn5b8b1Vui16IvV7l6IgqGSp9Ku0lgt3GI	2021-11-09 02:41:12.117887-03
wzvmk1fax8gpbjxm0xusttd4ilq3tsiy	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mfuGe:PZL9wQw0r7HfFc-65PfdytIArDLk1BJgtg-i69T0Aso	2021-11-10 22:31:44.297464-03
66xwggxjxy0i4i52mf3qa7frk9zuzgeg	.eJxVjEEOwiAURO_C2pBC4VPc6UXIBz6B2NDEwsp4d6npQpfzZua9mMPesus7PV2J7Moku_wyj-FB9ShwXQ_MMYSt18a_m7Pe-W0kqq0EbGWr9_P1p8q45-GJgkSUpHTyszSQphBQLbRoiB4AbfIWhdDRSq9UoimRn6MxZjBIAMTeH9b8POI:1mgZQA:5WaZhL8UNJlnBu-1tUSm2rNBodGIJTOwXs7xyzhGWkk	2021-11-12 18:28:18.179724-03
gmg1qk7yii9pgkceft8cdpkbsrymhxzb	.eJxVjEEOwiAURO_C2jRAST91pxdphv9pIDY0sbAy3t3WdKHLeTPzXmpCq2lqW3xOWdRVDeryywL4EctRYFkO3IF5baV2381Zb91tT7HUzKh5Lffz9adK2NLuYTNaH4DBIsrgHY2ixVsKxE56MVGLzM5HRk-YrdEkhgjGBQ3RulfvD9Q_PMA:1mnmuq:mIknx_m8KTRy0hX2cFR0Y-VKiWsJM4UDGZUysq-sSEs	2021-12-02 16:17:48.204421-03
r4hkacu859moyi67wqxhchjljpwfhnr8	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mm4uJ:mio1UnQ2by6CeVgMpd-5MOT8yCFgqDKJb0GL2bQsgZ4	2021-11-27 23:06:11.862858-03
5dtef17qphmtgyrk1wihog6535amtcy7	.eJxVjk0OgjAQRu8ya0IYSsG604VH0IUxZNqp0ogt0rIwxrsLho27me-9-XlDDMZRT8aEyac2JkoWtuc3DGMwNkbYQh9uzkMG0YRhZjCXNKWuHWikxyLAJ4OjSk9diNdLHk5wyaD9KVO0Y-t4dhD-Mk3mbv0CqO-XOF8_yH_OimO-mzvrkzOUXPD7depvVUexWw6wRGZRNY0UWpTIBUriTS2Lq-JG1dUGpS1QXRmJEEsmocpK2kYIrrVG-HwBeoVYhw:1mmGRA:hDqFI-TTR8vCaWnq8RqmbFnXe68Eyq4HqpCrYEVXFnI	2021-11-28 11:24:52.894389-03
xhg2ty2z47t5jvjen6n2ydqa9kh30fw3	.eJxVjkFvgzAMhf-Lz4hhQqD0tt5a7bxDqwmZOB1RaYJwkCZV_e8LFZfe_N777OcHSDCORjImLD52Eila2F8e4O1fhD18QAbTHIwVSWoMv84nR0yYEgZppCUO3UQz3VcAnhkctVTnE30f5cvDTwbdC1nEzp3jxCC8eT2Zm_VrQOO42vn2TP5itljyz6Ssj85QdMEftq23UwPJsBawRmZVNY1WvSqRC9TEu1oX15abtq52qG2B7ZWRCLFkUm1ZadsoxXXfIzz_AakcW1w:1mmhil:A1h2ZwCyOqh8joWeHNtf2cNNgA6pHECILOyi2-0XL-Q	2021-11-29 16:32:51.622912-03
gm0euwjxbsb5pewcsxug4i9othw61az9	.eJxVjU0OwiAQhe_C2hCmdEpxZy_SDAwNxIYmAivj3RXThS7f9_6eYqVW49pKeKyJxVWAuPwyR_4ecjdo3zuW5P3RcpXfzGkXefuokGvyVNORl7P1NxWpxH7ACMx6NAa10wOwAiSeJ1SbZWOncQYMCuzGQAQwMGk7jBiM1jw5B-L1BpWZO3M:1mnyD4:D1ELszerELmhA-SdKSDSPzmhu36E6Dyh4rVW1l-_kJY	2021-12-03 04:21:22.718538-03
\.


--
-- TOC entry 3519 (class 0 OID 99542)
-- Dependencies: 256
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_site (id, domain, name) FROM stdin;
1	example.com	example.com
2	127.0.0.1:8000	127.0.0.1:8000
3	gestorproyectosis2.com	gestorproyectosis2.com
\.


--
-- TOC entry 3522 (class 0 OID 99549)
-- Dependencies: 259
-- Data for Name: socialaccount_socialaccount; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialaccount (id, provider, uid, last_login, date_joined, extra_data, user_id) FROM stdin;
2	google	116211657448384212679	2021-11-18 01:29:43.149921-03	2021-10-01 14:59:59.405104-04	{"id": "116211657448384212679", "email": "mateoggv94@gmail.com", "verified_email": true, "name": "Mateo Guillen", "given_name": "Mateo", "family_name": "Guillen", "picture": "https://lh3.googleusercontent.com/a/AATXAJyRIvrMKGM4xwfz2IyS-ehQ9Ho1nOltYbYoqV_s=s96-c", "locale": "es"}	3
5	google	109754220952392839343	2021-11-19 03:56:33.4746-03	2021-11-18 15:39:54.730248-03	{"id": "109754220952392839343", "email": "equipo10superuser@gmail.com", "verified_email": true, "name": "admin equipo10 is2", "given_name": "admin equipo10", "family_name": "is2", "picture": "https://lh3.googleusercontent.com/a/AATXAJwHnP9W54Mg2Paaboq9Pm3afK_gSsxAg-awsnYT=s96-c", "locale": "es"}	6
3	google	110890719950138544676	2021-11-19 04:11:01.022628-03	2021-10-01 15:13:52.856162-04	{"id": "110890719950138544676", "email": "is2equipo10@gmail.com", "verified_email": true, "name": "equipo10 is2", "given_name": "equipo10", "family_name": "is2", "picture": "https://lh3.googleusercontent.com/a/AATXAJxAlDyQ6qtC7fdwoCNz6P-JWIM-CdMMH1lBZeqF=s96-c", "locale": "es"}	4
1	google	102431728714348567625	2021-11-19 03:37:22.820762-03	2021-10-01 14:59:38.082182-04	{"id": "102431728714348567625", "email": "mateoggv94@fpuna.edu.py", "verified_email": true, "name": "Mateo Giovanni Guillen Vera", "given_name": "Mateo Giovanni", "family_name": "Guillen Vera", "picture": "https://lh3.googleusercontent.com/a/AATXAJxnF_PUkEqGHUPCy7CsFzA1aCZjS6Puv0dPofuk=s96-c", "locale": "es", "hd": "fpuna.edu.py"}	2
4	google	115590532310910401076	2021-11-19 03:41:23.151896-03	2021-10-01 15:17:10.510081-04	{"id": "115590532310910401076", "email": "equipo10is22021@gmail.com", "verified_email": true, "name": "maria is2", "given_name": "maria", "family_name": "is2", "picture": "https://lh3.googleusercontent.com/a/AATXAJxlpUy82Cu5QtH4_kz9OSTGyWrlJLN47vGZ1Vck=s96-c", "locale": "es"}	5
\.


--
-- TOC entry 3524 (class 0 OID 99557)
-- Dependencies: 261
-- Data for Name: socialaccount_socialapp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialapp (id, provider, name, client_id, secret, key) FROM stdin;
1	google	Django Oauth	86622636108-vmagdge7o474dk5ppphjef8637hbd43k.apps.googleusercontent.com	pcv89XUheGcaTPnAlke5kV7u	
\.


--
-- TOC entry 3526 (class 0 OID 99565)
-- Dependencies: 263
-- Data for Name: socialaccount_socialapp_sites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialapp_sites (id, socialapp_id, site_id) FROM stdin;
1	1	2
2	1	3
\.


--
-- TOC entry 3528 (class 0 OID 99570)
-- Dependencies: 265
-- Data for Name: socialaccount_socialtoken; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialtoken (id, token, token_secret, expires_at, account_id, app_id) FROM stdin;
2	ya29.a0ARrdaM-dQPf-CmCJeWuYixahji2Alm7r-yg7cFHtkDojTYoRS1b-QAuL9Gm5k_pZ3sKrGixx-hBeS_GFowBe2WL4TAIRAB-YYzdu6B6cgUAFcxersAfzBS14XyGNATAa3QyHwWrePBWoK79Y43DzItJRrvVF		2021-11-18 02:29:41.963668-03	2	1
1	ya29.a0ARrdaM-Tw-zI5ZZSNpY0v17W0urdfDtFdJdFS1xMly7bI3QZoo0BmQKy4r4-a79D3GWX2j-fPOx0i0ldsBSTGBfdn3T7UwdFsF6KXwzl45SedSZKpIlkiTOjGC60WnltMXCbK50Bk7MNuQxmCMfyKpLkfVG3FA		2021-11-19 04:37:21.618991-03	1	1
4	ya29.a0ARrdaM_2p7YbDj7JDpiGIiPydPbPueXfg-k93I0-uH69EeQ6w7eWNLf9Q6aL1mXP8escPOKprXs5SAZNWZk1ZCB-nZWVz4Oa3GEXDF3TyQJ7hyT9KegCwqV2HtI_W6YjDCMNjPjQeQeilBeihAj3yyWIMhin		2021-11-19 04:41:21.944481-03	4	1
5	ya29.a0ARrdaM8rIHi02QKp31Kph_cwz0qsBJb7v0OL6c2vIIWgx2cFHn-pTZLQt0RRQFoqxiXVJtTMlXz-PT_NULBmD9ehUrcnYSBWfO_4NWWygaVXQgEXCPAEEaElkpEaqpddHwvq0ixU7006sjTlYPlRvWobBlam		2021-11-19 04:56:32.274496-03	5	1
3	ya29.a0ARrdaM_s8G_QRjiVUB8p9jYtwHhzxr4x-Y8Ht4qhMxS5-2hJ4dBbu6H3WrZerecuvGRltwNifebsKp_sGmYQZvKVOtLg3yuwtFfMmxw1Gk6JjhWJHqAFzaYnW4Snzn6UmuVR_Nr7p1vPbyJl7hYbgoNNQhP0		2021-11-19 05:10:59.816785-03	3	1
\.


--
-- TOC entry 3568 (class 0 OID 0)
-- Dependencies: 203
-- Name: Actividad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Actividad_id_seq"', 167, true);


--
-- TOC entry 3569 (class 0 OID 0)
-- Dependencies: 205
-- Name: HistorialEstadosUserStory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."HistorialEstadosUserStory_id_seq"', 304, true);


--
-- TOC entry 3570 (class 0 OID 0)
-- Dependencies: 207
-- Name: HistorialUserStory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."HistorialUserStory_id_seq"', 65, true);


--
-- TOC entry 3571 (class 0 OID 0)
-- Dependencies: 209
-- Name: MiembrosSprint_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."MiembrosSprint_id_seq"', 162, true);


--
-- TOC entry 3572 (class 0 OID 0)
-- Dependencies: 211
-- Name: Nota_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Nota_id_seq"', 44, true);


--
-- TOC entry 3573 (class 0 OID 0)
-- Dependencies: 213
-- Name: Permiso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Permiso_id_seq"', 43, true);


--
-- TOC entry 3574 (class 0 OID 0)
-- Dependencies: 215
-- Name: Profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Profile_id_seq"', 6, true);


--
-- TOC entry 3575 (class 0 OID 0)
-- Dependencies: 217
-- Name: Proyecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_id_seq"', 177, true);


--
-- TOC entry 3576 (class 0 OID 0)
-- Dependencies: 219
-- Name: RolProyecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."RolProyecto_id_seq"', 119, true);


--
-- TOC entry 3577 (class 0 OID 0)
-- Dependencies: 221
-- Name: RolProyecto_permisos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."RolProyecto_permisos_id_seq"', 892, true);


--
-- TOC entry 3578 (class 0 OID 0)
-- Dependencies: 223
-- Name: RolSistema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."RolSistema_id_seq"', 3, true);


--
-- TOC entry 3579 (class 0 OID 0)
-- Dependencies: 225
-- Name: RolSistema_permisos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."RolSistema_permisos_id_seq"', 17, true);


--
-- TOC entry 3580 (class 0 OID 0)
-- Dependencies: 228
-- Name: Tablero_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Tablero_id_seq"', 34, true);


--
-- TOC entry 3581 (class 0 OID 0)
-- Dependencies: 230
-- Name: TeamMember_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."TeamMember_id_seq"', 105, true);


--
-- TOC entry 3582 (class 0 OID 0)
-- Dependencies: 232
-- Name: UserStory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."UserStory_id_seq"', 169, true);


--
-- TOC entry 3583 (class 0 OID 0)
-- Dependencies: 234
-- Name: account_emailaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_emailaddress_id_seq', 5, true);


--
-- TOC entry 3584 (class 0 OID 0)
-- Dependencies: 236
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_emailconfirmation_id_seq', 1, false);


--
-- TOC entry 3585 (class 0 OID 0)
-- Dependencies: 238
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- TOC entry 3586 (class 0 OID 0)
-- Dependencies: 240
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- TOC entry 3587 (class 0 OID 0)
-- Dependencies: 242
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 104, true);


--
-- TOC entry 3588 (class 0 OID 0)
-- Dependencies: 245
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- TOC entry 3589 (class 0 OID 0)
-- Dependencies: 246
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 6, true);


--
-- TOC entry 3590 (class 0 OID 0)
-- Dependencies: 248
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 3591 (class 0 OID 0)
-- Dependencies: 250
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 711, true);


--
-- TOC entry 3592 (class 0 OID 0)
-- Dependencies: 252
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 26, true);


--
-- TOC entry 3593 (class 0 OID 0)
-- Dependencies: 254
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 63, true);


--
-- TOC entry 3594 (class 0 OID 0)
-- Dependencies: 257
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_site_id_seq', 3, true);


--
-- TOC entry 3595 (class 0 OID 0)
-- Dependencies: 258
-- Name: mysite_sprint_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.mysite_sprint_id_seq', 144, true);


--
-- TOC entry 3596 (class 0 OID 0)
-- Dependencies: 260
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialaccount_id_seq', 5, true);


--
-- TOC entry 3597 (class 0 OID 0)
-- Dependencies: 262
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_id_seq', 1, true);


--
-- TOC entry 3598 (class 0 OID 0)
-- Dependencies: 264
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_sites_id_seq', 2, true);


--
-- TOC entry 3599 (class 0 OID 0)
-- Dependencies: 266
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialtoken_id_seq', 5, true);


--
-- TOC entry 3127 (class 2606 OID 99611)
-- Name: Actividad Actividad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Actividad"
    ADD CONSTRAINT "Actividad_pkey" PRIMARY KEY (id);


--
-- TOC entry 3132 (class 2606 OID 99613)
-- Name: HistorialEstadosUserStory HistorialEstadosUserStory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialEstadosUserStory"
    ADD CONSTRAINT "HistorialEstadosUserStory_pkey" PRIMARY KEY (id);


--
-- TOC entry 3137 (class 2606 OID 99615)
-- Name: HistorialUserStory HistorialUserStory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialUserStory"
    ADD CONSTRAINT "HistorialUserStory_pkey" PRIMARY KEY (id);


--
-- TOC entry 3197 (class 2606 OID 99617)
-- Name: TeamMember Miembro debe ser Unico; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TeamMember"
    ADD CONSTRAINT "Miembro debe ser Unico" UNIQUE (usuario_id, proyecto_id);


--
-- TOC entry 3141 (class 2606 OID 99619)
-- Name: MiembrosSprint Miembro del Sprint debe ser Unico; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."MiembrosSprint"
    ADD CONSTRAINT "Miembro del Sprint debe ser Unico" UNIQUE (team_member_id, sprint_id);


--
-- TOC entry 3143 (class 2606 OID 99621)
-- Name: MiembrosSprint MiembrosSprint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."MiembrosSprint"
    ADD CONSTRAINT "MiembrosSprint_pkey" PRIMARY KEY (id);


--
-- TOC entry 3166 (class 2606 OID 99623)
-- Name: RolProyecto Nombre de Roles Unicos Por Proyecto; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto"
    ADD CONSTRAINT "Nombre de Roles Unicos Por Proyecto" UNIQUE (nombre, proyecto_id);


--
-- TOC entry 3188 (class 2606 OID 99625)
-- Name: Sprint Nombre del Sprint por Proyecto debe ser Unico; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT "Nombre del Sprint por Proyecto debe ser Unico" UNIQUE (nombre, proyecto_id);


--
-- TOC entry 3148 (class 2606 OID 99627)
-- Name: Nota Nota_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Nota"
    ADD CONSTRAINT "Nota_pkey" PRIMARY KEY (id);


--
-- TOC entry 3152 (class 2606 OID 99629)
-- Name: Permiso Permiso_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Permiso"
    ADD CONSTRAINT "Permiso_nombre_key" UNIQUE (nombre);


--
-- TOC entry 3154 (class 2606 OID 99631)
-- Name: Permiso Permiso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Permiso"
    ADD CONSTRAINT "Permiso_pkey" PRIMARY KEY (id);


--
-- TOC entry 3156 (class 2606 OID 99633)
-- Name: Profile Profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Profile"
    ADD CONSTRAINT "Profile_pkey" PRIMARY KEY (id);


--
-- TOC entry 3159 (class 2606 OID 99635)
-- Name: Profile Profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Profile"
    ADD CONSTRAINT "Profile_user_id_key" UNIQUE (user_id);


--
-- TOC entry 3162 (class 2606 OID 99637)
-- Name: Proyecto Proyecto_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto"
    ADD CONSTRAINT "Proyecto_nombre_key" UNIQUE (nombre);


--
-- TOC entry 3164 (class 2606 OID 99639)
-- Name: Proyecto Proyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto"
    ADD CONSTRAINT "Proyecto_pkey" PRIMARY KEY (id);


--
-- TOC entry 3172 (class 2606 OID 99641)
-- Name: RolProyecto_permisos RolProyecto_permisos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto_permisos"
    ADD CONSTRAINT "RolProyecto_permisos_pkey" PRIMARY KEY (id);


--
-- TOC entry 3175 (class 2606 OID 99643)
-- Name: RolProyecto_permisos RolProyecto_permisos_rolproyecto_id_permiso_id_7f320b4b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto_permisos"
    ADD CONSTRAINT "RolProyecto_permisos_rolproyecto_id_permiso_id_7f320b4b_uniq" UNIQUE (rolproyecto_id, permiso_id);


--
-- TOC entry 3168 (class 2606 OID 99645)
-- Name: RolProyecto RolProyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto"
    ADD CONSTRAINT "RolProyecto_pkey" PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 99647)
-- Name: RolSistema RolSistema_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolSistema"
    ADD CONSTRAINT "RolSistema_nombre_key" UNIQUE (nombre);


--
-- TOC entry 3183 (class 2606 OID 99649)
-- Name: RolSistema_permisos RolSistema_permisos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolSistema_permisos"
    ADD CONSTRAINT "RolSistema_permisos_pkey" PRIMARY KEY (id);


--
-- TOC entry 3186 (class 2606 OID 99651)
-- Name: RolSistema_permisos RolSistema_permisos_rolsistema_id_permiso_id_facbb331_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolSistema_permisos"
    ADD CONSTRAINT "RolSistema_permisos_rolsistema_id_permiso_id_facbb331_uniq" UNIQUE (rolsistema_id, permiso_id);


--
-- TOC entry 3180 (class 2606 OID 99653)
-- Name: RolSistema RolSistema_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolSistema"
    ADD CONSTRAINT "RolSistema_pkey" PRIMARY KEY (id);


--
-- TOC entry 3193 (class 2606 OID 99655)
-- Name: Tablero Tablero_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tablero"
    ADD CONSTRAINT "Tablero_pkey" PRIMARY KEY (id);


--
-- TOC entry 3199 (class 2606 OID 99657)
-- Name: TeamMember TeamMember_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TeamMember"
    ADD CONSTRAINT "TeamMember_pkey" PRIMARY KEY (id);


--
-- TOC entry 3206 (class 2606 OID 99659)
-- Name: UserStory UserStory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserStory"
    ADD CONSTRAINT "UserStory_pkey" PRIMARY KEY (id);


--
-- TOC entry 3212 (class 2606 OID 99661)
-- Name: account_emailaddress account_emailaddress_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_email_key UNIQUE (email);


--
-- TOC entry 3214 (class 2606 OID 99663)
-- Name: account_emailaddress account_emailaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_pkey PRIMARY KEY (id);


--
-- TOC entry 3219 (class 2606 OID 99665)
-- Name: account_emailconfirmation account_emailconfirmation_key_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_key_key UNIQUE (key);


--
-- TOC entry 3221 (class 2606 OID 99667)
-- Name: account_emailconfirmation account_emailconfirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_pkey PRIMARY KEY (id);


--
-- TOC entry 3224 (class 2606 OID 99669)
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 3229 (class 2606 OID 99671)
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 3232 (class 2606 OID 99673)
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3226 (class 2606 OID 99675)
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3235 (class 2606 OID 99677)
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 3237 (class 2606 OID 99679)
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3245 (class 2606 OID 99681)
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 3248 (class 2606 OID 99683)
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- TOC entry 3239 (class 2606 OID 99685)
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3251 (class 2606 OID 99687)
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3254 (class 2606 OID 99689)
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- TOC entry 3242 (class 2606 OID 99691)
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 3257 (class 2606 OID 99693)
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3260 (class 2606 OID 99695)
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 3262 (class 2606 OID 99697)
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3264 (class 2606 OID 99699)
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 3267 (class 2606 OID 99701)
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 3271 (class 2606 OID 99703)
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- TOC entry 3273 (class 2606 OID 99705)
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- TOC entry 3190 (class 2606 OID 99707)
-- Name: Sprint mysite_sprint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT mysite_sprint_pkey PRIMARY KEY (id);


--
-- TOC entry 3275 (class 2606 OID 99709)
-- Name: socialaccount_socialaccount socialaccount_socialaccount_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_pkey PRIMARY KEY (id);


--
-- TOC entry 3277 (class 2606 OID 99711)
-- Name: socialaccount_socialaccount socialaccount_socialaccount_provider_uid_fc810c6e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_provider_uid_fc810c6e_uniq UNIQUE (provider, uid);


--
-- TOC entry 3282 (class 2606 OID 99713)
-- Name: socialaccount_socialapp_sites socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq UNIQUE (socialapp_id, site_id);


--
-- TOC entry 3280 (class 2606 OID 99715)
-- Name: socialaccount_socialapp socialaccount_socialapp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp
    ADD CONSTRAINT socialaccount_socialapp_pkey PRIMARY KEY (id);


--
-- TOC entry 3284 (class 2606 OID 99717)
-- Name: socialaccount_socialapp_sites socialaccount_socialapp_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp_sites_pkey PRIMARY KEY (id);


--
-- TOC entry 3290 (class 2606 OID 99719)
-- Name: socialaccount_socialtoken socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq UNIQUE (app_id, account_id);


--
-- TOC entry 3292 (class 2606 OID 99721)
-- Name: socialaccount_socialtoken socialaccount_socialtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_pkey PRIMARY KEY (id);


--
-- TOC entry 3125 (class 1259 OID 99722)
-- Name: Actividad_miembro_id_1d4e65be; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Actividad_miembro_id_1d4e65be" ON public."Actividad" USING btree (miembro_id);


--
-- TOC entry 3128 (class 1259 OID 99723)
-- Name: Actividad_sprint_id_45c4b485; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Actividad_sprint_id_45c4b485" ON public."Actividad" USING btree (sprint_id);


--
-- TOC entry 3129 (class 1259 OID 99724)
-- Name: Actividad_us_id_253afcd9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Actividad_us_id_253afcd9" ON public."Actividad" USING btree (us_id);


--
-- TOC entry 3130 (class 1259 OID 99725)
-- Name: HistorialEstadosUserStory_miembro_sprint_id_97d924e6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "HistorialEstadosUserStory_miembro_sprint_id_97d924e6" ON public."HistorialEstadosUserStory" USING btree (miembro_sprint_id);


--
-- TOC entry 3133 (class 1259 OID 99726)
-- Name: HistorialEstadosUserStory_sprint_id_9a819c28; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "HistorialEstadosUserStory_sprint_id_9a819c28" ON public."HistorialEstadosUserStory" USING btree (sprint_id);


--
-- TOC entry 3134 (class 1259 OID 99727)
-- Name: HistorialEstadosUserStory_us_id_eaf9cf48; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "HistorialEstadosUserStory_us_id_eaf9cf48" ON public."HistorialEstadosUserStory" USING btree (us_id);


--
-- TOC entry 3135 (class 1259 OID 99728)
-- Name: HistorialUserStory_miembro_sprint_id_4a178fe9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "HistorialUserStory_miembro_sprint_id_4a178fe9" ON public."HistorialUserStory" USING btree (miembro_sprint_id);


--
-- TOC entry 3138 (class 1259 OID 99729)
-- Name: HistorialUserStory_sprint_id_ab3680f1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "HistorialUserStory_sprint_id_ab3680f1" ON public."HistorialUserStory" USING btree (sprint_id);


--
-- TOC entry 3139 (class 1259 OID 99730)
-- Name: HistorialUserStory_us_id_6b5f4d9e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "HistorialUserStory_us_id_6b5f4d9e" ON public."HistorialUserStory" USING btree (us_id);


--
-- TOC entry 3144 (class 1259 OID 99731)
-- Name: MiembrosSprint_sprint_id_be08be9b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "MiembrosSprint_sprint_id_be08be9b" ON public."MiembrosSprint" USING btree (sprint_id);


--
-- TOC entry 3145 (class 1259 OID 99732)
-- Name: MiembrosSprint_team_member_id_7da3cbbd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "MiembrosSprint_team_member_id_7da3cbbd" ON public."MiembrosSprint" USING btree (team_member_id);


--
-- TOC entry 3146 (class 1259 OID 99733)
-- Name: MiembrosSprint_usuario_id_fdc48280; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "MiembrosSprint_usuario_id_fdc48280" ON public."MiembrosSprint" USING btree (usuario_id);


--
-- TOC entry 3149 (class 1259 OID 99734)
-- Name: Nota_us_id_3b2c9dd2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Nota_us_id_3b2c9dd2" ON public."Nota" USING btree (us_id);


--
-- TOC entry 3150 (class 1259 OID 99735)
-- Name: Permiso_nombre_271e96c7_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Permiso_nombre_271e96c7_like" ON public."Permiso" USING btree (nombre varchar_pattern_ops);


--
-- TOC entry 3157 (class 1259 OID 99736)
-- Name: Profile_rol_sistema_id_827380ed; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Profile_rol_sistema_id_827380ed" ON public."Profile" USING btree (rol_sistema_id);


--
-- TOC entry 3160 (class 1259 OID 99737)
-- Name: Proyecto_nombre_f702d6e4_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_nombre_f702d6e4_like" ON public."Proyecto" USING btree (nombre varchar_pattern_ops);


--
-- TOC entry 3170 (class 1259 OID 99738)
-- Name: RolProyecto_permisos_permiso_id_34d8375b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "RolProyecto_permisos_permiso_id_34d8375b" ON public."RolProyecto_permisos" USING btree (permiso_id);


--
-- TOC entry 3173 (class 1259 OID 99739)
-- Name: RolProyecto_permisos_rolproyecto_id_c13208de; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "RolProyecto_permisos_rolproyecto_id_c13208de" ON public."RolProyecto_permisos" USING btree (rolproyecto_id);


--
-- TOC entry 3169 (class 1259 OID 99740)
-- Name: RolProyecto_proyecto_id_516c5737; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "RolProyecto_proyecto_id_516c5737" ON public."RolProyecto" USING btree (proyecto_id);


--
-- TOC entry 3176 (class 1259 OID 99741)
-- Name: RolSistema_nombre_1354dfb2_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "RolSistema_nombre_1354dfb2_like" ON public."RolSistema" USING btree (nombre varchar_pattern_ops);


--
-- TOC entry 3181 (class 1259 OID 99742)
-- Name: RolSistema_permisos_permiso_id_be4481bb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "RolSistema_permisos_permiso_id_be4481bb" ON public."RolSistema_permisos" USING btree (permiso_id);


--
-- TOC entry 3184 (class 1259 OID 99743)
-- Name: RolSistema_permisos_rolsistema_id_bf0cfa08; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "RolSistema_permisos_rolsistema_id_bf0cfa08" ON public."RolSistema_permisos" USING btree (rolsistema_id);


--
-- TOC entry 3194 (class 1259 OID 99744)
-- Name: Tablero_sprint_id_df7f4e58; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Tablero_sprint_id_df7f4e58" ON public."Tablero" USING btree (sprint_id);


--
-- TOC entry 3195 (class 1259 OID 99745)
-- Name: Tablero_us_id_7cd4ac05; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Tablero_us_id_7cd4ac05" ON public."Tablero" USING btree (us_id);


--
-- TOC entry 3200 (class 1259 OID 99746)
-- Name: TeamMember_proyecto_id_45fff58a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "TeamMember_proyecto_id_45fff58a" ON public."TeamMember" USING btree (proyecto_id);


--
-- TOC entry 3201 (class 1259 OID 99747)
-- Name: TeamMember_rol_proyecto_id_39790d89; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "TeamMember_rol_proyecto_id_39790d89" ON public."TeamMember" USING btree (rol_proyecto_id);


--
-- TOC entry 3202 (class 1259 OID 99748)
-- Name: TeamMember_usuario_id_d255f0a3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "TeamMember_usuario_id_d255f0a3" ON public."TeamMember" USING btree (usuario_id);


--
-- TOC entry 3203 (class 1259 OID 99749)
-- Name: UserStory_miembro_id_92908534; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "UserStory_miembro_id_92908534" ON public."UserStory" USING btree (miembro_id);


--
-- TOC entry 3204 (class 1259 OID 99750)
-- Name: UserStory_miembro_sprint_id_060adfa7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "UserStory_miembro_sprint_id_060adfa7" ON public."UserStory" USING btree (miembro_sprint_id);


--
-- TOC entry 3207 (class 1259 OID 99751)
-- Name: UserStory_proyecto_id_af833476; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "UserStory_proyecto_id_af833476" ON public."UserStory" USING btree (proyecto_id);


--
-- TOC entry 3208 (class 1259 OID 99752)
-- Name: UserStory_sprint_id_54722c8e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "UserStory_sprint_id_54722c8e" ON public."UserStory" USING btree (sprint_id);


--
-- TOC entry 3209 (class 1259 OID 99753)
-- Name: UserStory_usuario_id_5b3ba766; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "UserStory_usuario_id_5b3ba766" ON public."UserStory" USING btree (usuario_id);


--
-- TOC entry 3210 (class 1259 OID 99754)
-- Name: account_emailaddress_email_03be32b2_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX account_emailaddress_email_03be32b2_like ON public.account_emailaddress USING btree (email varchar_pattern_ops);


--
-- TOC entry 3215 (class 1259 OID 99755)
-- Name: account_emailaddress_user_id_2c513194; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX account_emailaddress_user_id_2c513194 ON public.account_emailaddress USING btree (user_id);


--
-- TOC entry 3216 (class 1259 OID 99756)
-- Name: account_emailconfirmation_email_address_id_5b7f8c58; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX account_emailconfirmation_email_address_id_5b7f8c58 ON public.account_emailconfirmation USING btree (email_address_id);


--
-- TOC entry 3217 (class 1259 OID 99757)
-- Name: account_emailconfirmation_key_f43612bd_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX account_emailconfirmation_key_f43612bd_like ON public.account_emailconfirmation USING btree (key varchar_pattern_ops);


--
-- TOC entry 3222 (class 1259 OID 99758)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 3227 (class 1259 OID 99759)
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- TOC entry 3230 (class 1259 OID 99760)
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- TOC entry 3233 (class 1259 OID 99761)
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- TOC entry 3243 (class 1259 OID 99762)
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- TOC entry 3246 (class 1259 OID 99763)
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- TOC entry 3249 (class 1259 OID 99764)
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 3252 (class 1259 OID 99765)
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 3240 (class 1259 OID 99766)
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 3255 (class 1259 OID 99767)
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- TOC entry 3258 (class 1259 OID 99768)
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- TOC entry 3265 (class 1259 OID 99769)
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- TOC entry 3268 (class 1259 OID 99770)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 3269 (class 1259 OID 99771)
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- TOC entry 3191 (class 1259 OID 99772)
-- Name: mysite_sprint_proyecto_id_83a31937; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX mysite_sprint_proyecto_id_83a31937 ON public."Sprint" USING btree (proyecto_id);


--
-- TOC entry 3278 (class 1259 OID 99773)
-- Name: socialaccount_socialaccount_user_id_8146e70c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialaccount_user_id_8146e70c ON public.socialaccount_socialaccount USING btree (user_id);


--
-- TOC entry 3285 (class 1259 OID 99774)
-- Name: socialaccount_socialapp_sites_site_id_2579dee5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialapp_sites_site_id_2579dee5 ON public.socialaccount_socialapp_sites USING btree (site_id);


--
-- TOC entry 3286 (class 1259 OID 99775)
-- Name: socialaccount_socialapp_sites_socialapp_id_97fb6e7d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialapp_sites_socialapp_id_97fb6e7d ON public.socialaccount_socialapp_sites USING btree (socialapp_id);


--
-- TOC entry 3287 (class 1259 OID 99776)
-- Name: socialaccount_socialtoken_account_id_951f210e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialtoken_account_id_951f210e ON public.socialaccount_socialtoken USING btree (account_id);


--
-- TOC entry 3288 (class 1259 OID 99777)
-- Name: socialaccount_socialtoken_app_id_636a42d7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialtoken_app_id_636a42d7 ON public.socialaccount_socialtoken USING btree (app_id);


--
-- TOC entry 3293 (class 2606 OID 99778)
-- Name: Actividad Actividad_miembro_id_1d4e65be_fk_MiembrosSprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Actividad"
    ADD CONSTRAINT "Actividad_miembro_id_1d4e65be_fk_MiembrosSprint_id" FOREIGN KEY (miembro_id) REFERENCES public."MiembrosSprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3294 (class 2606 OID 99783)
-- Name: Actividad Actividad_sprint_id_45c4b485_fk_mysite_sprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Actividad"
    ADD CONSTRAINT "Actividad_sprint_id_45c4b485_fk_mysite_sprint_id" FOREIGN KEY (sprint_id) REFERENCES public."Sprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3295 (class 2606 OID 99788)
-- Name: Actividad Actividad_us_id_253afcd9_fk_UserStory_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Actividad"
    ADD CONSTRAINT "Actividad_us_id_253afcd9_fk_UserStory_id" FOREIGN KEY (us_id) REFERENCES public."UserStory"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3296 (class 2606 OID 99793)
-- Name: HistorialEstadosUserStory HistorialEstadosUserStory_sprint_id_9a819c28_fk_Sprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialEstadosUserStory"
    ADD CONSTRAINT "HistorialEstadosUserStory_sprint_id_9a819c28_fk_Sprint_id" FOREIGN KEY (sprint_id) REFERENCES public."Sprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3297 (class 2606 OID 99798)
-- Name: HistorialEstadosUserStory HistorialEstadosUserStory_us_id_eaf9cf48_fk_UserStory_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialEstadosUserStory"
    ADD CONSTRAINT "HistorialEstadosUserStory_us_id_eaf9cf48_fk_UserStory_id" FOREIGN KEY (us_id) REFERENCES public."UserStory"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3298 (class 2606 OID 99803)
-- Name: HistorialEstadosUserStory HistorialEstadosUser_miembro_sprint_id_97d924e6_fk_MiembrosS; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialEstadosUserStory"
    ADD CONSTRAINT "HistorialEstadosUser_miembro_sprint_id_97d924e6_fk_MiembrosS" FOREIGN KEY (miembro_sprint_id) REFERENCES public."MiembrosSprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3299 (class 2606 OID 99808)
-- Name: HistorialUserStory HistorialUserStory_miembro_sprint_id_4a178fe9_fk_MiembrosS; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialUserStory"
    ADD CONSTRAINT "HistorialUserStory_miembro_sprint_id_4a178fe9_fk_MiembrosS" FOREIGN KEY (miembro_sprint_id) REFERENCES public."MiembrosSprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3300 (class 2606 OID 99813)
-- Name: HistorialUserStory HistorialUserStory_sprint_id_ab3680f1_fk_Sprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialUserStory"
    ADD CONSTRAINT "HistorialUserStory_sprint_id_ab3680f1_fk_Sprint_id" FOREIGN KEY (sprint_id) REFERENCES public."Sprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3301 (class 2606 OID 99818)
-- Name: HistorialUserStory HistorialUserStory_us_id_6b5f4d9e_fk_UserStory_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HistorialUserStory"
    ADD CONSTRAINT "HistorialUserStory_us_id_6b5f4d9e_fk_UserStory_id" FOREIGN KEY (us_id) REFERENCES public."UserStory"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3302 (class 2606 OID 99823)
-- Name: MiembrosSprint MiembrosSprint_sprint_id_be08be9b_fk_mysite_sprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."MiembrosSprint"
    ADD CONSTRAINT "MiembrosSprint_sprint_id_be08be9b_fk_mysite_sprint_id" FOREIGN KEY (sprint_id) REFERENCES public."Sprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3303 (class 2606 OID 99828)
-- Name: MiembrosSprint MiembrosSprint_team_member_id_7da3cbbd_fk_TeamMember_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."MiembrosSprint"
    ADD CONSTRAINT "MiembrosSprint_team_member_id_7da3cbbd_fk_TeamMember_id" FOREIGN KEY (team_member_id) REFERENCES public."TeamMember"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3304 (class 2606 OID 99833)
-- Name: MiembrosSprint MiembrosSprint_usuario_id_fdc48280_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."MiembrosSprint"
    ADD CONSTRAINT "MiembrosSprint_usuario_id_fdc48280_fk_auth_user_id" FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3305 (class 2606 OID 99838)
-- Name: Nota Nota_us_id_3b2c9dd2_fk_UserStory_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Nota"
    ADD CONSTRAINT "Nota_us_id_3b2c9dd2_fk_UserStory_id" FOREIGN KEY (us_id) REFERENCES public."UserStory"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3306 (class 2606 OID 99843)
-- Name: Profile Profile_rol_sistema_id_827380ed_fk_RolSistema_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Profile"
    ADD CONSTRAINT "Profile_rol_sistema_id_827380ed_fk_RolSistema_id" FOREIGN KEY (rol_sistema_id) REFERENCES public."RolSistema"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3307 (class 2606 OID 99848)
-- Name: Profile Profile_user_id_fa0e72a2_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Profile"
    ADD CONSTRAINT "Profile_user_id_fa0e72a2_fk_auth_user_id" FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3309 (class 2606 OID 99853)
-- Name: RolProyecto_permisos RolProyecto_permisos_permiso_id_34d8375b_fk_Permiso_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto_permisos"
    ADD CONSTRAINT "RolProyecto_permisos_permiso_id_34d8375b_fk_Permiso_id" FOREIGN KEY (permiso_id) REFERENCES public."Permiso"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3310 (class 2606 OID 99858)
-- Name: RolProyecto_permisos RolProyecto_permisos_rolproyecto_id_c13208de_fk_RolProyecto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto_permisos"
    ADD CONSTRAINT "RolProyecto_permisos_rolproyecto_id_c13208de_fk_RolProyecto_id" FOREIGN KEY (rolproyecto_id) REFERENCES public."RolProyecto"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3308 (class 2606 OID 99863)
-- Name: RolProyecto RolProyecto_proyecto_id_516c5737_fk_Proyecto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolProyecto"
    ADD CONSTRAINT "RolProyecto_proyecto_id_516c5737_fk_Proyecto_id" FOREIGN KEY (proyecto_id) REFERENCES public."Proyecto"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3311 (class 2606 OID 99868)
-- Name: RolSistema_permisos RolSistema_permisos_permiso_id_be4481bb_fk_Permiso_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolSistema_permisos"
    ADD CONSTRAINT "RolSistema_permisos_permiso_id_be4481bb_fk_Permiso_id" FOREIGN KEY (permiso_id) REFERENCES public."Permiso"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3312 (class 2606 OID 99873)
-- Name: RolSistema_permisos RolSistema_permisos_rolsistema_id_bf0cfa08_fk_RolSistema_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolSistema_permisos"
    ADD CONSTRAINT "RolSistema_permisos_rolsistema_id_bf0cfa08_fk_RolSistema_id" FOREIGN KEY (rolsistema_id) REFERENCES public."RolSistema"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3314 (class 2606 OID 99878)
-- Name: Tablero Tablero_sprint_id_df7f4e58_fk_mysite_sprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tablero"
    ADD CONSTRAINT "Tablero_sprint_id_df7f4e58_fk_mysite_sprint_id" FOREIGN KEY (sprint_id) REFERENCES public."Sprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3315 (class 2606 OID 99883)
-- Name: Tablero Tablero_us_id_7cd4ac05_fk_UserStory_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tablero"
    ADD CONSTRAINT "Tablero_us_id_7cd4ac05_fk_UserStory_id" FOREIGN KEY (us_id) REFERENCES public."UserStory"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3316 (class 2606 OID 99888)
-- Name: TeamMember TeamMember_proyecto_id_45fff58a_fk_Proyecto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TeamMember"
    ADD CONSTRAINT "TeamMember_proyecto_id_45fff58a_fk_Proyecto_id" FOREIGN KEY (proyecto_id) REFERENCES public."Proyecto"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3317 (class 2606 OID 99893)
-- Name: TeamMember TeamMember_rol_proyecto_id_39790d89_fk_RolProyecto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TeamMember"
    ADD CONSTRAINT "TeamMember_rol_proyecto_id_39790d89_fk_RolProyecto_id" FOREIGN KEY (rol_proyecto_id) REFERENCES public."RolProyecto"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3318 (class 2606 OID 99898)
-- Name: TeamMember TeamMember_usuario_id_d255f0a3_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TeamMember"
    ADD CONSTRAINT "TeamMember_usuario_id_d255f0a3_fk_auth_user_id" FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3319 (class 2606 OID 99903)
-- Name: UserStory UserStory_miembro_id_92908534_fk_TeamMember_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserStory"
    ADD CONSTRAINT "UserStory_miembro_id_92908534_fk_TeamMember_id" FOREIGN KEY (miembro_id) REFERENCES public."TeamMember"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3320 (class 2606 OID 99908)
-- Name: UserStory UserStory_miembro_sprint_id_060adfa7_fk_MiembrosSprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserStory"
    ADD CONSTRAINT "UserStory_miembro_sprint_id_060adfa7_fk_MiembrosSprint_id" FOREIGN KEY (miembro_sprint_id) REFERENCES public."MiembrosSprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3321 (class 2606 OID 99913)
-- Name: UserStory UserStory_proyecto_id_af833476_fk_Proyecto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserStory"
    ADD CONSTRAINT "UserStory_proyecto_id_af833476_fk_Proyecto_id" FOREIGN KEY (proyecto_id) REFERENCES public."Proyecto"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3322 (class 2606 OID 99918)
-- Name: UserStory UserStory_sprint_id_54722c8e_fk_mysite_sprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserStory"
    ADD CONSTRAINT "UserStory_sprint_id_54722c8e_fk_mysite_sprint_id" FOREIGN KEY (sprint_id) REFERENCES public."Sprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3323 (class 2606 OID 99923)
-- Name: UserStory UserStory_usuario_id_5b3ba766_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserStory"
    ADD CONSTRAINT "UserStory_usuario_id_5b3ba766_fk_auth_user_id" FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3324 (class 2606 OID 99928)
-- Name: account_emailaddress account_emailaddress_user_id_2c513194_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_user_id_2c513194_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3325 (class 2606 OID 99933)
-- Name: account_emailconfirmation account_emailconfirmation_email_address_id_5b7f8c58_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_email_address_id_5b7f8c58_fk FOREIGN KEY (email_address_id) REFERENCES public.account_emailaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3326 (class 2606 OID 99938)
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3327 (class 2606 OID 99943)
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3328 (class 2606 OID 99948)
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3329 (class 2606 OID 99953)
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3330 (class 2606 OID 99958)
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3331 (class 2606 OID 99963)
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3332 (class 2606 OID 99968)
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3333 (class 2606 OID 99973)
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3334 (class 2606 OID 99978)
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3313 (class 2606 OID 99983)
-- Name: Sprint mysite_sprint_proyecto_id_83a31937_fk_Proyecto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT "mysite_sprint_proyecto_id_83a31937_fk_Proyecto_id" FOREIGN KEY (proyecto_id) REFERENCES public."Proyecto"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3336 (class 2606 OID 99988)
-- Name: socialaccount_socialapp_sites socialaccount_social_site_id_2579dee5_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_social_site_id_2579dee5_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3335 (class 2606 OID 99993)
-- Name: socialaccount_socialaccount socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3337 (class 2606 OID 99998)
-- Name: socialaccount_socialtoken socialaccount_socialtoken_account_id_951f210e_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_account_id_951f210e_fk FOREIGN KEY (account_id) REFERENCES public.socialaccount_socialaccount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3338 (class 2606 OID 100003)
-- Name: socialaccount_socialtoken socialaccount_socialtoken_app_id_636a42d7_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_app_id_636a42d7_fk FOREIGN KEY (app_id) REFERENCES public.socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


-- Completed on 2021-11-19 04:23:24 -03

--
-- PostgreSQL database dump complete
--

