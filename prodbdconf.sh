#!/bin/bash
echo "---Base de datos produccionBD para entorno de Producción---"
echo "Borrando base de datos produccionBD existente..."
dropdb -i --if-exists produccionBD
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo borrar la base de datos produccionBD, verifique que no esté siendo usada."
    exit 1
fi
echo "Se ha borrado la base de datos produccionBD."
echo "Creando la base de datos produccionBD..."
createdb produccionBD
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo crear produccionBD"
    exit 2
fi
echo "Se ha creado produccionBD"

source venv/bin/activate
PGPASSWORD="postgres"
psql -h localhost -p 5432 -U postgres -d produccionBD -f produccionBD.sql
echo "produccionBD se cargó exitosamente."
