from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.proyecto.model import Proyecto
from django.contrib.auth.models import User
from django.test import Client
import datetime


class Sprint(TestCase):
    def test_pagina(self):
        """
        Carga la página y verifica que no encuentra
        """
        client = Client()
        response = client.get("/sprint/")
        #HTTP_302_FOUND
        self.assertEqual(response.status_code, 404)

    def test_create_sprint(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_prueba2" + str(datetime.datetime.now())
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'En Proceso'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        dias_laborales=2
        dias_habiles=[1,2,3,4,5,6,7]
        # fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_inicio=fecha_ini, fecha_fin=fecha_fin,dias_laborales=dias_laborales,dias_habiles=dias_habiles)
                            #   fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        # """crea un proyecto un segundo proyecto"""
        # """
        # proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        # proyectoNvo2.save()

        # sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
        #                        fecha_fin_est=fecha_fin_est)
        # """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise