import unittest
from django.test import TestCase
from mysite.modelos.rol.model  import RolProyecto, RolSistema, Permiso
from django.contrib.auth.models import User
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mateo.base")
from django.urls import reverse
from rest_framework import status 
from django.db import IntegrityError # PARA CAPTURAR ERRORES EN LA DATABASE
from django.test import Client



class Test(TestCase):
    """
    Test de creacion de Rol vacio
    """
    def test_creacionRol_Sistema(self):
        rol1 = RolSistema()
        self.assertIsNotNone(rol1)

    def test_creacionRol_Proyecto(self):
        rol1 = RolProyecto
        self.assertIsNotNone(rol1)

    #  def test_rol_NombreRol_Es_Obligatorio(self):
    #      with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
    #          User.objects.create_user('prueba','prueba@gmail.com','prueba')

    #          rol2=RolProyecto(descripcion="hola") 
    #          rol2.save()
            
    #      self.assertTrue0('NOT NULL constraint failed' in str(context.exception)) 

    # def test_permiso_NombrePermiso_Es_Obligatorio(self):
    #     with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
    #         User.objects.create_user('prueba','prueba@gmail.com','prueba')

    #         permiso1=Permiso( tipo_permiso = '1') 
    #         permiso1.save()
            
    #     self.assertTrue0('NOT NULL constraint failed' in str(context.exception)) 
