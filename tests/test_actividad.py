from django.db import IntegrityError # PARA CAPTURAR ERRORES EN LA DATABASE
from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from django.test import Client
from mysite.modelos.actividad.model import Actividad
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.proyecto.model import Proyecto
from mysite.modelos.miembros_sprint.model import MiembrosSprint
import datetime

class Test(TestCase):
    def test_actividad(self):
        nro= str(datetime.datetime.now())
        nombre = "proyecto_pruebas_unitarias" +nro
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
        proyectoNvo.save()
        # proyecto='descripcion_completa'
        nombre ='us1'
        valor_negocio =1
        prioridad =2
        valor_tecnico =2
        estado ='Pendiente'
        estado_tablero ='To Do'
        us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                      valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
        us_nuevo.save()              
        actividad=Actividad(nombre='actividad1',duracion=200,fecha='2001-01-01',us=us_nuevo)

        assert actividad.nombre == 'actividad1'


    def test_pagina(self):
        """
        Carga la página y verifica que encuentra
        """
        client = Client()
        response = client.get("/actividad/")
        #HTTP_302_FOUND
        self.assertEqual(response.status_code, 404)

    def test_Actividad_Nombre_Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            vacio=None
            actividad=Actividad(nombre=vacio,duracion=200) #como intentamos crear una nota sin descripciob, salta error de integridad
            actividad.save() #SI Comentamos LA linea siguiente salta error

    def test_Actividad_Duracion_Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            vacio=None
            actividad=Actividad(nombre='actividad2',duracion=vacio) #como intentamos crear una nota sin descripciob, salta error de integridad
            actividad.save() #SI Comentamos LA linea siguiente salta error

    def test_Actividad_Fecha_Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            vacio=None
            actividad=Actividad(nombre='actividad',duracion=200,fecha=vacio) #como intentamos crear una nota sin descripciob, salta error de integridad
            actividad.save() #SI Comentamos LA linea siguiente salta error

    def test_Actividad_Us_Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            vacio=None
            actividad=Actividad(nombre=vacio,duracion=200,fecha='2001-01-01',us=vacio) #como intentamos crear una nota sin descripciob, salta error de integridad
            actividad.save() #SI Comentamos LA linea siguiente salta error