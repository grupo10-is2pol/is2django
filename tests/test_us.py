from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.proyecto.model import Proyecto
from django.test import Client
import datetime

class UserStroy(TestCase):
    def test_pagina(self):
        """
        Carga la página y verifica que encuentra
        """
        client = Client()
        response = client.get("/us/")
        #HTTP_302_FOUND
        self.assertEqual(response.status_code, 404)

    def test_create_us(self):
        """
        Verifica que un us se crea
        """
    try:
        nro= str(datetime.datetime.now())
        nombre = "proyecto_pruebas_unitarias" +nro
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
        proyectoNvo.save()
        # proyecto='descripcion_completa'
        nombre ='us1'
        valor_negocio =1
        prioridad =2
        valor_tecnico =2
        estado ='Pendiente'
        estado_tablero ='To Do'
        descripcion ='asfd'
        priorizacion=3.5
        duracion_estimada =2
        sprint ='sprint1'
        miembro_sprint ='usuario1'
        miembro ='usuario1'
        usuario ='usuario1'
        duracion_restante =2
        us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                      valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
        us_nuevo.save()
        us_nuevo.delete()

    except:
        # us_nuevo.delete()
        raise

    def test_update_us(self):
        """
        Verifica que un us se modifica
        """
    try:
        nro= str(datetime.datetime.now())
        nombre = nro
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
        proyectoNvo.save()
       
        nombre='us2'
        descripcion  = 'descripcion_breve'
        valor_negocio = 2
        prioridad = 3
        valor_tecnico = 3
        estado ='Pendiente'
        estado_tablero ='To Do'
        priorizacion=3.5
        duracion_estimada =2
        sprint ='sprint2'
        miembro_sprint ='usuario1'
        miembro ='usuario1'
        usuario ='usuario1'
        duracion_restante =2
        us_nuevo = UserStory(nombre=nombre,proyecto=proyectoNvo,descripcion=descripcion,
                      valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico,estado=estado,
                      estado_tablero=estado_tablero)
        us_nuevo.save()
        us_nuevo2 = UserStory(nombre=nombre,proyecto=proyectoNvo,descripcion ='descripcion_breve2',
                       valor_negocio=3, prioridad=4, valor_tecnico=5,estado_tablero=estado_tablero)
        us_nuevo.save()
        us_nuevo2.save()
        # No falla (siguiente 7 lineas):
        us_nuevo2.descripcion = 'descripcion_breve3'
        us_nuevo2.valor_negocio=4
        us_nuevo2.prioridad = 5
        us_nuevo2.valor_tecnico =6
        #us_nuevo2.valor_tecnico = 'cadena'
        us_nuevo2.save()
        us_nuevo.delete()
        us_nuevo2.delete()
    except:
        # us_nuevo.delete()
        # us_nuevo2.delete()
        raise

    def test_delete_us(self):
        """
        Verifica que un us se elimina sin problemas
        """
        try:
            nro= str(datetime.datetime.now())
            nombre = nro
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
            proyectoNvo.save()
            
            nombre ='us3'
            valor_negocio =1
            prioridad =2
            valor_tecnico =2
            estado ='Pendiente'
            estado_tablero ='To Do'
            us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                        valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
            us_nuevo.save()
            us_nuevo.delete()
        
            #Falla(intenta borrar algo que no existe,(descomentar linea de abajo)
            #us_nuevo.delete()
        except:
            raise