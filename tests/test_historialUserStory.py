from django.db import IntegrityError # PARA CAPTURAR ERRORES EN LA DATABASE
from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from mysite.modelos.historial_userstory.model import HistorialUserStory
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.proyecto.model import Proyecto,TeamMember
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.miembros_sprint.model import MiembrosSprint
from django.test import Client
import datetime

class Test(TestCase):
    def test_Historial_Us_Create(self):
        nro= str(datetime.datetime.now())
        nombre = "proyecto_pruebas_unitarias" +nro
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
        proyectoNvo.save()
        nombre ='us1'
        valor_negocio =1
        prioridad =2
        valor_tecnico =2
        estado ='Pendiente'
        estado_tablero ='To Do'
        us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                      valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
        us_nuevo.save()
        estado = 'En Proceso'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        dias_laborales=2
        dias_habiles=[1,2,3,4,5,6,7]
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_inicio=fecha_ini, fecha_fin=fecha_fin,dias_laborales=dias_laborales,dias_habiles=dias_habiles)
        sprint_nuevo.save()
        teamMember_nvo=TeamMember(proyecto=proyectoNvo)
        teamMember_nvo.save()
        horas_laborales=12,
        miembroSprint=MiembrosSprint(horas_laborales=12,team_member=teamMember_nvo,sprint=sprint_nuevo)
        miembroSprint.save()
        historialUs=HistorialUserStory(us=us_nuevo,sprint=sprint_nuevo,miembro_sprint=miembroSprint,duracion_estimada_sprint=5)
        historialUs.save()
        
    def test_Us__Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            nro= str(datetime.datetime.now())
            nombre = "proyecto_pruebas_unitarias" +nro
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
            proyectoNvo.save()
            nombre ='us1'
            valor_negocio =1
            prioridad =2
            valor_tecnico =2
            estado ='Pendiente'
            estado_tablero ='To Do'
            us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                        valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
            us_nuevo.save()
            estado = 'En Proceso'
            fecha_ini = '2018-01-01'
            fecha_fin = '2018-01-15'
            dias_laborales=2
            dias_habiles=[1,2,3,4,5,6,7]
            sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_inicio=fecha_ini, fecha_fin=fecha_fin,dias_laborales=dias_laborales,dias_habiles=dias_habiles)
            sprint_nuevo.save()
            vacio=None
            historialUs=HistorialUserStory(us=vacio,sprint=sprint_nuevo) #como intentamos crear una nota sin descripciob, salta error de integridad
            historialUs.save() #SI Comentamos LA linea siguiente salta error

    def test_Sprint__Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            nro= str(datetime.datetime.now())
            nombre = "proyecto_pruebas_unitarias" +nro
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
            proyectoNvo.save()
            nombre ='us1'
            valor_negocio =1
            prioridad =2
            valor_tecnico =2
            estado ='Pendiente'
            estado_tablero ='To Do'
            us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                        valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
            us_nuevo.save()
            estado = 'En Proceso'
            fecha_ini = '2018-01-01'
            fecha_fin = '2018-01-15'
            dias_laborales=2
            dias_habiles=[1,2,3,4,5,6,7]
            sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_inicio=fecha_ini, fecha_fin=fecha_fin,dias_laborales=dias_laborales,dias_habiles=dias_habiles)
            sprint_nuevo.save()
            vacio=None
            historialUs=HistorialUserStory(us=us_nuevo,sprint=vacio) #como intentamos crear una nota sin descripciob, salta error de integridad
            historialUs.save() #SI Comentamos LA linea siguiente salta error

    def test_duracion_estimada_sprint__Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            nro= str(datetime.datetime.now())
            nombre = "proyecto_pruebas_unitarias" +nro
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
            proyectoNvo.save()
            nombre ='us1'
            valor_negocio =1
            prioridad =2
            valor_tecnico =2
            estado ='Pendiente'
            estado_tablero ='To Do'
            us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                        valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
            us_nuevo.save()
            estado = 'En Proceso'
            fecha_ini = '2018-01-01'
            fecha_fin = '2018-01-15'
            dias_laborales=2
            dias_habiles=[1,2,3,4,5,6,7]
            sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_inicio=fecha_ini, fecha_fin=fecha_fin,dias_laborales=dias_laborales,dias_habiles=dias_habiles)
            sprint_nuevo.save()
            vacio=None
            historialUs=HistorialUserStory(us=us_nuevo,sprint=sprint_nuevo,duracion_estimada_sprint=vacio) #como intentamos crear una nota sin descripciob, salta error de integridad
            historialUs.save() #SI Comentamos LA linea siguiente salta error