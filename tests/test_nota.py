from unittest import TestCase
from django.db import IntegrityError # PARA CAPTURAR ERRORES EN LA DATABASE
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from django.test import Client
from mysite.modelos.nota.model import Nota
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.proyecto.model import Proyecto
import datetime


class Test(TestCase):
    def test_nota(self):
        nro= str(datetime.datetime.now())
        nombre = "proyecto_pruebas_unitarias" +nro
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
        proyectoNvo.save()
        nombre ='us1'
        valor_negocio =1
        prioridad =2
        valor_tecnico =2
        estado ='Pendiente'
        estado_tablero ='To Do'
        us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                      valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
        us_nuevo.save() 
        nota=Nota(nota='nota1',us=us_nuevo)
        nota.save()
    
    def test_Nota_Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            vacio=None
            nota=Nota(nota='nota2',us=vacio) #como intentamos crear una nota sin descripciob, salta error de integridad
            nota.save() #SI Comentamos LA linea siguiente salta error

    def test_Nota_Us_Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            # User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            nro= str(datetime.datetime.now())
            nombre = "proyecto_pruebas_unitarias" +nro
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
            proyectoNvo.save()
            nombre ='us1'
            valor_negocio =1
            prioridad =2
            valor_tecnico =2
            estado ='Pendiente'
            estado_tablero ='To Do'
            us_nuevo = UserStory(proyecto= proyectoNvo,nombre=nombre,  valor_negocio=valor_negocio ,prioridad=prioridad,
                        valor_tecnico=valor_tecnico , estado=estado,estado_tablero=estado_tablero)
            us_nuevo.save() 
            vacio=None
            nota=Nota(nota=vacio,us=us_nuevo) #como intentamos crear una nota sin descripciob, salta error de integridad
            nota.save() #SI Comentamos LA linea siguiente salta error