from unittest import TestCase
import unittest
import time
import django
import os 
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mateo.base") 
django.setup()
from django.contrib.auth.models import User
# from mysite.modelos.profile.models import Profile
##from django.test import Client

"""
Prubas unitarias para la app usuario
"""

class Usuario(TestCase):
#     def test_pagina(self):
#         """
#         Carga la página y verifica que encuentra
#         """
#         client = Client()
#         response = client.get("/usuario/")
#         #HTTP_302_FOUND
#         self.assertEqual(response.status_code, 302)

    def test_create_usuario(self):
        """
        Verifica que un usuario se crea (y prueba que no se crean dos usuarios con el mismo username)
        """
        try:
            new_user = User.objects.create_user('prueba','maricris.orue@gmail.com','prueba')
            new_user.save()
            # Falla si se intenta crear un usuario con username que ya existe (descomentar linea de abajo)
            #new_user = User.objects.create_user('prueba', 'maricris.orue@gmail.com', 'prueba')
            new_user.delete()
        except:
            new_user.delete()
            raise

    # def test_update_usuario(self):
    #      """
    #      Verifica que un usuario se modifica (y no se modifica si el CI que intenta ser introducido ya existe)
    #      """
    #      try:
    #          new_user = User.objects.create_user('prueba','maricris.orue@gmail.com','prueba')
    #          new_user2 = User.objects.create_user('prueba2', 'maricris.orue@gmail.com', 'prueba')
    #          new_user.usuario.ci = 'prueba'
    #          new_user.save()
    #          new_user2.save()
    #          #No falla (siguiente linea):
    #          new_user2.usuario.ci = 'prueba2'
    #          #Falla (siguientes dos lineas, ci es único):
    #          #new_user2.usuario.ci = 'prueba'
    #          #new_user2.save()
    #          new_user.delete()
    #          new_user2.delete()
    #      except:
    #          new_user.delete()
    #          new_user2.delete()
    #          raise

    def test_delete_usuario(self):
        """
        Verifica que un usuario se elimina sin problemas (y que no se puede eliminar algo que no existe)
        """
        try:
            new_user = User.objects.create_user('prueba','maricris.orue@gmail.com','prueba')
            new_user.save()
            new_user.delete()
            #Falla(intenta borrar algo que no existe, linea de abajo):
            #new_user.delete()
        except:
            raise

    def test_update_own_password(self):
        """
        Verifica que un User puede loguearse luego de modificar su password
        (y no se loguea si el password no es correcto)
        """
        try:
            # client = Client()
            anterior = 'prueba'
            new_user = User.objects.create_user('prueba', 'maricris.orue@gmail.com', anterior)
            new_user.save()
            nueva = 'prueba2'
            new_user.set_password(nueva)
            new_user.save()
            #No falla:
            # self.assertEqual(client.login(username='prueba', password=nueva), True)
            #Falla:
            #self.assertEqual(client.login(username='prueba', password=anterior), True)
            new_user.delete()
        except:
            new_user.delete()
            raise