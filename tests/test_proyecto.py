import unittest
from django.test import TestCase
import time
from django.contrib.auth.models import User
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mateo.base")
from django.db import IntegrityError # PARA CAPTURAR ERRORES EN LA DATABASE
from django.test import Client
from mysite.modelos.proyecto.model import Proyecto,TeamMember
## from flujo.models import Flujo, Fase

class Test(TestCase):
    """
        Test de Creacion de Proyecto
    """
    def test_creacionProyecto(self):
        project = Proyecto()
        self.assertIsNotNone(project)


    def test_fechasProyecto(self):
        """Test de validacion de fechas de inicio y fin de proyecto"""
        proyecto1 = Proyecto(nombre='Proyecto1', fecha_inicio='22/04/2019', fecha_fin='23/04/2019')
        fechaInicio = time.strptime(proyecto1.fecha_inicio, "%d/%m/%Y")
        fechaFin = time.strptime(proyecto1.fecha_fin, "%d/%m/%Y")
        self.assertLessEqual(fechaInicio, fechaFin, "La Fecha de Inicio debe ser menor a la fecha de Fin")


    def test_update_proyeto(self):
         """
         Verifica que un proyecto se modifica
         """
         proyecto1=Proyecto(nombre="prueba1",descripcion="hola")
         

         proyecto1.descripcion="holaModificado"
         #incluir otros campos para probar
         # .. . . . . . . .
         self.assertIsNotNone(proyecto1)

             #ESTA ES OTRA FORMA "MUCHO MEJOR :D"
    def test_proyecto_Unique_constraint(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            User.objects.create_user('prueba2','prueba@gmail.com','prueba')
            proyecto1=Proyecto(nombre="prueba1",descripcion="hola")
            proyecto1.save()
            #SI COMENTAMOS LAS SIGUIENTE DOS LINEAS FALLA LA PRUEBA
            proyecto2=Proyecto(nombre="prueba1",descripcion="hola")
            proyecto2.save()
        
        

    # def test_proyecto_usuarioScrum_Es_Obligatorio(self):
    #     with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
    #         User.objects.create_user('prueba3','prueba@gmail.com','prueba')

    #         proyecto1=Proyecto(nombre="prueba1",descripcion="hola") #como no ponemos usuario_scrum_id, se captura el error
    #         #SI DESCOMENTAMOS LA linea siguiente salta error
    #         proyecto1.save()
            
    #     self.assertFalse('NOT NULL constraint failed' in str(context.exception))
        
    def test_proyecto_NombreProyecto_Es_Obligatorio(self):
        with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
            User.objects.create_user('prueba4','prueba@gmail.com','prueba')
            vacio=None
            proyecto3=Proyecto(nombre=vacio,descripcion="hola") #como intentamos crear un proyecto sin Nombre, salta error de integridad
            proyecto3.save() #SI Comentamos LA linea siguiente salta error

    

    #def test_teamMeamber(self):
    #    with self.assertRaises(IntegrityError) as context: #aca se captura el IntegrityError
    #        User.objects.create_user('prueba','prueba@gmail.com','prueba')
    #
    #       TeamMember1=TeamMember(usuario_id=1) #como no ponemos usuario_scrum_id, se captura el error
     #       #SI DESCOMENTAMOS LA linea siguiente salta error
     #       TeamMember1.save()
            
        #self.assertTrue('NOT NULL constraint failed' in str(context.exception))
    # def test_delete_proyecto(self):
    #     """
    #     Verifica que un proyecto se elimina sin problemas
    #     """
    #     try:
    #         nombre = "prueba"
    #         fecha_ini = '2001-01-01'
    #         fecha_fin = '2002-01-01'
    #         observaciones = 'obs'
    #         proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
    #         proyectoNvo.save()
    #         proyectoNvo.delete()
    #         #Falla(intenta borrar algo que no existe, linea de abajo):
    #         #proyectoNvo.delete()
    #     except:
    #         raise

     #Crea un proyecto auxiliar para las pruebas
    # def creacion_auxiliar(self, nombre='Testeando Proyecto'):
        
    #     u = user.objects.create(user='Test_user', password='dummy password')

    #     datos = {
    #         'nombre': nombre,
    #         'fecha_inicio': timezone.now(),
    #         'fecha_fin': timezone.now() + timezone.timedelta(days=20),
    #         'scrum_master': u
    #     }

    #     return Proyecto.objects.create(**datos)

    # #Utiliza la funcion "creacion_auxiliar" para crear proyecto de prueba
    # def test_creacionProyecto(self):
    #     proyectos = self.creacion_auxiliar()

    # #Prueba la eliminacion de un proyecto
    # def test_eliminacionProyecto(self):
    #     proyectos = self.creacion_auxiliar()
    #     proyectos.delete()

    # #Funciona pero no se que hace
    # def test_chekeoParametro(self):
    #     no_nombre = {
    
    #     }
    #     self.assertRaises(KeyError, Proyecto.proyectos.crear_proyecto, **no_nombre)

    # #Prueba el funcionamiento del get_nombre()
    # def test_get_nombre(self):
    #     nombre = 'Test'
    #     proyecto = self.creacion_auxiliar(nombre)
    #     self.assertEqual(nombre, proyecto.get_nombre())