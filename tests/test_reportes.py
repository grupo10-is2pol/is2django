from unittest import TestCase
from django.db import IntegrityError # PARA CAPTURAR ERRORES EN LA DATABASE
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from django.test import Client
from mysite.modelos.nota.model import Nota
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.proyecto.model import Proyecto
import datetime

from mysite.vistas.reportes_pdf.vista import *


class Test(TestCase):

    def validar_response_pdf( self, response ):
        """
            Valida que el response sea un pdf
                # Checkea que la respuesta tenga status code 200(OK)
                # Checkea que la respuesta sea un pdf
                # Checkea que el contenido tenga una longitud mayor a 0
                # Checkea que no tenga contenido vacío
        """
        self.assertTrue(response is not None)
        # Checkear que la respuesta sea OK
        self.assertTrue(response.status_code == 200)
        # Checkear que la respuesta sea un pdf
        self.assertTrue(response['Content-Type'] == 'application/pdf')
        # Checkear que el contenido tenga una longitud mayor a 0
        self.assertTrue(response.content)
        # Checkear que no tenga contenido vacío
        self.assertTrue(len(response.content) > 0)

    def test_generacion_springbacklog_pdf(self):       
        """
        Verifica que el pdf del sprint backlog se crea         
        """
        try:
            """crea un proyecto"""
            nombre = "proyecto_prueba_" + str(datetime.datetime.now())
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
            proyectoNvo.save()

            """crea un sprint"""
            estado = 'En Proceso'
            proyecto = 'descripcion_completa'
            fecha_ini = '2018-01-01'
            fecha_fin = '2018-01-15'
            dias_laborales=2
            dias_habiles=[1,2,3,4,5,6,7]
            # fecha_fin_est = '2018-01-16'
            sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_inicio=fecha_ini, fecha_fin=fecha_fin,dias_laborales=dias_laborales,dias_habiles=dias_habiles)
            sprint_nuevo.save()

            response = SprintBacklogPDF().get( None, None,  codigo_ejecucion=proyectoNvo.pk, codigo_sprint=sprint_nuevo.pk )        
            self.validar_response_pdf( response )

        except:
            #proyectoNvo.delete()
            #proyectoNvo2.delete()
            raise

    def test_generacion_pdf_product_backlog(self):
        """
            Valida la generación del pdf del product_backlog
        """
        """crea un proyecto"""
        nombre = "proyecto_prueba_" + str(datetime.datetime.now())
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
        proyectoNvo.save()

        response = ProductBacklogPDF().get( None, None, codigo_ejecucion=proyectoNvo.pk )
        self.validar_response_pdf( response )

    def test_generacion_pdf_horastrabajadas_pdf(self):
        """
            Valida la generación del pdf del horas trabajadas
        """
        """crea un proyecto"""
        nombre = "proyecto_prueba_" + str(datetime.datetime.now())
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_inicio=fecha_ini, fecha_fin=fecha_fin, descripcion=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'En Proceso'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        dias_laborales=2
        dias_habiles=[1,2,3,4,5,6,7]
        # fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_inicio=fecha_ini, fecha_fin=fecha_fin,dias_laborales=dias_laborales,dias_habiles=dias_habiles)
        sprint_nuevo.save()

        response = HorasTrabajadasPDF().get( None, None, codigo_ejecucion=proyectoNvo.pk, codigo_sprint=sprint_nuevo.pk )
        self.validar_response_pdf( response )