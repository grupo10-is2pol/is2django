from .modelos.rol.model import RolSistema
from .modelos.rol.model import RolProyecto

from .modelos.profile.model import Profile
#from .modelos.profile.serializer import ProfileSerializer

from .modelos.proyecto.model import Proyecto
#from .modelos.proyecto.serializer import ProyectoSerializer

from .modelos.rol.model import Permiso
from .modelos.rol.serializer import PermisoSerializer

from .modelos.sprint.model import Sprint
#from .modelos.sprint.serializer import SprintReadSerializer


from .modelos.miembros_sprint.model import  MiembrosSprint
from .modelos.miembros_sprint.serializer import  MiembrosSprintReadSerializer


from .modelos.userstory.model import UserStory
from .modelos.userstory.serializer import UserStoryReadSerializer


from .modelos.actividad.model import Actividad
from .modelos.actividad.serializer import ActividadReadSerializer

from .modelos.nota.model import Nota
from .modelos.nota.serializer import NotaReadSerializer


from .modelos.tablero.model import Tablero
from .modelos.tablero.serializer import TableroReadSerializer



from .modelos.historial_userstory.model import HistorialUserStory
from .modelos.historial_userstory.serializer import HistorialUserStoryReadSerializer


from .modelos.historial_estados_userstory.model import HistorialEstadosUserStory
from .modelos.historial_estados_userstory.serializer import HistorialEstadosUserStoryReadSerializer