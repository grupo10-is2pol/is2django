import os 

from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from mysite.util.template import obtenerTemplate

dir_path = os.path.dirname(os.path.realpath(__file__))

from django.contrib.auth import logout as auth_logout

@login_required(login_url='login')
def vista(request, codigo):
    """Metodo que es ejecutado para renderizar el contexto;
       en template con la funcion **obtnerTemplate**, devuelve 
       una cadena que representa los componentes de la ruta concatenados.
    """
    template = obtenerTemplate( os.path.join(dir_path,'vista.html') )
    return HttpResponse(template.render( request=request, context={"codigo":codigo} ))
