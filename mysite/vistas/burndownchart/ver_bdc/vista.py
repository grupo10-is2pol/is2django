import os 

import time
import uuid
from datetime import timedelta

from django import template
from django.conf import settings
from django.utils import timezone

import os #https://stackoverflow.com/questions/48163641/django-core-exceptions-appregistrynotready-apps-arent-loaded-yet-django-2-0/50654136#50654136
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()


from mysite.modelos.historial_userstory.model import HistorialUserStory
from mysite.modelos.actividad.model import Actividad
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.userstory.model import UserStory

from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from mysite.util.template import obtenerTemplate


dir_path = os.path.dirname(os.path.realpath(__file__))

from django.contrib.auth import logout as auth_logout

@login_required(login_url='login')
def vista(request, codigo_ejecucion,codigo_sprint):

    instance_sprint=Sprint.objects.get(pk=codigo_sprint)
    #print(instance_sprint)

    if not instance_sprint.fecha_inicio:  #si el sprint todavia no inicio, no tiene fecha de inicio
       coordenadas = []
       xMax = 0
       dias_laborales = 0
    else:
      dias_laborales = instance_sprint.dias_laborales
      coordenadas = []
      instance_userStories = HistorialUserStory.objects.filter(sprint=codigo_sprint)
      horas = 0

      for us in instance_userStories:
         print(us.us.nombre)
         horas += us.duracion_estimada_sprint

      coordenadas.append({
                           'x': 0,
                           'y': horas
                           })

      inicio = instance_sprint.fecha_inicio

      #dias_habiles = instance_sprint.dias_habiles
      fin = instance_sprint.fecha_fin
      dia = inicio
      nro_dia = 1
      while dia <= fin:
         
         instance_actividadesAux = Actividad.objects.filter(sprint=codigo_sprint).order_by('fecha')

         for actividad in instance_actividadesAux:
            if actividad.fecha == dia:
               finActual=actividad.fecha
               #print(finActual)
         
         dia=dia+timedelta(days=1)

      # if instance_sprint.fecha_fin: #si el sprint ya finalizo
      #    fin = instance_sprint.fecha_fin
      # else:                           #si el sprint sigue en curso, fin = hoy
      #    fin = timezone.now().date()

      dia = inicio

      while dia <= finActual:
         #if dia.isoweekday() in dias_habiles: #la fecha en que inicio el sprint es un dia habil? 
               
               
               actividades = []
               instance_actividades = Actividad.objects.filter(sprint=codigo_sprint).order_by('fecha')

               for actividad in instance_actividades:
                  if actividad.fecha == dia:
                     actividades.append(actividad)

               horas_trabajadas = 0
               for actividad in actividades:
                  horas_trabajadas += actividad.duracion

               coordenadas.append({
                  'x': nro_dia,
                  'y': horas - horas_trabajadas
               })

               horas = horas - horas_trabajadas

               nro_dia += 1
               #fin = fin + timedelta(days=1)
               dia = dia + timedelta(days=1)
         # else:
         #       #no es un dia habil entonces -> y trabajo igual, cargo actividades en el tablero de  su userstory, no debería ocurrir, pero si se trabajo igual, entonces -->
         #       actividades = []
         #       instance_actividades = Actividad.objects.filter(sprint=codigo_sprint).order_by('fecha')

         #       for actividad in instance_actividades:
         #          if actividad.fecha.date() == dia:
         #             actividades.append(actividad)

         #       horas_trabajadas = 0
         #       for actividad in actividades:
         #          horas_trabajadas += actividad.duracion

         #       horas = horas - horas_trabajadas #se trabajo igual, no estoy seguro de poner ESTO.

         #       fin = fin + timedelta(days=1)

         # dia = dia + timedelta(days=1) 

      
      if len(coordenadas) < instance_sprint.dias_laborales:
            xMax = instance_sprint.dias_laborales
      else:
            xMax = instance_sprint.dias_laborales
            #xMax = len(coordenadas) + 1



      

    template = obtenerTemplate( os.path.join(dir_path,'vista.html') )
    return HttpResponse( template.render( request=request, context={"codigo_ejecucion":codigo_ejecucion,"codigo_sprint":codigo_sprint,"coordenadas":coordenadas,"xMax":xMax,"dias_laborales":dias_laborales} ) )

   #  template = obtenerTemplate( os.path.join(dir_path,'vista.html') )
   #  return HttpResponse(template.render( request=request, context={"codigo_ejecucion":codigo_ejecucion,"codigo_sprint":codigo_sprint} ))


