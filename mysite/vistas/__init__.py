from .inicio import vista as inicio
from .administracion import vista as administracion
from .logout import vista as logout
from .login import vista as login

from .rol_sistema import *
from .rol_proyecto import *
from .usuario import *
from .proyecto import *
from .definicion_proyecto import *
from .ejecucion_proyecto import *
from .sprint import *
from .miembros_sprint import *
from .userstory import *
from .sprint_planing import *
from .sprint_backlog import *
from .burndownchart import *
from .iniciar_sprint import *
from .control_calidad import vista as control_calidad
from .actividades import *




#from .bdc import vista as bdc
from .product_backlog import vista as product_backlog
#from .tablero_kanban import vista as tablero_kanban

