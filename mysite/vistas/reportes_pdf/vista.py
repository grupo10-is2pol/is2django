from io import BytesIO
import locale


from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle, TA_CENTER, TA_LEFT
from reportlab.lib.units import inch, mm
from reportlab.lib import colors
from reportlab.graphics.shapes import Drawing, Line
from reportlab.lib.enums import TA_RIGHT
from reportlab.platypus import (
        Paragraph,
        Table,
        SimpleDocTemplate,
        Spacer,
        TableStyle,
        Paragraph,
        Image)
        

from django.views.generic import View
from django.http.response import HttpResponse
from mysite import settings


from mysite.modelos.proyecto.model import Proyecto
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.actividad.model import Actividad
from mysite.modelos.historial_userstory.model import HistorialUserStory



class SprintBacklogPDF(View):
    """
    Clase de la vista para creacion de Reporte Sprint Backlog
    """
    def get(self, request, *args, **kwargs):
        """
        respuesta a la consulta GET
        :param request: consulta GET
        :param args: argumentos
        :param kwargs: diccionario de datos
        :return: respuesta a consultas GET
        """
        self.proyecto = Proyecto.objects.get(pk=kwargs['codigo_ejecucion'])
        self.sprint = Sprint.objects.get(pk=kwargs['codigo_sprint'])
        #self.historial = HistorialUserStory.objects.get(pk=self.kwargs['codigo_sprint'])
        
        response = HttpResponse(content_type='application/pdf')
        #se crea el pdf
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        pagesize = (13 * inch, 20 * inch)
        self.doc = SimpleDocTemplate(buffer,
                                rightMargin=72,
                                leftMargin=72,
                                topMargin=72,
                                bottomMargin=72,
                                pagesize=pagesize)
        self.doc.title = 'Reporte de Sprint Backlog del Sprint: ' + str(self.sprint.nombre)
        self.story = []
        self.encabezado()
        self.titulo()
        self.descripcion()
        self.crearTabla()
        self.doc.build(self.story, onFirstPage=self.numeroPagina,
                       onLaterPages=self.numeroPagina)
        pdf = buffer.getvalue()
        #fin
        buffer.close()
        response.write(pdf)
        return response

    def encabezado(self):
        """
        agrega el encabezado al documento pdf a imprimir
        :return: None
        """
        logo = settings.MEDIA_ROOT+"logo2.png"
        im = Image(logo, inch, inch)
        im.hAlign = 'LEFT'
        p = Paragraph("<i>Software Gestor de Proyectos - IS2 <br/>Asunción-Paraguay<br/>Contacto: 0971-641-445</i>", self.estiloPR())
        data_tabla = [[im, p]]
        tabla = Table(data_tabla)
        self.story.append(tabla)

        d = Drawing(800, 3)
        d.add(Line(0, 0, 800, 0))
        self.story.append(d)
        self.story.append(Spacer(1, 0.3 * inch))

    def titulo(self):
        """
        agrega el titulo al documento pdf a imprimir
        :return: None
        """
        txt = "<b><u>Reporte de Sprint Backlog</u></b>"
        p = Paragraph('<font size=20>'+str(txt)+'</font>', self.estiloPC())
        self.story.append(p)
        self.story.append(Spacer(1, 0.5 * inch))

    def descripcion(self):
        """
        agrega la descripcion al documento pdf
        :return: None
        """
        txt = "<b>Proyecto: </b>" + str(self.proyecto) + "<br/><b>Sprint: </b>" + str(self.sprint.nombre)
        #txt = "<b>Proyecto: </b>" + str(self.proyecto)
        p = Paragraph('<font size=12>' + str(txt) + '</font>', self.estiloPL())
        self.story.append(p)
        self.story.append(Spacer(1, 0.3 * inch))
        
    def crearTabla(self):
        """
        agrega la tabla del reporte
        :return: None
        """
        userstories = HistorialUserStory.objects.filter(sprint = self.sprint).order_by('-priorizacion')
        nro = 1
        data = [["N°","Nombre", "Estado", "Priorización","Estado Tablero","Duración Estimada","Horas Trabajadas","Usuario Asignado"]]
        
        for us in userstories:
            if( us.miembro_sprint != None):
                usuario = us.miembro_sprint.team_member.usuario.username
            else:
                usuario = ''
            
            if( us.us != None):
                nombre = us.us.nombre
            else:
                nombre = ''

            aux = [nro,nombre,us.estado,us.priorizacion,us.estado_tablero,us.duracion_estimada_sprint,us.horas_trabajadas_sprint,usuario]
            nro += 1
            data.append(aux)
        style = TableStyle([
            ('GRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('FONTNAME', (0,0), (7,0), 'Helvetica-Bold'), #primera fila en negrita
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE')])

        t = Table(data)
        t.setStyle(style)
        self.story.append(t)
    

    def estiloPC(self):
        """
        estilo del cuerpo del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="centrado", alignment=TA_CENTER)

    def estiloPL(self):
        """
        estilo de la descripcion del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="izquierda", alignment=TA_LEFT)

    def estiloPR(self):
        """
        estilo el encabezado del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="derecha", alignment=TA_RIGHT)

    def numeroPagina(self, canvas, doc):
        """
        agrega el numero de pagina al documento pdf
        :param canvas: pdf
        :param doc: documento pdf
        :return: None
        """
        num = canvas.getPageNumber()
        text = "Página %s" % num
        canvas.drawRightString(190 * mm, 20 * mm, text)
    



class ProductBacklogPDF(View):
    """
    Clase de la vista para creacion de Reporte Product Backlog
    """
    def get(self, request, *args, **kwargs):
        """
        respuesta a la consulta GET
        :param request: consulta GET
        :param args: argumentos
        :param kwargs: diccionario de datos
        :return: respuesta a consultas GET
        """
        #self.sprint = Sprint.objects.get(pk=self.kwargs['sprint_pk'])
        print(kwargs['codigo_ejecucion'])
        #self.sprint = Sprint.objects.get(pk=self.kwargs[''])
        self.proyecto = Proyecto.objects.get(pk=kwargs['codigo_ejecucion'])
        #self.sprint = Sprint.objects.get(pk=self.proyecto.sprint)
        response = HttpResponse(content_type='application/pdf')
        #se crea el pdf
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        pagesize = (13 * inch, 20 * inch)
        self.doc = SimpleDocTemplate(buffer,
                                rightMargin=72,
                                leftMargin=72,
                                topMargin=72,
                                bottomMargin=72,
                                pagesize=pagesize)
        self.doc.title = 'Reporte de Producto Backlog del Proyecto: ' + str(self.proyecto.nombre)
        self.story = []
        self.encabezado()
        self.titulo()
        self.descripcion()
        self.crearTabla()
        self.doc.build(self.story, onFirstPage=self.numeroPagina,
                       onLaterPages=self.numeroPagina)
        pdf = buffer.getvalue()
        #fin
        buffer.close()
        response.write(pdf)
        return response

    def encabezado(self):
        """
        agrega el encabezado al documento pdf a imprimir
        :return: None
        """
        logo = settings.MEDIA_ROOT+"logo2.png"
        im = Image(logo, inch, inch)
        im.hAlign = 'LEFT'
        p = Paragraph("<i>Software Gestor de Proyectos - IS2 <br/>Asunción-Paraguay<br/>Contacto: 0971-641-445</i>", self.estiloPR())
        data_tabla = [[im, p]]
        tabla = Table(data_tabla)
        self.story.append(tabla)

        d = Drawing(800, 3)
        d.add(Line(0, 0, 800, 0))
        self.story.append(d)
        self.story.append(Spacer(1, 0.3 * inch))

    def titulo(self):
        """
        agrega el titulo al documento pdf a imprimir
        :return: None
        """
        txt = "<b><u>Reporte de Product Backlog</u></b>"
        p = Paragraph('<font size=20>'+str(txt)+'</font>', self.estiloPC())
        self.story.append(p)
        self.story.append(Spacer(1, 0.5 * inch))

    def descripcion(self):
        """
        agrega la descripcion al documento pdf
        :return: None
        """
        #txt = "<b>Proyecto: </b>" + str(self.proyecto) + "<br/><b>Sprint: </b>" + str(self.sprint.nombre)
        txt = "<b>Proyecto: </b>" + str(self.proyecto)
        p = Paragraph('<font size=12>' + str(txt) + '</font>', self.estiloPL())
        self.story.append(p)
        self.story.append(Spacer(1, 0.3 * inch))

    def crearTabla(self):
        """
        agrega la tabla del reporte
        :return: None
        """
        userstories = UserStory.objects.filter(proyecto = self.proyecto).order_by('-priorizacion')
        nro = 1
        data = [["N°","Nombre", "Estado", "Priorización","Estado Tablero","Duración Estimada","Horas Trabajadas","Usuario Asignado"]]
        for us in userstories:
            if( us.miembro_sprint != None):
                usuario = us.miembro_sprint.team_member.usuario.username
            else:
                usuario = ''
            
            if(us.duracion_estimada!=None):
                duracion_estimada = us.duracion_estimada
            else:
                duracion_estimada = None

            
            aux = [nro,us.nombre,us.estado,us.priorizacion,us.estado_tablero,duracion_estimada,us.horas_trabajadas_us,usuario]
            nro += 1
            data.append(aux)
        style = TableStyle([
            ('GRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('FONTNAME', (0,0), (7,0), 'Helvetica-Bold'), #primera fila en negrita
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE')])

        t = Table(data)
        t.setStyle(style)
        self.story.append(t)

    def estiloPC(self):
        """
        estilo del cuerpo del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="centrado", alignment=TA_CENTER)

    def estiloPL(self):
        """
        estilo de la descripcion del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="izquierda", alignment=TA_LEFT)

    def estiloPR(self):
        """
        estilo el encabezado del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="derecha", alignment=TA_RIGHT)

    def numeroPagina(self, canvas, doc):
        """
        agrega el numero de pagina al documento pdf
        :param canvas: pdf
        :param doc: documento pdf
        :return: None
        """
        num = canvas.getPageNumber()
        text = "Página %s" % num
        canvas.drawRightString(190 * mm, 20 * mm, text)


class HorasTrabajadasPDF(View):
    """
    Clase de la vista para creacion de Reporte Sprint Actual
    """
    def get(self, request, *args, **kwargs):
        """
        respuesta a la consulta GET
        :param request: consulta GET
        :param args: argumentos
        :param kwargs: diccionario de datos
        :return: respuesta a consultas GET
        """
        #self.sprint = Sprint.objects.get(pk=self.kwargs['sprint_pk'])
        print(kwargs['codigo_ejecucion'])
        #self.sprint = Sprint.objects.get(pk=self.kwargs[''])
        self.proyecto = Proyecto.objects.get(pk=kwargs['codigo_ejecucion'])
        self.sprint = Sprint.objects.get(pk=kwargs['codigo_sprint'])
        #self.sprint = Sprint.objects.get(pk=self.proyecto.sprint)
        response = HttpResponse(content_type='application/pdf')
        #se crea el pdf
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        pagesize = (13 * inch, 20 * inch)
        self.doc = SimpleDocTemplate(buffer,
                                rightMargin=72,
                                leftMargin=72,
                                topMargin=72,
                                bottomMargin=72,
                                pagesize=pagesize)
        self.doc.title = 'Reporte del Sprint Actual del proyecto: ' + str(self.proyecto.nombre)
        self.story = []
        self.encabezado()
        self.titulo()
        self.descripcion()
        self.crearTabla()
        self.doc.build(self.story, onFirstPage=self.numeroPagina,
                       onLaterPages=self.numeroPagina)
        pdf = buffer.getvalue()
        #fin
        buffer.close()
        response.write(pdf)
        return response

    def encabezado(self):
        """
        agrega el encabezado al documento pdf a imprimir
        :return: None
        """
        logo = settings.MEDIA_ROOT+"logo2.png"
        im = Image(logo, inch, inch)
        im.hAlign = 'LEFT'
        p = Paragraph("<i>Software Gestor de Proyectos - IS2 <br/>Asunción-Paraguay<br/>Contacto: 0971-641-445</i>", self.estiloPR())
        data_tabla = [[im, p]]
        tabla = Table(data_tabla)
        self.story.append(tabla)

        d = Drawing(800, 3)
        d.add(Line(0, 0, 800, 0))
        self.story.append(d)
        self.story.append(Spacer(1, 0.3 * inch))

    def titulo(self):
        """
        agrega el titulo al documento pdf a imprimir
        :return: None
        """
        txt = "<b><u>Reporte del Sprint Actual</u></b>"
        p = Paragraph('<font size=20>'+str(txt)+'</font>', self.estiloPC())
        self.story.append(p)
        self.story.append(Spacer(1, 0.5 * inch))

    def descripcion(self):
        """
        agrega la descripcion al documento pdf
        :return: None
        """
        txt = "<b>Proyecto: </b>" + str(self.proyecto) + "<br/><b>Sprint: </b>" + str(self.sprint.nombre)
        #txt = "<b>Proyecto: </b>" + str(self.proyecto)
        p = Paragraph('<font size=12>' + str(txt) + '</font>', self.estiloPL())
        self.story.append(p)
        self.story.append(Spacer(1, 0.3 * inch))

    def crearTabla(self):
        """
        agrega la tabla del reporte
        :return: None
        """
        userstories = UserStory.objects.filter(sprint = self.sprint).order_by('-priorizacion')
        nro = 1
        data = [["N°","Nombre", "Estado", "Priorización","Estado Tablero","Duración Estimada","Horas Trabajadas","Usuario Asignado"]]
        for us in userstories:
            if( us.miembro_sprint != None):
                usuario = us.miembro_sprint.team_member.usuario.username
            else:
                usuario = ''
            aux = [nro,us.nombre,us.estado,us.priorizacion,us.estado_tablero,us.duracion_estimada,us.horas_trabajadas_us,usuario]
            nro += 1
            data.append(aux)
        style = TableStyle([
            ('GRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('FONTNAME', (0,0), (7,0), 'Helvetica-Bold'), #primera fila en negrita
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE')])

        t = Table(data)
        t.setStyle(style)
        self.story.append(t)


    def estiloPC(self):
        """
        estilo del cuerpo del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="centrado", alignment=TA_CENTER)

    def estiloPL(self):
        """
        estilo de la descripcion del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="izquierda", alignment=TA_LEFT)

    def estiloPR(self):
        """
        estilo el encabezado del reporte
        :return: objeto para estilo del reporte
        """
        return ParagraphStyle(name="derecha", alignment=TA_RIGHT)

    def numeroPagina(self, canvas, doc):
        """
        agrega el numero de pagina al documento pdf
        :param canvas: pdf
        :param doc: documento pdf
        :return: None
        """
        num = canvas.getPageNumber()
        text = "Página %s" % num
        canvas.drawRightString(190 * mm, 20 * mm, text)



    