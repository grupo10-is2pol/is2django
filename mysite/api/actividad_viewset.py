"""**ActividadViewSet** 
"""
"""**La clase View** Esta clase es el núcleo de todo el montaje, 
    y si ninguno de las funcionalidades que nos proporciona Django nos sirve, casi seguro que empezaremos a crear nuestra propia classe extendiendo de ésta.

"""
"""El marco Django REST le permite combinar la lógica para un conjunto de vistas relacionadas en una sola clase, llamada ViewSet. En otros marcos, también puede encontrar implementaciones conceptualmente similares denominadas algo como 'Recursos' o 'Controladores'.Los enrutadores predeterminados incluidos con el marco REST proporcionarán rutas para un conjunto estándar de acciones de estilo de creación / La ViewSetclase hereda de APIView. Puede utilizar cualquiera de los atributos estándar, como permission_classes, authentication_classespara controlar la política de la API en el conjunto de vistas. recuperación / actualización / destrucción
"""

from rest_framework import routers, serializers, viewsets
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from rest_framework import mixins


from mysite.models import Actividad
from mysite.models import ActividadReadSerializer

from mysite.modelos.actividad.model import Actividad
from mysite.modelos.actividad.serializer import ActividadReadSerializer, ActividadUpdateSerializer
import django_filters
from rest_framework import status
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.historial_userstory.model import HistorialUserStory
from datetime import datetime

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.contrib.auth.models import User
from mysite.modelos.sprint.model import Sprint

from mysite.vistas import sprint
"""**Mixins:** Los mixins se utilizan para reutilizar código en componentes de Vue que realizan la misma acción. Los mixins son como funciones en la programación de C. Podemos definir algunas acciones en mixins y usarlo donde sea necesario. La reutilización del código es simple con la ayuda de mixins."""
class ActividadViewSet(  
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,                    
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet                
):
    queryset = Actividad.objects.all()
    
    read_serializer_class = ActividadReadSerializer
    update_serializer_class = ActividadUpdateSerializer

    serializer_class = ActividadReadSerializer   
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['id','nombre','descripcion','duracion','miembro','us','fecha','sprint','estado_tablero'] 

    def update(self, request, *args, **kwargs ):
        """**Metodo Update**"""
        """Instancia del modelo  en UpdateAPIViw django REST"""
        return super().update(request, args ,kwargs)

    def create(self, request, *args, **kwargs): #al crear actividad, no esta permitido cargar actividades en estado del us en 'To Do' o en 'Done'

            us_id = request.data['us']
            sprint_id = request.data['sprint']
            duracion_actividad = request.data['duracion']

            instance_us = UserStory.objects.get(pk=us_id)
            estado = request.data['estado_tablero']
            print(estado)

            #us_id = us_id = request.data['id']
            #instance_us = UserStory.objects.get(pk=us_id)

            
            # us_nombre = instance_us.nombre
            # sprint_nombre = (instance_us.sprint).nombre
            # usuario_nombre = (((instance_us.miembro_sprint).team_member).usuario).username
            # proyecto_nombre = (instance_us.proyecto).nombre
            # motivo_comentario = request.data['motivo_comentario']
            # usuario_correo = (((instance_us.miembro_sprint).team_member).usuario).email
            
            # titulo = "Rechazo UserStory"
            # asunto = "asdf"
            # remitente = settings.EMAIL_HOST_USER
            # destinatarios = [usuario_correo]
            
            # message = EmailMultiAlternatives(titulo,asunto,remitente, destinatarios)
            # template = get_template('correo.html')
            # cuerpoCorreo = template.render( {'us_nombre': us_nombre,'sprint_nombre': sprint_nombre, 'usuario_nombre': usuario_nombre, 'proyecto_nombre':proyecto_nombre, 'motivo_comentario': motivo_comentario,} ) 
            # message.attach_alternative(cuerpoCorreo, "text/html")
            # message.send()



            
            fecha_inicio = instance_us.sprint.fecha_inicio
            fecha_fin = instance_us.sprint.fecha_fin
            fecha_actividad = datetime.strptime(request.data['fecha'], "%Y-%m-%d").date()
            print(request.data['fecha'])
            print(fecha_actividad)

            # print(fecha_inicio)
            # print(fecha_fin)
            # print(fecha_actividad)
            # print(type(fecha_inicio))
            # print(type(fecha_fin))
            # print(type(fecha_actividad))
            
            if(estado == 'Doing'):

                if(fecha_actividad >= fecha_inicio and fecha_actividad <= fecha_fin):


                    instance_HistorialUserstory = HistorialUserStory.objects.get(us = us_id, sprint = sprint_id)

                    print("recuperamos el historial", instance_HistorialUserstory)

                    instance_HistorialUserstory.horas_trabajadas_sprint = instance_HistorialUserstory.horas_trabajadas_sprint + duracion_actividad

                    
                    
                    if(instance_us.duracion_restante == None): #cuando creamos us duracion restante es null
                        instance_us.duracion_restante =  instance_us.duracion_estimada - duracion_actividad #si es la primera vez que cargamos actividad
                        

                    else: #si ya cargamos anteriormente actividades
                        instance_us.duracion_restante =  instance_us.duracion_restante - duracion_actividad
                        if(instance_us.duracion_restante < 0): #si trabajo de mas en el userstorie la duracion seria negativa, se resta esto con la duracion estimada original
                            instance_us.horas_extras_trabajadas_us = -1*instance_us.duracion_restante 
            
                                        

                    instance_us.horas_trabajadas_us = instance_us.horas_trabajadas_us + duracion_actividad # total de horas trabajadas entre todos los sprints en donde se asigno este us

                    

                    instance_us.porcentaje_realizado = (100 * instance_HistorialUserstory.horas_trabajadas_sprint) / (instance_HistorialUserstory.duracion_estimada_sprint)
                    instance_HistorialUserstory.save()
                    instance_us.save()
                    
                    return super().create(request, args ,kwargs)
                    
            else: 
                if (estado == 'Control de Calidad'):
                    return super().create(request, args ,kwargs)
                else: # estado To Do y Done (No se puede cargar actividad)
                    return Response( { 'Mensaje': 'no esta permitido cargar actividades en estado del us en To Do ni en Done'}, status=status.HTTP_403_FORBIDDEN)
                    
                

                
            
        
    def get_serializer_class(self):
        """**Metodo get_serializer_class**"""
        """Devuelve la clase que se usará para el serializador.Por defecto, usa `self.serializer_class`.Es posible que desee anular esto si necesita proporcionar diferentes serializaciones dependiendo de la solicitud entrante.
        """        
        if self.action in ["create", "update", "partial_update", "destroy"]:
            return self.update_serializer_class
        return self.read_serializer_class



