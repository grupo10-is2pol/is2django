"""**SprintViewSet** 
"""
"""**La clase View** Esta clase es el núcleo de todo el montaje, 
    y si ninguno de las funcionalidades que nos proporciona Django nos sirve, casi seguro que empezaremos a crear nuestra propia classe extendiendo de ésta.

"""
"""El marco Django REST le permite combinar la lógica para un conjunto de vistas relacionadas en una sola clase, llamada ViewSet. En otros marcos, también puede encontrar implementaciones conceptualmente similares denominadas algo como 'Recursos' o 'Controladores'.Los enrutadores predeterminados incluidos con el marco REST proporcionarán rutas para un conjunto estándar de acciones de estilo de creación / La ViewSetclase hereda de APIView. Puede utilizar cualquiera de los atributos estándar, como permission_classes, authentication_classespara controlar la política de la API en el conjunto de vistas. recuperación / actualización / destrucción
"""
from rest_framework import routers, serializers, viewsets
from rest_framework.response import Response
from rest_framework import mixins
from datetime import datetime,timedelta


from mysite.modelos.sprint.model import Sprint
from mysite.modelos.sprint.serializer import SprintReadSerializer, SprintUpdateSerializer
import django_filters
"""**Mixins:** Los mixins se utilizan para reutilizar código en componentes de Vue que realizan la misma acción. Los mixins son como funciones en la programación de C. Podemos definir algunas acciones en mixins y usarlo donde sea necesario. La reutilización del código es simple con la ayuda de mixins."""
class SprintViewSet(  
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,                    
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet                
):

    queryset = Sprint.objects.all()
    
    read_serializer_class = SprintReadSerializer
    update_serializer_class = SprintUpdateSerializer

    serializer_class = SprintReadSerializer   
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields =['id','nombre','dias_laborales','proyecto','estado','fecha_inicio','fecha_fin'] 

    def update(self, request, *args, **kwargs):
        """**Metodo Update**"""
        """Instancia del modelo  en UpdateAPIViw django REST"""
        instance = self.get_object()
        # print(instance.estado)
        # print(request.data.get("estado"))
        if(request.data.get("estado") == 'En Proceso'):

            fechaInicio = datetime.today().strftime('%Y-%m-%d')
            fechaFin = ( datetime.today() + timedelta(days=request.data['dias_laborales']) ).strftime('%Y-%m-%d')
            instance.fecha_inicio = fechaInicio
            instance.fecha_fin = fechaFin
            instance.save()    
            request.data['fecha_inicio'] = fechaInicio
            request.data['fecha_fin'] = fechaFin
            
            serializer = self.get_serializer(instance,data=request.data) 
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.data)
        #else:
            
            # if(request.data.get("estado") == 'Terminado'):

            #     fechaFin = datetime.today().strftime('%Y-%m-%d')
            #     instance.fecha_fin= fechaFin
            #     instance.save()    
            #     request.data['fecha_fin'] = fechaFin
                
            #     serializer = self.get_serializer(instance,data=request.data) 
            #     serializer.is_valid(raise_exception=True)
            #     self.perform_update(serializer)
            #     return Response(serializer.data)
        else:
            return super().update(request, args ,kwargs)

    def get_serializer_class(self):
        """**Metodo get_serializer_class**"""
        """Devuelve la clase que se usará para el serializador.Por defecto, usa `self.serializer_class`.Es posible que desee anular esto si necesita proporcionar diferentes serializaciones dependiendo de la solicitud entrante.
        """        
        if self.action in ["create", "update", "partial_update", "destroy"]:
            return self.update_serializer_class
        return self.read_serializer_class



