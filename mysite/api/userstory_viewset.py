"""**UserStoryViewSet** 
"""
"""**La clase View** Esta clase es el núcleo de todo el montaje, 
    y si ninguno de las funcionalidades que nos proporciona Django nos sirve, casi seguro que empezaremos a crear nuestra propia classe extendiendo de ésta.

"""
"""El marco Django REST le permite combinar la lógica para un conjunto de vistas relacionadas en una sola clase, llamada ViewSet. En otros marcos, también puede encontrar implementaciones conceptualmente similares denominadas algo como 'Recursos' o 'Controladores'.Los enrutadores predeterminados incluidos con el marco REST proporcionarán rutas para un conjunto estándar de acciones de estilo de creación / La ViewSetclase hereda de APIView. Puede utilizar cualquiera de los atributos estándar, como permission_classes, authentication_classespara controlar la política de la API en el conjunto de vistas. recuperación / actualización / destrucción
"""
from datetime import datetime

import django_filters
from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse, JsonResponse
from django.template.loader import get_template
from pygments.lexers.sql import RqlLexer
from rest_framework import filters, mixins, routers, serializers, viewsets
from rest_framework.response import Response

from mysite.modelos.miembros_sprint.model import MiembrosSprint
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.userstory.serializer import (UserStoryReadSerializer,
                                                 UserStoryUpdateSerializer)
from mysite.models import UserStory, UserStoryReadSerializer

from mysite.modelos.proyecto.model import TeamMember
from mysite.modelos.rol.model import RolProyecto


"""**Mixins:** Los mixins se utilizan para reutilizar código en componentes de Vue que realizan la misma acción. Los mixins son como funciones en la programación de C. Podemos definir algunas acciones en mixins y usarlo donde sea necesario. La reutilización del código es simple con la ayuda de mixins."""
class UserStoryViewSet(  
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,                    
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet                
):

    queryset = UserStory.objects.all()
    
    read_serializer_class = UserStoryReadSerializer
    update_serializer_class = UserStoryUpdateSerializer

    serializer_class = UserStoryReadSerializer   
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend,filters.OrderingFilter]
    filterset_fields =['id','nombre','proyecto','valor_negocio','prioridad',
                      'valor_tecnico','descripcion','estado','estado_tablero',
                      'duracion_estimada','sprint','miembro',
                      'duracion_restante','miembro_sprint','horas_trabajadas_us','porcentaje_realizado','horas_extras_trabajadas_us','motivo_comentario','estado_qa']

    ordering_fields = ['priorizacion'] #/api/userstory?ordering="priorizacion" #con esto solo se puede ordenar por priorizacion
    #ordering = ['priorizacion'] #POR DEFECTO SE ORDENA POR priorizacion (ASCENDENTE)
    ordering = ['-priorizacion'] #POR DEFECTO SE ORDENA POR priorizacion (DESCENDENTE)   #https://www.django-rest-framework.org/api-guide/filtering/#orderingfilter

    def create(self, request, *args, **kwargs): #al crear US se calcula su priorizacoin
            valor_tecnico = request.data['valor_tecnico']
            valor_negocio = request.data['valor_negocio']
            prioridad = request.data['prioridad']
            priorizacion = round(((2*valor_negocio + prioridad + 2*valor_tecnico) / 4),2)
            request.data['priorizacion'] = priorizacion
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        
        
      

    def update(self, request, *args, **kwargs): #al actualizar Us se calcula su priorizacion 
        """**Metodo Update**"""
        """Instancia del modelo  en UpdateAPIViw django REST""" 
        instance = self.get_object()
    

        if( 'estado_qa' in request.data ):
            estado_qa = request.data['estado_qa']
        else:
            estado_qa = 'Ninguno'
        
        if  (estado_qa == 'Rechazado'):
            us_nombre = instance.nombre
            sprint_nombre = (instance.sprint).nombre
            usuario_nombre = (((instance.miembro_sprint).team_member).usuario).username
            proyecto_nombre = (instance.proyecto).nombre
            motivo_comentario = request.data['motivo_comentario']
            usuario_correo = (((instance.miembro_sprint).team_member).usuario).email
            
            asunto = "Notificación Gestor Proyectos IS2 - UserStory Rechazado"
            remitente = settings.EMAIL_HOST_USER
            destinatarios = [usuario_correo]
            
            message = EmailMultiAlternatives(asunto,'',remitente, destinatarios)
            template = get_template('qa_rechazado.html')
            cuerpoCorreo = template.render( {'us_nombre': us_nombre,'sprint_nombre': sprint_nombre, 'usuario_nombre': usuario_nombre, 'proyecto_nombre':proyecto_nombre, 'motivo_comentario': motivo_comentario,} ) 
            message.attach_alternative(cuerpoCorreo, "text/html")
            message.send()
        
        if (estado_qa == 'Aprobado'):
            us_nombre = instance.nombre
            sprint_nombre = (instance.sprint).nombre
            usuario_nombre = (((instance.miembro_sprint).team_member).usuario).username
            proyecto_nombre = (instance.proyecto).nombre
            motivo_comentario = request.data['motivo_comentario']
            usuario_correo = (((instance.miembro_sprint).team_member).usuario).email
            
            asunto = "Notificación Gestor Proyectos IS2 - UserStory Aprobado"
            remitente = settings.EMAIL_HOST_USER
            destinatarios = [usuario_correo]
            
            message = EmailMultiAlternatives(asunto,'',remitente, destinatarios)
            template = get_template('qa_aprobado.html')
            cuerpoCorreo = template.render( {'us_nombre': us_nombre,'sprint_nombre': sprint_nombre, 'usuario_nombre': usuario_nombre, 'proyecto_nombre':proyecto_nombre, 'motivo_comentario': motivo_comentario,} ) 
            message.attach_alternative(cuerpoCorreo, "text/html")
            message.send()

        if( 'estado_tablero' in request.data ):
            if(request.data['estado_tablero'] == 'Done'): #cuando pasamos un us a Done comprobamos si todos los userstories estan en donde para enviar la notificacion al scrum

                userstories = UserStory.objects.filter(sprint = instance.sprint.id) #todos los userstories del sprint, con cualquier estado
                cantidadUserstories = len(userstories) -1 #se resta 1 ya que el userstorie que estamos actualizando todavia no se guardo como instancia en la bd
                #print(cantidadUserstories)

                userstoriesDone = UserStory.objects.filter(sprint = instance.sprint.id, estado_tablero='Done') #US EN DONE
                cantidadUserstorieDone = len(userstoriesDone)
                #print(cantidadUserstorieDone)

                if(cantidadUserstorieDone == cantidadUserstories): # si todos los userstories finalizaron

                    instance_rolProyecto = RolProyecto.objects.filter(proyecto = instance.proyecto.id, nombre ='ScrumMaster')
                    RolProyectoIdScrumMaster = instance_rolProyecto[0].id
                    instance_miembro = TeamMember.objects.filter(proyecto = instance.proyecto.id, rol_proyecto = RolProyectoIdScrumMaster) #recuperamos al scrumMaster

                    
                    us_nombre = instance.nombre
                    sprint_nombre = (instance.sprint).nombre
                    usuario_scrum_nombre = instance_miembro[0].usuario.username
                    proyecto_nombre = (instance.proyecto).nombre
                    usuario_scrum_correo =instance_miembro[0].usuario.email
                
                    asunto = "Notificación Gestor Proyectos IS2 - Todos los UserStories estan en Done"
                    remitente = settings.EMAIL_HOST_USER
                    destinatarios = [usuario_scrum_correo]
                
                    message = EmailMultiAlternatives(asunto,'',remitente, destinatarios)
                    template = get_template('todosLosUserstoriesEnDone.html')
                    cuerpoCorreo = template.render( {'us_nombre': us_nombre,'sprint_nombre': sprint_nombre, 'usuario_scrum_nombre': usuario_scrum_nombre, 'proyecto_nombre':proyecto_nombre,} ) 
                    message.attach_alternative(cuerpoCorreo, "text/html")
                    message.send()





        

        valor_tecnico = request.data['valor_tecnico']
        valor_negocio = request.data['valor_negocio']
        prioridad = request.data['prioridad']
        Priorizacion = round(((2*valor_negocio + prioridad + 2*valor_tecnico) / 4),2)
        instance.priorizacion = Priorizacion
        instance.save()
        request.data['priorizacion'] = Priorizacion
        serializer = self.get_serializer(instance,data=request.data) 
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data)
        

    def get_serializer_class(self):
        """**Metodo get_serializer_class**"""
        """Devuelve la clase que se usará para el serializador.Por defecto, usa `self.serializer_class`.Es posible que desee anular esto si necesita proporcionar diferentes serializaciones dependiendo de la solicitud entrante.
        """        
        if self.action in ["create", "update", "partial_update", "destroy"]:
            return self.update_serializer_class
        return self.read_serializer_class


















