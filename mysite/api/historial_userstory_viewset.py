"""**HistorialUserStoryViewSet** 
"""
"""**La clase View** Esta clase es el núcleo de todo el montaje, 
    y si ninguno de las funcionalidades que nos proporciona Django nos sirve, casi seguro que empezaremos a crear nuestra propia classe extendiendo de ésta.

"""
"""El marco Django REST le permite combinar la lógica para un conjunto de vistas relacionadas en una sola clase, llamada ViewSet. En otros marcos, también puede encontrar implementaciones conceptualmente similares denominadas algo como 'Recursos' o 'Controladores'.Los enrutadores predeterminados incluidos con el marco REST proporcionarán rutas para un conjunto estándar de acciones de estilo de creación / La ViewSetclase hereda de APIView. Puede utilizar cualquiera de los atributos estándar, como permission_classes, authentication_classespara controlar la política de la API en el conjunto de vistas. recuperación / actualización / destrucción
"""
from rest_framework import routers, serializers, viewsets
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from rest_framework import mixins


from mysite.models import HistorialUserStory
from mysite.models import HistorialUserStoryReadSerializer

from mysite.modelos.historial_userstory.model import HistorialUserStory
from mysite.modelos.historial_userstory.serializer import HistorialUserStoryReadSerializer, HistorialUserStoryUpdateSerializer
import django_filters
from rest_framework import status
from mysite.modelos.userstory.model import UserStory
from datetime import datetime
"""**Mixins:** Los mixins se utilizan para reutilizar código en componentes de Vue que realizan la misma acción. Los mixins son como funciones en la programación de C. Podemos definir algunas acciones en mixins y usarlo donde sea necesario. La reutilización del código es simple con la ayuda de mixins."""
class HistorialUserStoryViewSet(  
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,                    
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet                
):
    queryset = HistorialUserStory.objects.all()
    
    read_serializer_class = HistorialUserStoryReadSerializer
    update_serializer_class = HistorialUserStoryUpdateSerializer

    serializer_class = HistorialUserStoryReadSerializer   
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields =['id','us','sprint','miembro_sprint','duracion_estimada_sprint','fecha_estimada','estado_tablero','horas_trabajadas_sprint','priorizacion','motivo_comentario','estado']
    ordering_fields = ['priorizacion']
    ordering = ['-priorizacion'] #POR DEFECTO SE ORDENA POR priorizacion (DESCENDENTE)   #https://www.django-rest-framework.org/api-guide/filtering/#orderingfilter

    def update(self, request, *args, **kwargs ):
        """**Metodo Update**"""
        """Instancia del modelo  en UpdateAPIViw django REST"""
        return super().update(request, args ,kwargs)

    def create(self, request, *args, **kwargs): #al crear un historial de userstorie tambien se guarda la fecha que se estimo por primera vez dentro de un sprint

        fechaEstimada = datetime.today().strftime('%Y-%m-%d') 
        request.data['fecha_estimada'] = fechaEstimada

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

                
            
        
    def get_serializer_class(self):
        """**Metodo get_serializer_class**"""
        """Devuelve la clase que se usará para el serializador.Por defecto, usa `self.serializer_class`.Es posible que desee anular esto si necesita proporcionar diferentes serializaciones dependiendo de la solicitud entrante.
        """        
        if self.action in ["create", "update", "partial_update", "destroy"]:
            return self.update_serializer_class
        return self.read_serializer_class



