"""**HistorialEstadosUserStoryViewSet** 
"""
"""**La clase View** Esta clase es el núcleo de todo el montaje, 
    y si ninguno de las funcionalidades que nos proporciona Django nos sirve, casi seguro que empezaremos a crear nuestra propia classe extendiendo de ésta.

"""
"""El marco Django REST le permite combinar la lógica para un conjunto de vistas relacionadas en una sola clase, llamada ViewSet. En otros marcos, también puede encontrar implementaciones conceptualmente similares denominadas algo como 'Recursos' o 'Controladores'.Los enrutadores predeterminados incluidos con el marco REST proporcionarán rutas para un conjunto estándar de acciones de estilo de creación / La ViewSetclase hereda de APIView. Puede utilizar cualquiera de los atributos estándar, como permission_classes, authentication_classespara controlar la política de la API en el conjunto de vistas. recuperación / actualización / destrucción
"""
from rest_framework import routers, serializers, viewsets
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from rest_framework import mixins


from mysite.models import HistorialEstadosUserStory
from mysite.models import HistorialEstadosUserStoryReadSerializer

from mysite.modelos.historial_estados_userstory.model import HistorialEstadosUserStory
from mysite.modelos.historial_estados_userstory.serializer import HistorialEstadosUserStoryReadSerializer, HistorialEstadosUserStoryUpdateSerializer
import django_filters
from rest_framework import status
from mysite.modelos.userstory.model import UserStory
from datetime import datetime
"""**Mixins:** Los mixins se utilizan para reutilizar código en componentes de Vue que realizan la misma acción. Los mixins son como funciones en la programación de C. Podemos definir algunas acciones en mixins y usarlo donde sea necesario. La reutilización del código es simple con la ayuda de mixins."""
class HistorialEstadosUserStoryViewSet(  
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,                    
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet                
):
    queryset = HistorialEstadosUserStory.objects.all()
    
    read_serializer_class = HistorialEstadosUserStoryReadSerializer
    update_serializer_class = HistorialEstadosUserStoryUpdateSerializer

    serializer_class = HistorialEstadosUserStoryReadSerializer   
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['id','us','sprint','miembro_sprint','estado_tablero','fecha_cambio_estado','hora_cambio_estado']

    def update(self, request, *args, **kwargs ):
        """**Metodo Update**"""
        """Instancia del modelo  en UpdateAPIViw django REST"""
        fechaCambioEstado = datetime.today().strftime('%Y-%m-%d')
        horaCambioEstado = datetime.today().strftime('%H:%M:%S') 
        instance = self.get_object()
        request.data['fecha_cambio_estado'] = fechaCambioEstado
        request.data['hora_cambio_estado'] = horaCambioEstado
        instance.fecha_cambio_estado = fechaCambioEstado
        instance.hora_cambio_estado = horaCambioEstado
        instance.save()


        serializer = self.get_serializer(instance,data=request.data) 
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs): #al crear un historial de userstorie tambien se guarda la fecha que se estimo por primera vez dentro de un sprint

        fechaCambioEstado = datetime.today().strftime('%Y-%m-%d') 
        horaCambioEstado = datetime.today().strftime('%H:%M:%S')
        request.data['fecha_cambio_estado'] = fechaCambioEstado
        request.data['hora_cambio_estado'] = horaCambioEstado
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

                
            
        
    def get_serializer_class(self):
        """**Metodo get_serializer_class**"""
        """Devuelve la clase que se usará para el serializador.Por defecto, usa `self.serializer_class`.Es posible que desee anular esto si necesita proporcionar diferentes serializaciones dependiendo de la solicitud entrante.
        """        
        if self.action in ["create", "update", "partial_update", "destroy"]:
            return self.update_serializer_class
        return self.read_serializer_class



