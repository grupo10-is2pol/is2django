"""El marco Django REST le permite combinar la lógica para un conjunto de vistas relacionadas en una sola clase, llamada ViewSet. En otros marcos, también puede encontrar implementaciones conceptualmente similares denominadas algo como 'Recursos' o 'Controladores'.Los enrutadores predeterminados incluidos con el marco REST proporcionarán rutas para un conjunto estándar de acciones de estilo de creación / La ViewSetclase hereda de APIView. Puede utilizar cualquiera de los atributos estándar, como permission_classes, authentication_classespara controlar la política de la API en el conjunto de vistas. recuperación / actualización / destrucción
"""
from django.http import HttpResponse, JsonResponse
from rest_framework import routers, serializers, viewsets
from rest_framework.response import Response
from mysite.models import Proyecto


from mysite.modelos.authuser.serializer import AuthUserSerializer

from django.contrib.auth.models import User


class AuthUserViewSet(viewsets.ModelViewSet):
    """Una ViewSetclase es simplemente un tipo
       de vista basada en clases, que no proporciona ningún controlador 
       de método como .get()o .post(), sino que 
       proporciona acciones como .list()y .create().
    """
    queryset = User.objects.all()
    serializer_class = AuthUserSerializer   
