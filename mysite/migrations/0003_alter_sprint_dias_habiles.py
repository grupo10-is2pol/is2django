# Generated by Django 3.2.6 on 2021-10-02 07:21

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0002_alter_teammember_rol_proyecto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sprint',
            name='dias_habiles',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), size=None),
        ),
    ]
