# Generated by Django 3.2.6 on 2021-10-16 05:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0017_alter_sprint_dias_habiles'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actividad',
            name='fecha',
            field=models.DateTimeField(),
        ),
    ]
