
from django.db import models
from django.contrib import admin
from mysite.modelos.miembros_sprint.model import MiembrosSprint
#from mysite.modelos.userstory.model import UserStory
from mysite.modelos.sprint.model import Sprint

ESTADOS_US = (                  #SE CAMBIA EL ESTADO DEL USERSTORY CUANDO:
    ('Pendiente', 'Pendiente'),#Cuando se Crea un US, se crea con estado PENDIENTE POR DEFAULT
    ('Asignado', 'Asignado'), #Cuando se Asigna un Us aun TeamMember y un Sprint en Especifico
    ('No Finalizado', 'No Finalizado'), #Cuando el Srint se termina y el UserStory se quedo en algun Estado de tablero (todo, doing,done,control de calidad)
    ('Finalizado','Finalizado') #Cuando Se finaliza el US, (Pasa el Control de Calidad) (El ScrumMaster da visto bueno)
)
class HistorialUserStory(models.Model): #el historial se usa en el sprint planing (POST)
    """**Clase HistorialUserStory**"""
    """Clase para agregar un User Story a una tabla donde se pueden ver el historial de ese User Story, contiene datos como el userstorie, el sprint, el miembro asignado, la duracion estimada, el estado en tablero
    """

    #se crea (Post HistorialUserStory) cuando se asigan userstories en el Sprint Planing
    us = models.ForeignKey('UserStory',on_delete=models.CASCADE, blank=False, null=False)
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, blank=False, null=False)
    miembro_sprint = models.ForeignKey(MiembrosSprint,on_delete=models.PROTECT, blank=False, null=False)
    duracion_estimada_sprint = models.PositiveIntegerField(blank=False, null=False)
    fecha_estimada = models.DateField(blank=True,null=True)
    priorizacion = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    motivo_comentario = models.CharField(max_length=1000, blank=True, null=True)

    #tambien guardar el estado_tablero en el que quedo el userstorie
    #esto se usa en el tablero y se actualiza el Historial cada vez que movemos un us a otro estado (PUT  de este campo)
    #Actualizamos estos dos campos :
    estado_tablero = models.CharField(max_length=30, blank=True, null=True)
    #fecha_cambioEstado = models.DateField(blank=True,null=True)
    estado = models.CharField(max_length=30, choices=ESTADOS_US, default='Asignado')
    #cuantas horas se trabajo en el userstory en un sprint en especifico
    #esto se actualiza (PUT de este campo) cada vez que cargamos una actividad 
    # a un userstorie (Post de actividad)

    #tambien podemos hacer en el backend, para que sea mas facil , def create() .... de la apiview Actividad
    horas_trabajadas_sprint =  models.PositiveIntegerField(blank=True, null=True)


    #El us tiene, la estimación original, la estimación del sprint, horas trabajadas dentro del sprint

    #! Estimacion Original : cuando se estima por primera vez el userstorie en el sprint planing
    # duracion_estimada_original = models.PositiveIntegerField(blank=True, null=True)

    #! Estimacion Sprint :cuando ya estima en otro en sprint, la estimacion original y de sprint la primera vez que estimamos son las mismas

    #duracion_estimada_sprint = models.PositiveIntegerField(blank=True, null=True)

    #! horas trabajadas totales :

    #horas_trabajadas_total =

    #! horas trabajdas en un sprint

    #horas_trabajadas_sprint =

    




    def __str__(self):
        """Retorna el nombre del objeto actual return nombre del objeto actual
        """
        return self.us.nombre

    class Meta:
        """Con este tipo de restricción, se especifica que los valores ingresados en la columna deben cumplir la regla o formula especificada
        """
        # constraints = [
        #     models.CheckConstraint(
        #         name="DuracionDebeEstarEntre:1-1000",
        #         check=models.Q(duracion_estimada__range=(1, 1000)),
        #     ),
        # ]

        db_table="HistorialUserStory"

admin.site.register(HistorialUserStory)




# historialUserStories{
#     id: 1
#     sprint : 1 // en que sprint se estimo
#     us : 2     //el userstorie en especifico
#     duracion_estimada : 20 hs //la duracion en ese esprint
#     estado_tablero : 'To Do'  //el estado de tablero que quedo el userstorie
# }