
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.sprint.model import Sprint


from rest_framework import routers, serializers, viewsets
from mysite.modelos.userstory.serializer import UserStoryReadSerializer
from mysite.modelos.miembros_sprint.serializer import MiembrosSprintReadSerializer
from mysite.modelos.sprint.serializer import SprintReadSerializer
from mysite.modelos.historial_userstory.model import HistorialUserStory  
from mysite.modelos.miembros_sprint.model import MiembrosSprint

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""
class HistorialUserStoryReadSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase HistorialUserStoryReadSerializer**"""
    """Clase para serializar los datos de la tabla HistorialUserStory de la db, los campos miembro, us y sprint son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """

    
    us = UserStoryReadSerializer()
    sprint =SprintReadSerializer()
    miembro_sprint = MiembrosSprintReadSerializer()
    

    class Meta:
        model = HistorialUserStory
        fields = ['id','us','sprint','miembro_sprint','duracion_estimada_sprint','fecha_estimada','estado_tablero','horas_trabajadas_sprint','priorizacion','motivo_comentario','estado']

 
class HistorialUserStoryUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase HistorialUserStoryUpdateSerializer**"""
    """Clase para serializar los datos de la tabla HistorialUserStory de la db
    """
    
    us=serializers.PrimaryKeyRelatedField( queryset=UserStory.objects.all(), many=False)
    sprint=serializers.PrimaryKeyRelatedField( queryset=Sprint.objects.all(), many=False )
    miembro_sprint=serializers.PrimaryKeyRelatedField( queryset=MiembrosSprint.objects.all(), many=False )
    class Meta:
        model = HistorialUserStory
        fields = ['id','us','sprint','miembro_sprint','duracion_estimada_sprint','fecha_estimada','estado_tablero','horas_trabajadas_sprint','priorizacion','motivo_comentario','estado']