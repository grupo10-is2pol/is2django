
from rest_framework import routers, serializers, viewsets

from mysite.modelos.proyecto.model import TeamMember
from django.contrib.auth.models import User
from mysite.modelos.proyecto.model import Proyecto
from mysite.modelos.rol.model import RolProyecto

from mysite.modelos.authuser.serializer import AuthUserSerializer
from mysite.modelos.proyecto.serializer import ProyectoSerializer
from mysite.modelos.rol.serializer import RolProyectoReadSerializer 

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""

class TeamMemberReadSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase TeamMemberReadSerializer**"""
    """Clase para serializar los datos de la tabla proyecto de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    usuario=AuthUserSerializer()
    proyecto=ProyectoSerializer()
    rol_proyecto=RolProyectoReadSerializer()
    
    class Meta:
        model = TeamMember
        fields = ['id','usuario','proyecto','rol_proyecto','disponible']

class TeamMemberUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase TeamMemberUpdateSerializer**"""
    """Clase para serializar los datos de la tabla TeamMember de la db
    """
    usuario=serializers.PrimaryKeyRelatedField( queryset=User.objects.all(), many=False )
    proyecto=serializers.PrimaryKeyRelatedField( queryset=Proyecto.objects.all(), many=False )
    rol_proyecto=serializers.PrimaryKeyRelatedField( queryset=RolProyecto.objects.all(), many=False )
    
    class Meta:
        model = TeamMember
        fields = ['id','usuario','proyecto','rol_proyecto','disponible']
