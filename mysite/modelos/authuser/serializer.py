from django.db.models import fields
from rest_framework import routers, serializers, viewsets
from rest_framework.fields import ListField

from django.contrib.auth.models import User


class AuthUserSerializer(serializers.ModelSerializer):
    """ModelSerializerclase que proporciona un atajo útil 
       para crear serializadores que se ocupan de instancias 
       de modelos y conjuntos de consultas.
    """
    class Meta:
        model = User
        fields=['id', 'username' ,'email']
