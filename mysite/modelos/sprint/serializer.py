
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.proyecto.serializer import ProyectoSerializer
from mysite.modelos.proyecto.model import Proyecto


from rest_framework import routers, serializers, viewsets

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""
    
class SprintReadSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase SprintReadSerializer**"""
    """Clase para serializar los datos de la tabla sprint de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    proyecto=ProyectoSerializer()
    #dias_habiles = serializers.ListField(child=serializers.IntegerField())
    class Meta:
        model = Sprint
        fields = ['id','nombre','dias_laborales','proyecto','estado','fecha_inicio','fecha_fin']

        
 
class SprintUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase SprintUpdateSerializer**"""
    """Clase para serializar los datos de la tabla sprint de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    proyecto=serializers.PrimaryKeyRelatedField( queryset=Proyecto.objects.all(), many=False )
    #dias_habiles = serializers.ListField(child=serializers.IntegerField())
    class Meta:
        model = Sprint
        fields = ['id','nombre','dias_laborales','proyecto','estado','fecha_inicio','fecha_fin']

        