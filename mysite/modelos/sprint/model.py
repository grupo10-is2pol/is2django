
from django.db import models
from django.contrib import admin
from mysite.modelos.proyecto.model import Proyecto, TeamMember
from datetime import timezone, timedelta
from django.contrib.postgres.fields import ArrayField
from mysite.modelos.miembros_sprint.model import MiembrosSprint

"""***Estados posibles del sprint*** **('Pendiente')** **('En Proceso')** **('Terminado')**
"""
ESTADOS_SPRINT = (
    ('Pendiente', 'Pendiente'), #varios sprints Pendientes para planificar en el sprint planing
    ('En Proceso', 'En Proceso'),#! OJO: SOLO un Sprint En Proceso, este seria EL sprint ACtual
    ('Terminado', 'Terminado') # y pueden haBer varios sprint Terminados
)

class Sprint(models.Model):
    """**Clase Sprint**"""
    """ Modelo de la clase sprint, el cual representa un periodo de tiempo  definido dentro del proyecto al que se encuentra relacionado en el que se trabajan una cantidad definida de user stories
    """
    nombre = models.CharField(max_length=20, blank=False, null=False)
    dias_laborales = models.PositiveIntegerField(blank=False, null=False) #Cantidad de dias en TOTAL que se va a trabajar en el Sprint

    # guardar los dias asi ->>> [1,2,3,4,5,6,7]  que corresponden a [Lunes,Martes,..,....]
    dias_habiles = ArrayField(models.IntegerField(), blank=True, null=True) #Dias de La semana en los que se va a trabajar en el Sprint
    proyecto = models.ForeignKey(Proyecto ,on_delete=models.PROTECT, blank=False, null=False)#si se existe un Sprint creado para el proyecto, no se puede eliminar el Proyecto
    estado = models.CharField(max_length=25, choices=ESTADOS_SPRINT, default='Pendiente')
    fecha_inicio = models.DateField(blank=True, null=True) #se establece cuando el sprint se inicia: pasa a estado "En Proceso"
    fecha_fin = models.DateField(blank=True, null=True) #se establece cuando el sprint se termina: pasa a estado "Terminado"

    
    # def get_duracion_real(self):
    #     """**Metodo get_duracion_real**"""
    #     """metodo del modelo Sprint que retorna la cantidad de dias de duracion del sprint en días hábiles :return: :dias: la cantidad de dias entre la fecha de inicio del sprint y la fecha de finalizacion del sprint, en caso de no tener fecha de finalizacion aun, hasta la fecha de hoy
    #     """
    
    #     if not self.fecha_inicio: 
    #       return 0
    
    #     dias_habiles = self.dias_habiles
    #     inicio = self.fecha_inicio
    #     if self.fecha_fin:
    #         fin = self.fecha_fin
    #     else:
    #         fin = timezone.now().date()
    #     diaActual = inicio
    #     dias = 0
    #     while diaActual <= fin:
    #         if diaActual.isoweekday() in dias_habiles:
    #             dias += 1
    #         diaActual += timedelta(days=1)
    #     return dias

    # def get_horas_trabajadas(self):
    #     """metodo de la clase Sprint que retorna el total de horas trabajadas en el sprint
    #         :return: total de horas trabajadas en el sprint
    #     """
    #     actividades = Actividad.objects.filter(sprint=self.pk)
    #     horasTrabajadas = 0
    #     for actividad in actividades:
    #         horasTrabajadas += actividad.duracion
    #     return horasTrabajadas


    class Meta:

        constraints = [
            models.UniqueConstraint(fields=['nombre','proyecto'], name='Nombre del Sprint por Proyecto debe ser Unico'),
            models.CheckConstraint(
                name="DiasLaboralesDebeEstarEntre:1-365",
                check=models.Q(dias_laborales__range=(1, 365)),
            ),     
        ]
        db_table = "Sprint"
  
    def __str__(self):
        """retorna el nombre del sprint
        """
        return self.nombre
    

class MiembrosSprintInLine(admin.TabularInline):
    model = MiembrosSprint

class SprintAdmin(admin.ModelAdmin):
    inlines = (MiembrosSprintInLine,)

admin.site.register(Sprint,SprintAdmin)