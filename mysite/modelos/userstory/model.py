
from django.contrib import admin
from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields import IntegerField

from mysite.modelos.miembros_sprint.model import MiembrosSprint
from mysite.modelos.proyecto.model import Proyecto, TeamMember
from mysite.modelos.sprint.model import Sprint

"""
***Definicion de los estados de User Story***:**Pendiente**, **Asignado**, **No Finalizado**, **Finalizado** 
"""

ESTADOS_US = (                  #SE CAMBIA EL ESTADO DEL USERSTORY CUANDO:
    ('Pendiente', 'Pendiente'),#Cuando se Crea un US, se crea con estado PENDIENTE POR DEFAULT
    ('Asignado', 'Asignado'), #Cuando se Asigna un Us aun TeamMember y un Sprint en Especifico
    ('No Finalizado', 'No Finalizado'), #Cuando el Srint se termina y el UserStory se quedo en algun Estado de tablero (todo, doing,done,control de calidad)
    ('Finalizado','Finalizado') #Cuando Se finaliza el US, (Pasa el Control de Calidad) (El ScrumMaster da visto bueno)
)

"""
***Se definen los estados en fase de los User Story (EN EL TABLERO)***: **To Do**, **Doing**, **Done**, **QA**
"""

ESTADOS_EN_TABLERO = (  #CORRESPONDE A LOS ESTADOS QUE TIENEN LOS USERTORY CUANDO SE LOS MUEVEN EN EL TABLERO
    ('To Do', 'To Do'),
    ('Doing', 'Doing'),
    ('Done', 'Done'),
    ('Control de Calidad', 'Control de Calidad'),
)

ESTADOS_EN_CONTROL_CALIDAD = (  #CORRESPONDE A LOS ESTADOS QUE TIENEN LOS USERTORY CUANDO SE APRUEBA O RECHAZA
    ('Aprobado', 'Aprobado'),
    ('Rechazado', 'Rechazado'),
    ('Ninguno', 'Ninguno'),

)

"""
**Se define el modelo User Story**
"""
class UserStory(models.Model):
    """**Clase UserStory**"""
    """
    Modelo de la clase User Story, el cual representa un conjunto de tareas a ser realizadas en un periodo de tiempo especificado
    """
    #Administracion User Stories (POST)
    proyecto=models.ForeignKey(Proyecto, on_delete=models.PROTECT) # si ya se creo un US asiociado a un Proyecto, no se puede eliminar el Proyecto asociado
    nombre = models.CharField(max_length=100, blank=False, null=False)
    valor_negocio = models.PositiveIntegerField(blank=False, null=False)
    prioridad = models.PositiveIntegerField(blank=False, null=False)
    valor_tecnico = models.PositiveIntegerField(blank=False, null=False)

    descripcion = models.TextField(max_length=300,blank=True, null=True)
    estado = models.CharField(max_length=30, choices=ESTADOS_US, default='Pendiente')
    estado_tablero = models.CharField(max_length=30, choices=ESTADOS_EN_TABLERO, default='To Do')
    priorizacion=models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    #priorizacion= models.IntegerField() #Esto se usa para Ordenar los US en el Product Backlog
    
    
    

    #Sprint Planing (PUT del User Storie ya creado en Administracion User Stories) (Se actualizan estos campos)
    duracion_estimada = models.IntegerField(blank=True, null=True) #en horas, se estima en el Sprint Planing, ***NO AL CREAR EL US***
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, blank=True, null=True) #se le pasa el codigo_sprint del sprint que creamos en Administracion de Sprint (Con estado pendiente)
    
    miembro_sprint = models.ForeignKey(MiembrosSprint,on_delete=models.PROTECT, blank=True, null=True) #miembro del sprint al cual se le asigna el User Story
    miembro = models.ForeignKey(TeamMember,on_delete=models.PROTECT, blank=True, null=True) #miembro del proyecto al cual se le asigna el User Story
    usuario = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True) #no se puede eliminar usuario si un us esta usando a el Usuario (on_delete=models.PROTECT),                                             

    
    duracion_restante = models.IntegerField(null=True,blank=True)
    horas_trabajadas_us = models.IntegerField(null=True,blank=True)
    porcentaje_realizado = models.IntegerField(null=True,blank=True)

    horas_extras_trabajadas_us = models.IntegerField(null=True,blank=True)
    motivo_comentario = models.CharField(max_length=1000, blank=True, null=True)
    estado_qa = models.CharField(max_length=30, choices=ESTADOS_EN_CONTROL_CALIDAD, default='Ninguno')

    class Meta:
        """Con este tipo de restricción, se especifica que los valores ingresados en la columna deben cumplir la regla o formula especificada
        """
        constraints = [
            models.CheckConstraint(
                name="ValorDeNegocioDebeEstarEntre:0-10",
                check=models.Q(valor_negocio__range=(0, 10)),
            ),
            models.CheckConstraint(
                name="ValorTecnicoDebeEstarEntre:0-10",
                check=models.Q(valor_tecnico__range=(0, 10)),
            ),
            models.CheckConstraint(
                name="PrioridadDebeEstarEntre:0-10",
                check=models.Q(prioridad__range=(0, 10)),
            ),
            models.CheckConstraint(
                name="DuracionEstimadaUSDebeEstarEntre:1-1000Horas",
                check=models.Q(duracion_estimada__range=(0, 1000)),
            ),
        ]

        db_table = "UserStory"

    def __str__(self):
        """ Metodo que retorna el nombre del us actual:return: retorna el valor del campo nombre del objeto actual
        """
        return self.nombre

admin.site.register(UserStory)
















#TODO preguntar al profe si es que hay varios flujos(tableros) asociados a un sprint
# # flujo = models.ForeignKey(Flujo, on_delete=models.PROTECT,blank=False, null=False)
#!mirar, en el caso que un us no se haya culminado se podria utilizar en otro sprint
#sprints_asignados = models.ManyToManyField(Sprint, blank=True, related_name='userstory_sprint_asignado')
#TODO preguntar al profe si es que un tablero puede tener varias FASES-MODULOS
# fase = models.ForeignKey(Fase, on_delete=models.PROTECT, null=True, blank=True)
