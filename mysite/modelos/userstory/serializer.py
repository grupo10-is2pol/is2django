from mysite.modelos.miembros_sprint.model import MiembrosSprint
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

from mysite.modelos import miembros_sprint
from mysite.modelos.miembros.serializer import TeamMemberReadSerializer
from mysite.modelos.miembros_sprint.serializer import MiembrosSprintReadSerializer
from mysite.modelos.profile.model import Profile
from mysite.modelos.proyecto.model import Proyecto, TeamMember
from mysite.modelos.proyecto.serializer import ProyectoSerializer
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.sprint.serializer import SprintReadSerializer
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.authuser.serializer import AuthUserSerializer

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""
class UserStoryReadSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase UserStoryReadSerializer**"""
    """Clase para serializar los datos de la tabla us de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    proyecto=ProyectoSerializer()
    sprint=SprintReadSerializer()
    miembro=TeamMemberReadSerializer()
    miembro_sprint=MiembrosSprintReadSerializer()
    usuario=AuthUserSerializer()

    class Meta:
        model = UserStory
        fields = ['id','nombre','proyecto','valor_negocio','prioridad',
                 'valor_tecnico','priorizacion','descripcion','estado','estado_tablero',
                 'duracion_estimada','sprint','miembro','miembro_sprint',
                 'duracion_restante','usuario','horas_trabajadas_us','porcentaje_realizado','horas_extras_trabajadas_us','motivo_comentario','estado_qa']



class UserStoryUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase UserStoryUpdateSerializer**"""
    """Clase para serializar los datos de la tabla us de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    proyecto=serializers.PrimaryKeyRelatedField( queryset=Proyecto.objects.all(), many=False )
    sprint=serializers.PrimaryKeyRelatedField( queryset=Sprint.objects.all(), many=False ,required=False, allow_null=True) #campo no obligatorio rest framework es rompe bolas con esto
    miembro_sprint=serializers.PrimaryKeyRelatedField( queryset=MiembrosSprint.objects.all(), many=False ,required=False, allow_null=True)
    
    #estos dos complican mas, mejor no usar
    miembro=serializers.PrimaryKeyRelatedField( queryset=TeamMember.objects.all(), many=False ,required=False, allow_null=True) #https://stackoverflow.com/questions/14382230/nullable-foreignkey-fields-in-django-rest-framework
    usuario=serializers.PrimaryKeyRelatedField( queryset=User.objects.all(), many=False ,required=False, allow_null=True)
    class Meta:
        model = UserStory
        fields = ['id','nombre','proyecto','valor_negocio','prioridad',
                 'valor_tecnico','priorizacion','descripcion','estado','estado_tablero',
                 'duracion_estimada','sprint','miembro','miembro_sprint',
                 'duracion_restante','usuario','horas_trabajadas_us','porcentaje_realizado','horas_extras_trabajadas_us','motivo_comentario','estado_qa']

    
