from email.policy import default
from operator import add

from django.contrib import admin
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from mysite.models import RolSistema


class Profile(models.Model):
    """**Clase Profile**"""
    """Clase que extiende al modelo de Usuario Predeterminado de Django, relacionando la tabla auth_user con la tabla profile, mediante un OneToOneField, ademas se agregaron campos necesarios para gestionar los roles de sistema de los usuarios, con la opcion de agregar mas campos en el futuro, en caso de necesidad.. 
    """
    
    user = models.OneToOneField(User, on_delete=models.CASCADE) #se elimina el user, tambien se elimina el profile
    descripcion = models.TextField(null=True, blank=True)
    rol_sistema = models.ForeignKey(RolSistema, blank=True, null=True, default=None, on_delete=models.PROTECT, verbose_name='Rol de Sistema') #no deja eliminar Rol Sistema si un Perfil esta usando ese ROL
    
    """Roles:De sistema: -Invitado (rol con permiso de ver la pagina de inicio) obs: el admin debe asignar un rol para que el usuario pueda hacer algo.-Administrador (rol con permisos de ver la pagina de pagina de administracion).
    """
    class Meta:
        db_table="Profile"


    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        """**Clase create_user_profile**"""
        """Metodo que crea automaticamente el perfil de usuario cuando se crea un usuario y agrega un rol por default (Invitado)
        """
        if created:
            Profile.objects.create(user = instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        """**Clase save_user_profile**"""
        """Metodo que guarda el perfil de usuario creado automaticamente al crear un usuario
        """
        instance.profile.save()
    
    def __str__(self):
        """Metodo que retorna el nombre del usuario:return: retorna el valor del campo username del objeto
        """
        return self.user.username

admin.site.register(Profile)
