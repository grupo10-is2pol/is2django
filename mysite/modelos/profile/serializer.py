from mysite.modelos.rol.model import Permiso, RolSistema
from .model import Profile
from django.contrib.auth.models import User

from rest_framework import routers, serializers, viewsets
from mysite.modelos.rol.serializer import RolSistemaReadSerializer
"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase UserSerializer**"""
    """Clase para serializar los datos de la tabla auth_user de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    class Meta:
        model = User
        fields = ['id', 'username']


class TagPermisosSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase TagPermisosSerializer**"""
    """Clase para serializar los datos de la tabla de permisos de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    class Meta:
        model = Permiso
        fields = ['nombre']


class TagRolSistemaSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase TagRolSistemaSerializer**"""
    """Clase para serializar los datos de la tabla de los permisos de sistema de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    permisos = TagPermisosSerializer(read_only=True, many=True)

    class Meta:
        model = RolSistema
        fields = ['permisos', 'nombre', 'descripcion']


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase ProfileSerializer**"""
    """Clase para serializar los datos de la tabla rol_sistema de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    user = UserSerializer()
    rol_sistema = RolSistemaReadSerializer()

    class Meta:
        model = Profile
        fields = ['id', 'user', 'rol_sistema', 'descripcion']


class ProfileUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase ProfileUpdateSerializerProfileSerializer**"""
    """Clase para serializar los datos de la tabla rol_sistema de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """

    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), many=False)
    rol_sistema = serializers.PrimaryKeyRelatedField(
        queryset=RolSistema.objects.all(), many=False)

    class Meta:
        model = Profile
        fields = ['id', 'user', 'rol_sistema', 'descripcion']
