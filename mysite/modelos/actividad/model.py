
from django.db import models
from django.contrib import admin
from mysite.modelos.proyecto.model import Proyecto,TeamMember
from mysite.modelos.miembros_sprint.model import MiembrosSprint
from mysite.modelos.userstory.model import UserStory

ESTADOS_EN_TABLERO = (  #CORRESPONDE A LOS ESTADOS QUE TIENEN LOS USERTORY CUANDO SE LOS MUEVEN EN EL TABLERO
    ('To Do', 'To Do'),
    ('Doing', 'Doing'),
    ('Done', 'Done'),
    ('Control de Calidad', 'Control de Calidad'),
)

class Actividad(models.Model):
    """**Clase Actividad**"""
    """Clase para agregar una actividad a un User Story, actividad son las horas trabajadas por us con los campos, nombre, duracion descripcion, fecha, miembro, el us y el sprint 
    """
    nombre = models.CharField(max_length=255,blank=False, null=False)
    duracion = models.PositiveIntegerField(blank=False, null=False)
    descripcion = models.TextField(blank=True, null=True)
    fecha = models.DateField(blank=False,null=False)
    
    miembro = models.ForeignKey(MiembrosSprint, on_delete=models.PROTECT)
    us = models.ForeignKey('UserStory',on_delete=models.CASCADE, blank=False, null=False)
    sprint = models.ForeignKey('Sprint', on_delete=models.CASCADE)
    estado_tablero = models.CharField(max_length=30, choices=ESTADOS_EN_TABLERO, default='Ninguno')
    

    def __str__(self):
        """Retorna el nombre del objeto actual return nombre del objeto actual
        """
        return self.nombre

    class Meta:
        """Con este tipo de restricción, se especifica que los valores ingresados en la columna deben cumplir la regla o formula especificada
        """
        constraints = [
            models.CheckConstraint(
                name="DuracionDebeEstarEntre:1-1000",
                check=models.Q(duracion__range=(0, 1000)),
            ),
        ]
        db_table="Actividad"
admin.site.register(Actividad)
