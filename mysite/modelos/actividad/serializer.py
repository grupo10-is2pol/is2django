
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.proyecto.model import TeamMember


from rest_framework import routers, serializers, viewsets
from mysite.modelos.userstory.serializer import UserStoryReadSerializer
from mysite.modelos.miembros_sprint.serializer import MiembrosSprintReadSerializer
from mysite.modelos.sprint.serializer import SprintReadSerializer
from mysite.modelos.actividad.model import Actividad
from mysite.modelos.miembros_sprint.model import MiembrosSprint

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""
class ActividadReadSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase ActividadReadSerializer**"""
    """Clase para serializar los datos de la tabla actividad de la db, los campos miembro, us y sprint son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    miembro=MiembrosSprintReadSerializer()
    us =UserStoryReadSerializer()
    sprint =SprintReadSerializer()
    #dias_habiles = serializers.ListField(child=serializers.CharField())
    class Meta:
        model = Actividad
        fields = ['id','nombre','descripcion','duracion','miembro','us','fecha','sprint','estado_tablero']

 
class ActividadUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase ActividadUpdateSerializer**"""
    """Clase para serializar los datos de la tabla actividad de la db
    """
    miembro=serializers.PrimaryKeyRelatedField( queryset=MiembrosSprint.objects.all(), many=False )
    us=serializers.PrimaryKeyRelatedField( queryset=UserStory.objects.all(), many=False)
    sprint=serializers.PrimaryKeyRelatedField( queryset=Sprint.objects.all(), many=False )
    #dias_habiles = serializers.ListField(child=serializers.CharField())
    class Meta:
        model = Actividad
        fields = ['id','nombre','descripcion','duracion','miembro','us','fecha','sprint','estado_tablero']