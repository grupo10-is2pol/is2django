from mysite.modelos.sprint.model import Sprint
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.tablero.model import Tablero


from rest_framework import routers, serializers, viewsets
from mysite.modelos.userstory.serializer import UserStoryReadSerializer
from mysite.modelos.sprint.serializer import SprintReadSerializer
    
class TableroReadSerializer(serializers.HyperlinkedModelSerializer):
    """Clase para serializar los datos de la tabla sprint de la db
    """
    sprint=SprintReadSerializer()
    us=UserStoryReadSerializer()
    
    class Meta:
        model = Tablero
        fields = ['id','sprint','us']


 
class TableroUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """Clase para serializar los datos de la tabla sprint de la db
    """
    sprint=serializers.PrimaryKeyRelatedField( queryset=Sprint.objects.all(), many=False )
    us=serializers.PrimaryKeyRelatedField( queryset=UserStory.objects.all(), many=False )
    
    class Meta:
        model = Tablero
        fields = ['id','sprint','us']