from email.policy import default
from operator import add

from django.contrib import admin
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from mysite.modelos.sprint.model import Sprint
from mysite.modelos.userstory.model import UserStory


class Tablero(models.Model):
    """
    """
    
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE) #se elimina el sprint, tambien se elimina el Tablero
    us = models.ForeignKey(UserStory, on_delete=models.PROTECT,blank=True, null=True)

    class Meta:
        db_table="Tablero"


    # @receiver(post_save, sender=Sprint)
    # def create_tablero(sender, instance, created, **kwargs):
    #     """Metodo que crea automaticamente el tablero asociado cuando se crea un Sprint en el Proyecto
    #     """
    #     if created:
    #         Tablero.objects.create(sprint = instance)

    # @receiver(post_save, sender=Sprint)
    # def save_tablero(sender, instance, **kwargs):
    #     """Metodo que guarda el tablero asociado creado automaticamente al crear un Sprint en el Proyecto
    #     """
    #     instance.tablero.save()
    
    # def __str__(self):
    #     """Metodo que retorna el nombre del tablero
    #        :return: retorna el valor del campo username del objeto
    #     """
    #     return self.user.username

admin.site.register(Tablero)
