
from .model import Proyecto


from rest_framework import routers, serializers, viewsets

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""
    
class ProyectoSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase ProyectoSerializer**"""
    """Clase para serializar los datos de la tabla proyecto de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    
    
    class Meta:
        model = Proyecto
        fields = ['id','nombre','fecha_inicio','fecha_fin','estado','descripcion']

class ProyectoUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase ProyectoUpdateSerializer**"""
    """Clase para serializar los datos de la tabla proyecto de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    #usuario_scrum=serializers.PrimaryKeyRelatedField( queryset=Profile.objects.all(), many=False )
    
    class Meta:
        model = Proyecto
        fields = ['id','nombre','fecha_inicio','fecha_fin','estado','descripcion']