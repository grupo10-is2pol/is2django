from mysite.modelos.rol.model import RolProyecto
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from mysite.models import Profile


"""***Definimos los estados de un Proyecto***
    ('Pendiente', 'Pendiente'),
    ('Activo', 'Activo'),
    ('Cancelado','Cancelado'),
    ('Terminado', 'Terminado'),
    ('Suspendido', 'Suspendido') 
"""

ESTADOS_PROYECTO = (
    ('Pendiente', 'Pendiente'),
    ('Activo', 'Activo'),
    ('Cancelado', 'Cancelado'),
    ('Terminado', 'Terminado'),
    ('Suspendido', 'Suspendido')
)


class Proyecto(models.Model):
    """**Clase Proyecto**"""
    """
    Implementa la clase de Proyectos, almacena datos generales acerca del proyecto:
    fecha de inicio, fecha fin, nombre, estado y descripcion y ademas quien será el
    scrum Master del mismo
    """
    nombre = models.CharField(
        max_length=300, unique=True, blank=False, null=False)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)
    estado = models.CharField(
        max_length=25, choices=ESTADOS_PROYECTO, default='Pendiente')
    descripcion = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "Proyecto"

    def __str__(self):
        """Metodo que retorna el nombre del proyecto actual:return: retorna el valor del campo nombre del objeto actual
        """
        return self.nombre


class TeamMember(models.Model):
    """**Clase TeamMember**"""
    """Implementa la clase de Team Member, el cual es una combinacion de un usuario y un rol ligada a un proyecto
    """
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, null=False,
                                 blank=False)  # se elimina Proyecto, tambien los teamMembers (on_delete=models.CASCADE)
    # no se puede eliminar usuario si un TeamMember esta usando a el Usuario (on_delete=models.PROTECT),
    usuario = models.ForeignKey(
        User, on_delete=models.PROTECT, blank=False, null=True, unique=False)
    # si se elimina rol_proyecto el TeamMember que estaba usando el ROL se queda sin rol (Set_null)
    rol_proyecto = models.ForeignKey(
        RolProyecto, on_delete=models.SET_NULL, blank=False, null=True)
    disponible = models.BooleanField(
        verbose_name='Esta Disponible Para Trabajar en el Sprint', default=True)

    # https://medium.com/@inem.patrick/django-database-integrity-foreignkey-on-delete-option-db7d160762e4

    class Meta:
        """Con este tipo de restricción, se especifica que los valores ingresados en la columna deben cumplir la regla o formula especificada
        """
        constraints = [
            models.UniqueConstraint(
                fields=['usuario', 'proyecto'], name='Miembro debe ser Unico'),

        ]

        db_table = "TeamMember"

    def __str__(self):
        """Metodo que retorna el nombre del miembro actual :return: retorna el valor del campo nombre del objeto actual
        """
        return self.usuario.username


class TeamMemberInLine(admin.TabularInline):
    model = TeamMember


class ProyectoAdmin(admin.ModelAdmin):
    inlines = (TeamMemberInLine,)


admin.site.register(Proyecto, ProyectoAdmin)
