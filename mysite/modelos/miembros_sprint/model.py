
from django.db import models
from django.contrib import admin
from mysite.modelos.proyecto.model import Proyecto, TeamMember
from django.contrib.auth.models import User

    


class MiembrosSprint(models.Model):
    """**Clase MiembrosSprint**"""
    """Modelo de Horas, el cual es una relacion entre un team member perteneciente al proyecto al que se relaciona el sprint en el que se están asignando las horas laborales
    """
    horas_laborales = models.IntegerField(blank=False, null=False) #HORAS POR DIA LABORAL
    team_member = models.ForeignKey('TeamMember', on_delete=models.CASCADE, blank=False, null=False)#si se elimina el team member se elimina las miembros del sprint #!hacer un select de miembro por proyecto
    sprint = models.ForeignKey('Sprint', on_delete=models.CASCADE, blank=False, null=False)#si se elimina el sprint se elimina las miembros del sprint              #!hacer un select de sprint por proyecto
    usuario = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True) #no se puede eliminar usuario si un miembrosprint esta usando a el Usuario (on_delete=models.PROTECT),                                             


    class Meta:
        """Con este tipo de restricción, se especifica que los valores ingresados en la columna deben cumplir la regla o formula especificada
        """
        constraints = [

            models.UniqueConstraint(fields=['team_member','sprint'], name='Miembro del Sprint debe ser Unico'),

            models.CheckConstraint(
                name="HorasLaboralesDelMiembroDebeEstarEntre:1-24", #TRABAJAR 24 HORAS EQUISDE
                check=models.Q(horas_laborales__range=(1, 24)),
            )
        ]
        db_table="MiembrosSprint"
    
    def __str__(self):
        """Metodo que retorna el nombre del miembro actual del sprint:return: retorna el valor del campo nombre del objeto actual
        """
        return self.team_member.usuario.username



    
admin.site.register(MiembrosSprint)
