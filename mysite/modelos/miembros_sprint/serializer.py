
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.proyecto.model import TeamMember
from mysite.modelos.miembros_sprint.model import MiembrosSprint
from mysite.modelos.proyecto.serializer import ProyectoSerializer
from mysite.modelos.sprint.serializer import SprintReadSerializer
from mysite.modelos.miembros.serializer import TeamMemberReadSerializer
from mysite.modelos.authuser.serializer import AuthUserSerializer
from django.contrib.auth.models import User


from rest_framework import routers, serializers, viewsets

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""
    
class MiembrosSprintReadSerializer(serializers.HyperlinkedModelSerializer):
    """**MiembrosSprintReadSerializer**"""
    """Clase para serializar los datos de la tabla miembro del sprint de la db son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    team_member=TeamMemberReadSerializer()
    sprint=SprintReadSerializer()
    usuario=AuthUserSerializer()
    class Meta:
        model = MiembrosSprint
        fields = ['id','horas_laborales','team_member','sprint','usuario']
 
class MiembrosSprintUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase MiembrosSprintUpdateSerializer**"""
    """Clase para serializar los datos de la tabla miembro del sprint de la db
    """
    team_member=serializers.PrimaryKeyRelatedField( queryset=TeamMember.objects.all(), many=False )
    sprint=serializers.PrimaryKeyRelatedField( queryset=Sprint.objects.all(), many=False )
    usuario=usuario=serializers.PrimaryKeyRelatedField( queryset=User.objects.all(), many=False )
    class Meta:
        model = MiembrosSprint
        fields = ['id','horas_laborales','team_member','sprint','usuario']