from mysite.modelos.profile.model import Profile
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.userstory.serializer import UserStoryReadSerializer
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.nota.model import Nota


from rest_framework import routers, serializers, viewsets

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""
    
class NotaReadSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase NotaReadSerializer**"""
    """Clase para serializar los datos de la tabla nota de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    us=UserStoryReadSerializer()
    #dias_habiles = serializers.ListField(child=serializers.CharField())
    class Meta:
        model = Nota
        fields = ['id','nota','us','fecha']

        
 
class NotaUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase NotaUpdateSerializer**"""
    """Clase para serializar los datos de la tabla nota de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    us=serializers.PrimaryKeyRelatedField( queryset=UserStory.objects.all(), many=False,required=False )
    
    class Meta:
        model = Nota
        fields = ['id','nota','us','fecha']

        