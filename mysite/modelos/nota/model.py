
from django.db import models
from django.contrib import admin
from mysite.modelos.proyecto.model import Proyecto
from mysite.modelos.miembros_sprint.model import MiembrosSprint
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.userstory.model import UserStory

class Nota(models.Model):
    """**Clase Nota**"""
    """Clase para adjuntar una nota a un US con los campos: nota, us y fecha
    """
    nota = models.TextField(blank=False, null=False)
    us = models.ForeignKey('UserStory', on_delete=models.CASCADE,blank=False, null=False)
    fecha = models.DateTimeField(auto_now_add=True)

    #usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.PROTECT, null=True)
    #sprint = models.ForeignKey('sprint.Sprint', on_delete=models.CASCADE)

    def __str__(self):
        """Retorna el contenido de la nota actual:return: campo nota del objeto actual
        """
        return self.nota
    class Meta:
        db_table="Nota"
        
admin.site.register(Nota)
