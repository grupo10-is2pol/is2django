
from django.db import models
from django.contrib import admin
from mysite.modelos.miembros_sprint.model import MiembrosSprint
#from mysite.modelos.userstory.model import UserStory
from mysite.modelos.sprint.model import Sprint

class HistorialEstadosUserStory(models.Model): #el historial se usa en el tablero
    """**Clase HistorialEstadosUserStory**"""
    """Clase para agregar un User Story a una tabla donde se pueden ver el historial de ese User Story, contiene datos como el userstorie, el sprint, el miembro asignado, la duracion estimada, el estado en tablero
    """

    #se crea (Post HistorialEstadosUserStory) cuando se asigan userstories en el Sprint Planing
    us = models.ForeignKey('UserStory',on_delete=models.CASCADE, blank=False, null=False)
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, blank=False, null=False)
    miembro_sprint = models.ForeignKey(MiembrosSprint,on_delete=models.PROTECT, blank=False, null=False)

    #se actualizan (Put HistorialEstadosUserStory) cuando se mueven userstories en el tablero kanban
    estado_tablero = models.CharField(max_length=30, blank=True, null=True)
    fecha_cambio_estado = models.DateField(blank=True,null=True)
    hora_cambio_estado = models.CharField(max_length=30, blank=True, null=True)


    def __str__(self):
        """Retorna el nombre del objeto actual return nombre del objeto actual
        """
        return self.us.nombre

    class Meta:
        """Con este tipo de restricción, se especifica que los valores ingresados en la columna deben cumplir la regla o formula especificada
        """
        # constraints = [
        #     models.CheckConstraint(
        #         name="DuracionDebeEstarEntre:1-1000",
        #         check=models.Q(duracion_estimada__range=(1, 1000)),
        #     ),
        # ]

        db_table="HistorialEstadosUserStory"

admin.site.register(HistorialEstadosUserStory)

