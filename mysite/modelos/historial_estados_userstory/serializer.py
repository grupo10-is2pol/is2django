
from mysite.modelos.userstory.model import UserStory
from mysite.modelos.sprint.model import Sprint


from rest_framework import routers, serializers, viewsets
from mysite.modelos.userstory.serializer import UserStoryReadSerializer
from mysite.modelos.miembros_sprint.serializer import MiembrosSprintReadSerializer
from mysite.modelos.sprint.serializer import SprintReadSerializer
from mysite.modelos.historial_estados_userstory.model import HistorialEstadosUserStory  
from mysite.modelos.miembros_sprint.model import MiembrosSprint

"""La clase HyperlinkedModelSerializer es similar a la clase ModelSerializer excepto que usa hipervínculos para representar relaciones, en lugar de claves primarias. De forma predeterminada, el serializador incluirá un campo de URL en lugar de un campo de clave principal. El campo de URL se representará mediante un campo de serializador HyperlinkedIdentityField, y cualquier relación en el modelo se representará mediante un campo de serializador HyperlinkedRelatedField."""
class HistorialEstadosUserStoryReadSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase HistorialEstadosUserStoryReadSerializer**"""
    """Clase para serializar los datos de la tabla HistorialEstadosUserStory de la db, los campos miembro, us y sprint son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """

    
    us = UserStoryReadSerializer()
    sprint =SprintReadSerializer()
    miembro_sprint = MiembrosSprintReadSerializer()
    

    class Meta:
        model = HistorialEstadosUserStory
        fields = ['id','us','sprint','miembro_sprint','estado_tablero','fecha_cambio_estado','hora_cambio_estado']

 
class HistorialEstadosUserStoryUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """**Clase HistorialEstadosUserStoryUpdateSerializer**"""
    """Clase para serializar los datos de la tabla HistorialEstadosUserStory de la db
    """
    
    us=serializers.PrimaryKeyRelatedField( queryset=UserStory.objects.all(), many=False)
    sprint=serializers.PrimaryKeyRelatedField( queryset=Sprint.objects.all(), many=False )
    miembro_sprint=serializers.PrimaryKeyRelatedField( queryset=MiembrosSprint.objects.all(), many=False )
    
    class Meta:
        model = HistorialEstadosUserStory
        fields = ['id','us','sprint','miembro_sprint','estado_tablero','fecha_cambio_estado','hora_cambio_estado']