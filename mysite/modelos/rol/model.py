from django.db import models
from django.contrib import admin



TIPOS_PERMISO = (
    ('Administracion', 'Administracion'),
    ('Estandar', 'Estandar')
)
class Permiso(models.Model):
    """**Clase Permiso**"""
    """Implementa la clase de permisos, con los campos nombre, tipo_permiso y desactivado
    """
    nombre = models.CharField(max_length=75, unique=True, blank=False, null=False)
    tipo_permiso = models.CharField(max_length=25, choices=TIPOS_PERMISO, default='Estandar')
    desactivado= models.BooleanField(verbose_name='Es seleccionable', default=False)

    class Meta:
        db_table="Permiso"

    def __str__(self):
        """Metodo que retorna el nombre del permiso actual:return: retorna el valor del campo nombre del objeto actual
        """
        return self.nombre

class RolSistema(models.Model):
    """**Clase RolSistema**"""
    """Esta clase Implementa la clase RolSistema, almacena datos generales acerca del Rol:nombre, descripcion y permisos
    """
    nombre = models.CharField(max_length=55, unique=True, blank=False, null=False)
    descripcion = models.TextField(blank=True, null=True)
    permisos = models.ManyToManyField('Permiso', blank=False)
    es_modificable = models.BooleanField(verbose_name='Es modificable', default=True)

    class Meta:
        db_table="RolSistema"

    def __str__(self):
        """Metodo que retorna el nombre del rol actual:return: retorna el valor del campo nombre del objeto
        """
        return self.nombre
class RolProyecto(models.Model):
    """**Clase RolProyecto**"""
    """Esta clase Implementa la clase RolProyecto, almacena datos generales acerca del Rol: nombre, descripcion, permisos."""
    nombre = models.CharField(max_length=55, unique=False, blank=False, null=False)
    descripcion = models.TextField(blank=True, null=True)
    permisos = models.ManyToManyField('Permiso', blank=False)
    es_unico = models.BooleanField(verbose_name='Rol único en el proyecto', default=False)
    es_modificable = models.BooleanField(verbose_name='Es modificable', default=True)
    proyecto = models.ForeignKey('Proyecto' ,on_delete=models.CASCADE,blank=True, null=True) #si se elimina proyecto, se eliminan todos los roles asociados al proyecto

    class Meta:
        """Con este tipo de restricción, se especifica que los valores ingresados en la columna deben cumplir la regla o formula especificada
        """
        constraints = [
            models.UniqueConstraint(fields=['nombre','proyecto'], name='Nombre de Roles Unicos Por Proyecto'),

        ]

        db_table="RolProyecto"
   

    def __str__(self):
        """Metodo que retorna el nombre del rol actual:return: retorna el valor del campo nombre del objeto
        """
        return self.nombre

admin.site.register(Permiso)
admin.site.register(RolProyecto)
admin.site.register(RolSistema)