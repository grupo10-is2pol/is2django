from mysite.vistas import proyecto
from django.db.models import fields
from rest_framework import routers, serializers, viewsets
from rest_framework.fields import ListField

from .model import Permiso, RolProyecto, RolSistema
from mysite.modelos.proyecto.model import Proyecto
from mysite.modelos.proyecto.serializer import ProyectoSerializer
"""La clase ModelSerializer proporciona un acceso directo que le permite crear automáticamente una clase Serializer con campos que corresponden a los campos del modelo.La clase ModelSerializer es la misma que una clase Serializer normal, excepto que Generará automáticamente un conjunto de campos para usted, basado en el modelo. Generará automáticamente validadores para el serializador, como validadores unique_together. Incluye implementaciones simples predeterminadas de .create()y .update()."""
class PermisoSerializer(serializers.ModelSerializer):
    """**Clase NotaReadSerializer**"""
    """Clase para serializar los datos de la tabla sprint de la db, son instancias en donde se serializan de forma distinta De forma predeterminada, todos los campos del modelo de la clase se asignarán a los campos del serializador correspondientes.
    """
    class Meta:
        model = Permiso
        fields=['id', 'nombre','tipo_permiso','desactivado' ]
        read_only_fields = ['nombre','tipo_permiso']

class PermisoSerializerManytoMany(serializers.ModelSerializer):
    """**Clase PermisoSerializerManytoMany**"""
    """Clase para serializar el modelo Permiso de la db, transformamos los datos de array de objetos a solo array de nombres de permisos para poder listarlos mas facilmente en el front
    """
    queryset=Permiso.objects.all()

    def to_representation(self, value):
        return value    
    class Meta:
        model = Permiso
        fields = [ 'id' ]
        read_only_fields = [ 'nombre' ]

class RolSistemaUpdateSerializer(serializers.ModelSerializer):
    """**Clase RolSistemaUpdateSerializer**"""
    """Clase para serializar el modelo rol sistema de la db, transformamos los datos de array de objetos a solo array de nombres de permisos para poder listarlos mas facilmente en el front
    """   
    permisos= serializers.PrimaryKeyRelatedField( queryset=Permiso.objects.all(), many=True )
    class Meta:
        model = RolSistema
        fields = ['id','permisos','nombre','descripcion']

class RolSistemaReadSerializer(serializers.ModelSerializer):
    """**Clase RolSistemaReadSerializer**"""
    """Clase para serializar el modelo rol sistema de la db, transformamos los datos de array de objetos a solo array de nombres de permisos para poder listarlos mas facilmente en el front
    """   
    permisos= PermisoSerializer(read_only=True, many=True)
    class Meta:
        model = RolSistema
        fields = ['id','permisos','nombre','descripcion']
        read_only_fields = ['permisos']


class RolProyectoUpdateSerializer(serializers.ModelSerializer):
    """**Clase RolProyectoUpdateSerializer**"""
    """Clase para serializar el modelo Rol de la db
    """    
    permisos= serializers.PrimaryKeyRelatedField( queryset=Permiso.objects.all(), many=True )
    proyecto= serializers.PrimaryKeyRelatedField( queryset=Proyecto.objects.all(), many=False )
    class Meta:
        model = RolProyecto
        fields = ['id','permisos','nombre','descripcion','es_unico','proyecto','es_modificable']
        read_only_fields = ['permisos']

class RolProyectoReadSerializer(serializers.ModelSerializer):
    """**Clase RolProyectoReadSerializer**"""
    """Clase para serializar el modelo Rol de la db
    """    
    permisos= PermisoSerializer(read_only=True, many=True)
    proyecto = ProyectoSerializer()
    class Meta:
        model = RolProyecto
        fields = ['id','permisos','nombre','descripcion','es_unico','proyecto','es_modificable']
        read_only_fields = ['permisos']

# class RolProyectoReadSerializer(serializers.ModelSerializer):
#     
#     class Meta:
#         model = RolProyecto
#         fields = ['id','permisos','nombre','descripcion','es_unico']

# class RolProyectoUpdateSerializer(serializers.ModelSerializer):
#     
#     permisos=PermisoSerializer(read_only=False, many=True)
    
#     class Meta:
#         model = RolProyecto
#         fields = ['id','permisos','nombre','descripcion','es_unico']
