"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.contrib import admin
from django.urls import path
from django.urls import path, include # <--
from django.views.generic import TemplateView
from django.contrib.auth.views import LogoutView
from rest_framework import routers, serializers, viewsets


from mysite.api.rol_sistema_viewset import RolSistemaViewSet
from mysite.api.rol_proyecto_viewset import RolProyectoViewset
from mysite.api.profile_viewset import ProfileViewSet
from mysite.api.proyecto_viewset import ProyectoViewSet
from mysite.api.permiso_viewset import PermisoViewSet
from mysite.api.authuser_viewset import AuthUserViewSet
from mysite.api.miembros_viewset import TeamMemberViewSet
from mysite.api.sprint_viewset import SprintViewSet
from mysite.api.miembros_sprint_viewset import MiembrosSprintViewSet
from mysite.api.userstory_viewset import UserStoryViewSet
from mysite.api.actividad_viewset import ActividadViewSet
from mysite.api.nota_viewset import NotaViewSet
from mysite.api.tablero_viewset import TableroViewSet
from mysite.api.historial_userstory_viewset import HistorialUserStoryViewSet
from mysite.api.historial_estados_userstory_viewset import HistorialEstadosUserStoryViewSet
from mysite.vistas.reportes_pdf.vista import *

# API endpoints

router = routers.DefaultRouter()
router.register(r'rol-sistema', RolSistemaViewSet)
router.register(r'rol-proyecto', RolProyectoViewset)
router.register(r'usuario', ProfileViewSet)
router.register(r'permiso', PermisoViewSet)
router.register(r'proyecto', ProyectoViewSet)
router.register(r'authuser', AuthUserViewSet)
router.register(r'miembro', TeamMemberViewSet)
router.register(r'sprint', SprintViewSet)
router.register(r'userstory', UserStoryViewSet)
router.register(r'actividad', ActividadViewSet)
router.register(r'nota', NotaViewSet)
router.register(r'miembros-sprint', MiembrosSprintViewSet)
router.register(r'tablero', TableroViewSet)
router.register(r'historial-userstory', HistorialUserStoryViewSet)
router.register(r'historial-estados-userstory', HistorialEstadosUserStoryViewSet)

from . import vistas
urlpatterns = [
    #Rutas de la api
    path('api/',include(router.urls)), #https://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),

    path('', vistas.inicio, name='index'),
    path('login/', TemplateView.as_view(template_name='social_app/index.html'),name='login'), # <--
    path('logout/', vistas.logout),
    
    

    path('rol_sistema/crear', vistas.rol_sistema.crear ),
    path('rol_sistema/listar', vistas.rol_sistema.listar ),
    path('rol_sistema/editar/<int:codigo_rol>/', vistas.rol_sistema.editar ),

    path('rol_proyecto/crear', vistas.rol_proyecto.crear ),
    path('rol_proyecto/listar', vistas.rol_proyecto.listar ),
    path('rol_proyecto/editar/<int:codigo_rol>/', vistas.rol_proyecto.editar ),

    path('usuario/crear', vistas.usuario.crear ),
    path('usuario/listar', vistas.usuario.listar ),
    path('usuario/editar/<int:codigo>/', vistas.usuario.editar ),

    path('proyecto/crear', vistas.proyecto.crear ),
    path('proyecto/listar', vistas.proyecto.listar ),
    path('proyecto/editar/<int:codigo>/', vistas.proyecto.editar ),


    path('definicion_proyecto/', vistas.definicion_proyecto.listar ),
    path('definicion_proyecto/<int:codigo_definicion>/', vistas.definicion_proyecto.opciones ),
    path('definicion_proyecto/<int:codigo_definicion>/agregar_miembro/', vistas.definicion_proyecto.agregar_miembro ), #ASIGNACION DE ROLES Y AGREGAR MIEMBROS AL proyecto
    path('definicion_proyecto/<int:codigo_definicion>/rol_proyecto/listar/', vistas.rol_proyecto.listar ), 
    path('definicion_proyecto/<int:codigo_definicion>/rol_proyecto/crear/', vistas.rol_proyecto.crear ), 
    path('definicion_proyecto/<int:codigo_definicion>/rol_proyecto/editar/<int:codigo_rol>/', vistas.rol_proyecto.editar ), 




    path('ejecucion_proyecto/', vistas.ejecucion_proyecto.listar ),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/', vistas.ejecucion_proyecto.opciones ),


    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/crear/', vistas.sprint.crear ), 
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/listar/', vistas.sprint.listar),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/editar/<int:codigo_sprint>/', vistas.sprint.editar),


    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/agregar_miembro/', vistas.miembros_sprint.listar),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/<int:codigo_sprint>/agregar_miembro/', vistas.miembros_sprint.agregar_miembro_sprint),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/<int:codigo_sprint>/reemplazar_miembro/', vistas.miembros_sprint.reemplazar_miembro),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/miembro_sprint/editar/<int:codigo_miembro_sprint>/', vistas.miembros_sprint.editar),
    

    path('ejecucion_proyecto/<int:codigo_ejecucion>/userstory/crear/', vistas.userstory.crear ), 
    path('ejecucion_proyecto/<int:codigo_ejecucion>/userstory/listar/', vistas.userstory.listar),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/userstory/editar/<int:codigo_userstory>/', vistas.userstory.editar),


    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/sprint_planing/', vistas.sprint_planing.listar),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/<int:codigo_sprint>/sprint_planing/', vistas.sprint_planing.asignar_us),

    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/sprint_backlog/', vistas.sprint_backlog.listar),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/<int:codigo_sprint>/sprint_backlog/', vistas.sprint_backlog.ver_sprintbl),

    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/burndownchart/', vistas.burndownchart.listar),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/<int:codigo_sprint>/burndownchart/', vistas.burndownchart.ver_bdc),

    path('ejecucion_proyecto/<int:codigo_ejecucion>/userstory/product_backlog/', vistas.product_backlog),


    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/iniciar_sprint/', vistas.iniciar_sprint.listar),


    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/<int:codigo_sprint>/kanban/', vistas.administracion),


    path('ejecucion_proyecto/<int:codigo_ejecucion>/control_calidad/', vistas.control_calidad),
    
    
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/actividades/', vistas.actividades.listar),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/actividades/<int:codigo_sprint>/userstory/', vistas.actividades.listar2),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/actividades/<int:codigo_sprint>/userstory/<int:codigo_us>/', vistas.actividades.ver_actividades),
    path('ejecucion_proyecto/<int:codigo_ejecucion>/sprint/notas/<int:codigo_sprint>/userstory/<int:codigo_us>/', vistas.actividades.ver_notas),

    path('sprintbacklogpdf/<int:codigo_sprint>/proyecto/<int:codigo_ejecucion>', view=SprintBacklogPDF.as_view(), name="reporte_sb"),
    path('productbacklogpdf/<int:codigo_ejecucion>', view=ProductBacklogPDF.as_view(), name="reporte_pb"),
    path('sprintactualpdf/<int:codigo_sprint>/proyecto/<int:codigo_ejecucion>', view=HorasTrabajadasPDF.as_view(), name="reporte_ht"),


    
     
]


