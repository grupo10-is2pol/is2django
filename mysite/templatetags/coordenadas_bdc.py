import os
import time
import uuid
from datetime import timedelta

from django import template
from django.conf import settings
from django.utils import timezone

from mysite.modelos.actividad.model import Actividad
from mysite.modelos.sprint.model import Sprint
from mysite.modelos.userstory.model import UserStory



register = template.Library()                                                                                                            

@register.simple_tag(name='coordenadas_bdc')  #en cualquier template se puede usar esto, parecido a pasar contexto.                                                                                                
def coordenadas_bdc():                                                                                                                        
    coordenadasJSON= [{'x': 0, 'y': 190}, {'x': 1, 'y': 60}]                                                                                                  
    #coordenadasJSON=codigo_sprint+10
    return coordenadasJSON



# register = template.Library()                                                                                                            

# @register.simple_tag(name='coordenadas_bdc')  
# def coordenadas_bdc(codigo_ejecucion,codigo_sprint):
#     """ metodo del modelo Sprint que retorna un JSON que representa la lista de coordenadas
#         para el grafico de su correspondiente burn down chart y el total de horas trabajadas
#         :return: lista de coordenadas para graficar el burn  down chart
#     """

#     instance_sprint=Sprint.objects.get(pk=codigo_sprint)

#     #if not instance_sprint.fecha_inicio: return []

#     coordenadas = []
#     instance_userStories = UserStory.objects.filter(sprint=codigo_sprint)
#     horas = 0

#     for us in instance_userStories:
#         horas += us.duracion_estimada

#     coordenadas.append({
#                         'x': 0,
#                         'y': horas
#                         })

#     inicio = instance_sprint.fecha_inicio

#     dias_habiles = instance_sprint.dias_habiles

#     if instance_sprint.fecha_fin: #si el sprint ya finalizo
#         fin = instance_sprint.fecha_fin
#     else:                           #si el sprint sigue en curso, fin = hoy
#         fin = timezone.now().date()

#     dia = inicio
#     nro_dia = 1

#     while dia <= fin:
#         if dia.isoweekday() in dias_habiles: #la fecha en que inicio el sprint es un dia habil? 
            
#             #es un dia habil entonces ->
#             actividades = []
#             instance_actividades = Actividad.objects.filter(sprint=codigo_sprint).order_by('fecha')

#             for actividad in instance_actividades:
#                 if actividad.fecha.date() == dia:
#                     actividades.append(actividad)

#             horas_trabajadas = 0
#             for actividad in actividades:
#                 horas_trabajadas += actividad.duracion

#             coordenadas.append({
#                 'x': nro_dia,
#                 'y': horas - horas_trabajadas
#             })

#             horas = horas - horas_trabajadas

#             nro_dia += 1
#         else:
#             #no es un dia habil entonces -> y trabajo igual, cargo actividades en el tablero de  su userstory, no debería ocurrir, pero si se trabajo igual, entonces -->
#             actividades = []
#             instance_actividades = Actividad.objects.filter(sprint=codigo_sprint).order_by('fecha')

#             for actividad in instance_actividades:
#                 if actividad.fecha.date() == dia:
#                     actividades.append(actividad)

#             horas_trabajadas = 0
#             for actividad in actividades:
#                 horas_trabajadas += actividad.duracion

#             horas = horas - horas_trabajadas #se trabajo igual, no estoy seguro de poner ESTO.

#             fin = fin + timedelta(days=1)

#         dia = dia + timedelta(days=1)

#     return coordenadas




