#!/bin/bash
echo "---Base de datos desarrolloBD para entorno de Desarrollo---"
echo "Borrando base de datos desarrolloBD existente..."
dropdb -i --if-exists desarrolloBD
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo borrar la base de datos desarrolloBD, verifique que no esté siendo usada."
    exit 1
fi
echo "Se ha borrado la base de datos desarrolloBD."
echo "Creando la base de datos desarrolloBD..."
createdb desarrolloBD
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo crear desarrolloBD"
    exit 2
fi
echo "Se ha creado desarrolloBD"

source venv/bin/activate
PGPASSWORD="postgres"
psql -h localhost -p 5432 -U postgres -d desarrolloBD -f desarrolloBD.sql
echo "desarrolloBD se cargó exitosamente."
