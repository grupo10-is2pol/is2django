
#python3 manage.py shell

#CARGA DE PERMISOS

from mysite.modelos.rol.model import *

																			 #idPermiso
permisos = [ {'nombre':'Crear Proyecto' , 'tipo_permiso':'Administracion' }, #1
			{'nombre':'Editar Proyecto' , 'tipo_permiso':'Administracion' }, #2
			{'nombre':'Listar Proyecto' , 'tipo_permiso':'Administracion' }, #3
			{'nombre':'Crear Rol Sistema' , 'tipo_permiso':'Administracion' },#4
			{'nombre':'Editar Rol Sistema' , 'tipo_permiso':'Administracion' },#5
			{'nombre':'Listar Rol Sistema' , 'tipo_permiso':'Administracion' },#6
			{'nombre':'Editar Usuario' , 'tipo_permiso':'Administracion' },#7
			{'nombre':'Listar Usuario' , 'tipo_permiso':'Administracion' },#8
			{'nombre':'Ver Definicion de Proyecto' , 'tipo_permiso':'Estandar' },#9 //este permiso no se usa
			{'nombre':'Ver Ejecucion de Proyecto' , 'tipo_permiso':'Administracion' },#10
			{'nombre':'Activar Proyecto' , 'tipo_permiso':'Estandar' },#11
			{'nombre':'Ver Menu del Proyecto' , 'tipo_permiso':'Estandar' },#12
			{'nombre':'Agregar Miembro al Proyecto' , 'tipo_permiso':'Estandar' },#13
			{'nombre':'Crear Rol Proyecto' , 'tipo_permiso':'Estandar' },#14
			{'nombre':'Listar Rol Proyecto' , 'tipo_permiso':'Estandar' },#15
			{'nombre':'Editar Rol Proyecto' , 'tipo_permiso':'Estandar' },#16
			{'nombre':'Cancelar Proyecto' , 'tipo_permiso':'Estandar' },#17
			{'nombre':'Suspender Proyecto' , 'tipo_permiso':'Estandar' },#18
			{'nombre':'Finalizar Proyecto' , 'tipo_permiso':'Estandar' },#19
			{'nombre':'Listar Sprint' , 'tipo_permiso':'Estandar' },#20
			{'nombre':'Crear Sprint' , 'tipo_permiso':'Estandar' },#21
			{'nombre':'Editar Sprint' , 'tipo_permiso':'Estandar' },#22
			{'nombre':'Listar UserStory' , 'tipo_permiso':'Estandar' },#23
			{'nombre':'Crear UserStory' , 'tipo_permiso':'Estandar' },#24
			{'nombre':'Editar UserStory' , 'tipo_permiso':'Estandar' },#25
			{'nombre':'Agregar Miembro al Sprint' , 'tipo_permiso':'Estandar' },#26
			{'nombre':'Reemplazar Miembro' , 'tipo_permiso':'Estandar' },#27
			{'nombre':'Asignar UserStory' , 'tipo_permiso':'Estandar' },#28
			{'nombre':'Ver SprintBacklog' , 'tipo_permiso':'Estandar' },#29
			{'nombre':'Ver ProductBacklog' , 'tipo_permiso':'Estandar' },#30
			{'nombre':'Ver BurndownChart' , 'tipo_permiso':'Estandar' },#31
			{'nombre':'Iniciar Sprint' , 'tipo_permiso':'Estandar' },#32
			{'nombre':'Finalizar Sprint' , 'tipo_permiso':'Estandar' },#33
			{'nombre':'Ver Kanban' , 'tipo_permiso':'Estandar' },#34
			{'nombre':'Ver Control de Calidad' , 'tipo_permiso':'Estandar' },#35
			{'nombre':'Ver Actividades' , 'tipo_permiso':'Estandar' },#36
			{'nombre':'Aprobar UserStory' , 'tipo_permiso':'Estandar' },#37
			{'nombre':'Rechazar UserStory' , 'tipo_permiso':'Estandar' },#38
			{'nombre':'Generar Reportes' , 'tipo_permiso':'Estandar' },#39
			{'nombre':'Eliminar UserStory' , 'tipo_permiso':'Estandar' },#40
			{'nombre':'Eliminar Proyecto' , 'tipo_permiso':'Administracion' },#41
			{'nombre':'Eliminar Rol Proyecto' , 'tipo_permiso':'Estandar' },#42
			{'nombre':'Eliminar Rol Sistema' , 'tipo_permiso':'Administracion' },#43
			
			
			
		   ]
#{'nombre':'' , 'tipo_permiso':'Estandar' },
#{'nombre':'' , 'tipo_permiso':'Administracion' },

for permiso in permisos:
	Permiso.objects.create(nombre = permiso["nombre"],tipo_permiso = permiso["tipo_permiso"])

#CARGA DE ROLES DE SISTEMA POR DEFECTO

DescripcionAdmin='Es el encargado de crear los proyectos, asignar un Scrum Master y de asignar roles de Sistema a los usuarios, entre otras labores.'
admin_instance = RolSistema(nombre='Administrador',es_modificable=False, descripcion=DescripcionAdmin)
admin_instance.save()
admin_instance.permisos.add(1,2,3,4,5,6,7,8,10,41,43) #se agregan las id de los permisos creados anteriormente



DescripcionUsuarioNormal='Usuario que puede acceder a los proyectos (al cual pertenece)'
usuario_instance = RolSistema(nombre='Usuario Normal',es_modificable=False, descripcion=DescripcionUsuarioNormal)
usuario_instance.save()
usuario_instance.permisos.add(10) #se agregan las id de los permisos creados anteriormente


#CONFIGURACION DEL SSO 

from django.contrib.sites.models import Site
site_instance1 = Site.objects.create(name="127.0.0.1:8000",domain="127.0.0.1:8000") #SITE_ID = 2
site_instance2 = Site.objects.create(name="gestorproyectosis2.com",domain="gestorproyectosis2.com") #SITE_ID = 3
site_instance1.save()
site_instance2.save()

from allauth.socialaccount.models import SocialApp
sapp = SocialApp(provider='google', 
				name='Django Oauth', 
    			client_id='86622636108-vmagdge7o474dk5ppphjef8637hbd43k.apps.googleusercontent.com',
    			secret='pcv89XUheGcaTPnAlke5kV7u')
sapp.save()
sapp.sites.add(2,3)