# Clonar el proyecto
Via HTTP:
```bash
git clone https://gitlab.com/grupo10-is2pol/is2django.git
```
Via SSH:
```bash
git clone git@gitlab.com:grupo10-is2pol/is2django.git
```
****
# Instalar los requerimientos
```bash
pip3 install -r requirements.txt
```
***
# Crear la Base de Datos Postgres (Desarrollo)
```SQL
sudo -u postgres psql
CREATE DATABASE devdbis2;
CREATE USER grupo10 WITH ENCRYPTED PASSWORD 'grupo10';
ALTER ROLE grupo10 SET client_encoding TO 'utf8';
ALTER ROLE grupo10 SET timezone TO 'America/Asuncion';
GRANT ALL PRIVILEGES ON DATABASE devdbis2 TO grupo10;
\c devdbis2
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to grupo10;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public to grupo10;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public to grupo10;
```
***
# Crear la Base de Datos Postgres (Produccion)
```SQL
sudo -u postgres psql
CREATE DATABASE proddbis2;
CREATE USER grupo10 WITH ENCRYPTED PASSWORD 'grupo10';
ALTER ROLE grupo10 SET client_encoding TO 'utf8';
ALTER ROLE grupo10 SET timezone TO 'America/Asuncion';
GRANT ALL PRIVILEGES ON DATABASE proddbis2 TO grupo10;
\c proddbis2
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to grupo10;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public to grupo10;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public to grupo10;
```
***
# Para poblar la DB con los datos iniciales (Desarrollo)
```bash
pg_restore -v --host=localhost --port=5432 --username=grupo10 --dbname=devdbis2 devdbis2.dump
```
***
# Para poblar la DB con los datos iniciales (Produccion)
```bash
pg_restore -v --host=localhost --port=5432 --username=grupo10 --dbname=proddbis2 proddbis2.dump
```
***
# Para Correr el Proyecto (en Ambiente de Desarrollo)
```bash
python3 manage.py runserver
```
***
# Para Correr las Pruebas Unitarias

```bash
python3 manage.py test
```
Importante : este comando busca todos los archivos que empiencen con la palabra "test", ex:test_login.py, test_logout.py, etc y ejecuta todas las pruebas.

Fuente: https://docs.djangoproject.com/en/3.2/topics/testing/overview/
***
# Para Generar Documentacion
```bash
pycco -i **/*.py -p
pycco mysite -s -i -p
```
***

# Para Desplegar el proyecto en Ambiente de Produccion

```bash
sudo apt-get update
sudo apt-get install python3-virtualenv
virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt
chmod +x produccion.sh #concede permisos para ejecucion del script
./produccion.sh #ejecutar script
```

# Borrar Entorno Virtual
```bash
rm -rf venv
```
***
# Activar- Desactivar Entorno Virtual
```bash
source venv/bin/activate
deactivate
```
