# Instalar Postgres
```bash
sudo apt install postgresql
```
Verificaciones
```bash
sudo systemctl is-active postgresql
sudo systemctl is-enabled postgresql
sudo systemctl status postgresql
sudo pg_isready
```
***
# Instalar Psycopg2
```bash
sudo apt install libpq-dev python3-dev
pip3 install pyscopg2
```
***

# Instalar Pgadmin4
```bash
curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
sudo apt install pgadmin4
sudo /usr/pgadmin4/bin/setup-web.sh #para configurar el pgadmin
```
FUENTE: https://tecadmin.net/how-to-install-pgadmin4-on-ubuntu-20-04/
***

# Backup de Base de Datos y Restauracion con archivo de texto plano .sql
Backup:
Primero hacer cd a la carpeta donde queremos guardar el archivo de respaldo, despues hacer:


```bash
pg_dump -Fc -v --host=localhost --username=postgres --dbname=nombreDB -f nombreDB_backup.dump
```

OBS: Pide la contraseña del usuario postgres, para cambiar contraseña o crear una nueva hacer:
```bash
sudo -u postgres psql
\password postgres
```

Restaurar Base de Datos desde el Backup:
Restauracion: Primero hacer cd a la carpeta donde esta el archivo de backup, despues hacer:
```bash
pg_restore -v --host=localhost --port=5432 --username=postgres --dbname=nombreDB nombreDB_backup.dump
```

FUENTE: https://docs.microsoft.com/en-us/azure/postgresql/howto-migrate-using-dump-and-restore
